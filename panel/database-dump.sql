-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: elombard_db
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add bookmark',1,'add_bookmark'),(2,'Can change bookmark',1,'change_bookmark'),(3,'Can delete bookmark',1,'delete_bookmark'),(4,'Can add pinned application',2,'add_pinnedapplication'),(5,'Can change pinned application',2,'change_pinnedapplication'),(6,'Can delete pinned application',2,'delete_pinnedapplication'),(7,'Can add user dashboard module',3,'add_userdashboardmodule'),(8,'Can change user dashboard module',3,'change_userdashboardmodule'),(9,'Can delete user dashboard module',3,'delete_userdashboardmodule'),(10,'Can add loq yazısı',4,'add_logentry'),(11,'Can change loq yazısı',4,'change_logentry'),(12,'Can delete loq yazısı',4,'delete_logentry'),(13,'Can add icazə',5,'add_permission'),(14,'Can change icazə',5,'change_permission'),(15,'Can delete icazə',5,'delete_permission'),(16,'Can add qrup',6,'add_group'),(17,'Can change qrup',6,'change_group'),(18,'Can delete qrup',6,'delete_group'),(19,'Can add məzmun növü',7,'add_contenttype'),(20,'Can change məzmun növü',7,'change_contenttype'),(21,'Can delete məzmun növü',7,'delete_contenttype'),(22,'Can add seans',8,'add_session'),(23,'Can change seans',8,'change_session'),(24,'Can delete seans',8,'delete_session'),(25,'Can add İstifadəçi',9,'add_myuser'),(26,'Can change İstifadəçi',9,'change_myuser'),(27,'Can delete İstifadəçi',9,'delete_myuser'),(28,'Can add Təstiqlənmiş user',10,'add_userconfrimationkeys'),(29,'Can change Təstiqlənmiş user',10,'change_userconfrimationkeys'),(30,'Can delete Təstiqlənmiş user',10,'delete_userconfrimationkeys'),(31,'Can add log',11,'add_log'),(32,'Can change log',11,'change_log'),(33,'Can delete log',11,'delete_log'),(34,'Can add package status',12,'add_packagestatus'),(35,'Can change package status',12,'change_packagestatus'),(36,'Can delete package status',12,'delete_packagestatus'),(37,'Can add outcome name',13,'add_outcomename'),(38,'Can change outcome name',13,'change_outcomename'),(39,'Can delete outcome name',13,'delete_outcomename'),(40,'Can add product type',14,'add_producttype'),(41,'Can change product type',14,'change_producttype'),(42,'Can delete product type',14,'delete_producttype'),(43,'Can add employee permit',15,'add_employeepermit'),(44,'Can change employee permit',15,'change_employeepermit'),(45,'Can delete employee permit',15,'delete_employeepermit'),(46,'Can add package product image',16,'add_packageproductimage'),(47,'Can change package product image',16,'change_packageproductimage'),(48,'Can delete package product image',16,'delete_packageproductimage'),(49,'Can add employee salary',17,'add_employeesalary'),(50,'Can change employee salary',17,'change_employeesalary'),(51,'Can delete employee salary',17,'delete_employeesalary'),(52,'Can add income outcome',18,'add_incomeoutcome'),(53,'Can change income outcome',18,'change_incomeoutcome'),(54,'Can delete income outcome',18,'delete_incomeoutcome'),(55,'Can add outcome type2',19,'add_outcometype2'),(56,'Can change outcome type2',19,'change_outcometype2'),(57,'Can delete outcome type2',19,'delete_outcometype2'),(58,'Can add product',20,'add_product'),(59,'Can change product',20,'change_product'),(60,'Can delete product',20,'delete_product'),(61,'Can add waiting package',21,'add_waitingpackage'),(62,'Can change waiting package',21,'change_waitingpackage'),(63,'Can delete waiting package',21,'delete_waitingpackage'),(64,'Can add package order',22,'add_packageorder'),(65,'Can change package order',22,'change_packageorder'),(66,'Can delete package order',22,'delete_packageorder'),(67,'Can add package type',23,'add_packagetype'),(68,'Can change package type',23,'change_packagetype'),(69,'Can delete package type',23,'delete_packagetype'),(70,'Can add customer note',24,'add_customernote'),(71,'Can change customer note',24,'change_customernote'),(72,'Can delete customer note',24,'delete_customernote'),(73,'Can add customer',25,'add_customer'),(74,'Can change customer',25,'change_customer'),(75,'Can delete customer',25,'delete_customer'),(76,'Can add package product',26,'add_packageproduct'),(77,'Can change package product',26,'change_packageproduct'),(78,'Can delete package product',26,'delete_packageproduct'),(79,'Can add outcome type',27,'add_outcometype'),(80,'Can change outcome type',27,'change_outcometype'),(81,'Can delete outcome type',27,'delete_outcometype'),(82,'Can add package',28,'add_package'),(83,'Can change package',28,'change_package'),(84,'Can delete package',28,'delete_package'),(85,'Can add package payment dates',29,'add_packagepaymentdates'),(86,'Can change package payment dates',29,'change_packagepaymentdates'),(87,'Can delete package payment dates',29,'delete_packagepaymentdates'),(88,'Can add currency',30,'add_currency'),(89,'Can change currency',30,'change_currency'),(90,'Can delete currency',30,'delete_currency'),(91,'Can add department',31,'add_department'),(92,'Can change department',31,'change_department'),(93,'Can delete department',31,'delete_department'),(94,'Can add customer phone number',32,'add_customerphonenumber'),(95,'Can change customer phone number',32,'change_customerphonenumber'),(96,'Can delete customer phone number',32,'delete_customerphonenumber'),(97,'Can add log entry',33,'add_logentry'),(98,'Can change log entry',33,'change_logentry'),(99,'Can delete log entry',33,'delete_logentry'),(100,'Can add package daily note',34,'add_packagedailynote'),(101,'Can change package daily note',34,'change_packagedailynote'),(102,'Can delete package daily note',34,'delete_packagedailynote');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `base_user_myuser`
--

DROP TABLE IF EXISTS `base_user_myuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_user_myuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(100) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `salary` decimal(19,2) NOT NULL,
  `profile_picture` varchar(100) DEFAULT NULL,
  `passport_picture1` varchar(100) DEFAULT NULL,
  `passport_picture2` varchar(100) DEFAULT NULL,
  `verified` tinyint(1) NOT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `slug` varchar(50) DEFAULT NULL,
  `usertype` int(11) DEFAULT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  `department_id` int(11),
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `base_user_myuser_2dbcba41` (`slug`),
  KEY `base_user_myuser_department_id_45d52523_fk_panel_department_id` (`department_id`),
  CONSTRAINT `base_user_myuser_department_id_45d52523_fk_panel_department_id` FOREIGN KEY (`department_id`) REFERENCES `panel_department` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_user_myuser`
--

LOCK TABLES `base_user_myuser` WRITE;
/*!40000 ALTER TABLE `base_user_myuser` DISABLE KEYS */;
INSERT INTO `base_user_myuser` VALUES (1,'pbkdf2_sha256$30000$okPFCXcTx69O$Bbc2IL6vVyLtXCypo3msD+aO83bGIUqyHytKypPHba8=','2019-09-01 21:08:57.000000',1,'ataxanr@gmail.com','','','ataxan@gmail.com',0.00,'','','',0,NULL,'1567357796-98552',1,1,1,'2019-09-01 21:02:41.000000',NULL),(2,'pbkdf2_sha256$30000$BR0iJmEDG0Sz$SjUbT783wmaeHfictGSv49LWeWuysKGHRKyHVnVeQ3k=','2019-09-03 00:25:17.242718',1,'yunis@mail.com','','','yunis@mail.com',0.00,'','','',0,NULL,'1567455463-661899',1,1,1,'2019-09-03 00:16:10.000000',NULL),(3,'pbkdf2_sha256$30000$db6da$2iyM5I06N1/R+ZxwB/wSOKMG2itFAzXw7j3V0joe9ig=',NULL,0,'cimicimi@mail.ru','Rəşad','Cimi','cimicimi@mail.ru',0.00,'','','',0,'0556550400','resad1568197083-379261',2,0,1,'2019-09-11 14:18:03.364780',1);
/*!40000 ALTER TABLE `base_user_myuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `base_user_myuser_groups`
--

DROP TABLE IF EXISTS `base_user_myuser_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_user_myuser_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `myuser_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `base_user_myuser_groups_myuser_id_83a07e94_uniq` (`myuser_id`,`group_id`),
  KEY `base_user_myuser_groups_group_id_ac6efcd5_fk_auth_group_id` (`group_id`),
  CONSTRAINT `base_user_myuser_group_myuser_id_28b31ef4_fk_base_user_myuser_id` FOREIGN KEY (`myuser_id`) REFERENCES `base_user_myuser` (`id`),
  CONSTRAINT `base_user_myuser_groups_group_id_ac6efcd5_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_user_myuser_groups`
--

LOCK TABLES `base_user_myuser_groups` WRITE;
/*!40000 ALTER TABLE `base_user_myuser_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `base_user_myuser_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `base_user_myuser_permissions_department`
--

DROP TABLE IF EXISTS `base_user_myuser_permissions_department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_user_myuser_permissions_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `myuser_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `base_user_myuser_permissions_department_myuser_id_fa62519c_uniq` (`myuser_id`,`department_id`),
  KEY `base_user_myuser_p_department_id_856c06b4_fk_panel_department_id` (`department_id`),
  CONSTRAINT `base_user_myuser_p_department_id_856c06b4_fk_panel_department_id` FOREIGN KEY (`department_id`) REFERENCES `panel_department` (`id`),
  CONSTRAINT `base_user_myuser_permi_myuser_id_f38adeab_fk_base_user_myuser_id` FOREIGN KEY (`myuser_id`) REFERENCES `base_user_myuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_user_myuser_permissions_department`
--

LOCK TABLES `base_user_myuser_permissions_department` WRITE;
/*!40000 ALTER TABLE `base_user_myuser_permissions_department` DISABLE KEYS */;
/*!40000 ALTER TABLE `base_user_myuser_permissions_department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `base_user_myuser_user_permissions`
--

DROP TABLE IF EXISTS `base_user_myuser_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_user_myuser_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `myuser_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `base_user_myuser_user_permissions_myuser_id_85ee0956_uniq` (`myuser_id`,`permission_id`),
  KEY `base_user_myuser_us_permission_id_43fe495d_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `base_user_myuser_us_permission_id_43fe495d_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `base_user_myuser_user__myuser_id_8e8dba13_fk_base_user_myuser_id` FOREIGN KEY (`myuser_id`) REFERENCES `base_user_myuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_user_myuser_user_permissions`
--

LOCK TABLES `base_user_myuser_user_permissions` WRITE;
/*!40000 ALTER TABLE `base_user_myuser_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `base_user_myuser_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `base_user_userconfrimationkeys`
--

DROP TABLE IF EXISTS `base_user_userconfrimationkeys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_user_userconfrimationkeys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `expired_date` datetime(6) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `date` datetime(6) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `base_user_userconfrimati_user_id_67e677d6_fk_base_user_myuser_id` (`user_id`),
  CONSTRAINT `base_user_userconfrimati_user_id_67e677d6_fk_base_user_myuser_id` FOREIGN KEY (`user_id`) REFERENCES `base_user_myuser` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_user_userconfrimationkeys`
--

LOCK TABLES `base_user_userconfrimationkeys` WRITE;
/*!40000 ALTER TABLE `base_user_userconfrimationkeys` DISABLE KEYS */;
INSERT INTO `base_user_userconfrimationkeys` VALUES (1,'ca8f45bb-c564-4e2e-8b6b-2f9250a8dcb8','2019-09-08 21:02:41.463280',0,'2019-09-01 21:02:41.463954',1),(2,'5cb0d4ad-8da0-482d-9aef-447412ae284d','2019-09-10 00:16:10.537683',0,'2019-09-03 00:16:10.538555',2),(3,'a530f4a2-1ab7-4576-adef-4f9da09e1114','2019-09-18 14:18:03.367562',0,'2019-09-11 14:18:03.368167',3);
/*!40000 ALTER TABLE `base_user_userconfrimationkeys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_userdashboardmodule`
--

DROP TABLE IF EXISTS `dashboard_userdashboardmodule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard_userdashboardmodule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `module` varchar(255) NOT NULL,
  `app_label` varchar(255) DEFAULT NULL,
  `user` int(10) unsigned NOT NULL,
  `column` int(10) unsigned NOT NULL,
  `order` int(11) NOT NULL,
  `settings` longtext NOT NULL,
  `children` longtext NOT NULL,
  `collapsed` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_userdashboardmodule`
--

LOCK TABLES `dashboard_userdashboardmodule` WRITE;
/*!40000 ALTER TABLE `dashboard_userdashboardmodule` DISABLE KEYS */;
INSERT INTO `dashboard_userdashboardmodule` VALUES (1,'Quick links','jet.dashboard.modules.LinkList',NULL,1,0,0,'{\"draggable\": false, \"layout\": \"inline\", \"collapsible\": false, \"deletable\": false}','[{\"url\": \"/\", \"title\": \"Return to site\"}, {\"url\": \"/admin/password_change/\", \"title\": \"Change password\"}, {\"url\": \"/admin/logout/\", \"title\": \"Log out\"}]',0),(2,'Applications','jet.dashboard.modules.AppList',NULL,1,1,0,'{\"models\": null, \"exclude\": [\"auth.*\"]}','',0),(3,'Administration','jet.dashboard.modules.AppList',NULL,1,2,0,'{\"models\": [\"auth.*\"], \"exclude\": null}','',0),(4,'Recent Actions','jet.dashboard.modules.RecentActions',NULL,1,0,1,'{\"limit\": 10, \"include_list\": null, \"user\": null, \"exclude_list\": null}','',0),(5,'Latest Django News','jet.dashboard.modules.Feed',NULL,1,1,1,'{\"limit\": 5, \"feed_url\": \"http://www.djangoproject.com/rss/weblog/\"}','',0),(6,'Support','jet.dashboard.modules.LinkList',NULL,1,2,1,'{\"draggable\": true, \"layout\": \"stacked\", \"collapsible\": true, \"deletable\": true}','[{\"url\": \"http://docs.djangoproject.com/\", \"external\": true, \"title\": \"Django documentation\"}, {\"url\": \"http://groups.google.com/group/django-users\", \"external\": true, \"title\": \"Django \\\"django-users\\\" mailing list\"}, {\"url\": \"irc://irc.freenode.net/django\", \"external\": true, \"title\": \"Django irc channel\"}]',0),(7,'Application models','jet.dashboard.modules.ModelList','base_user',1,0,0,'{\"models\": [\"base_user.*\"], \"exclude\": null}','',0),(8,'Recent Actions','jet.dashboard.modules.RecentActions','base_user',1,1,0,'{\"limit\": 10, \"include_list\": [\"base_user.*\"], \"user\": null, \"exclude_list\": null}','',0),(9,'Быстрые ссылки','jet.dashboard.modules.LinkList',NULL,2,0,0,'{\"layout\": \"inline\", \"draggable\": false, \"collapsible\": false, \"deletable\": false}','[{\"title\": \"\\u0412\\u0435\\u0440\\u043d\\u0443\\u0442\\u044c\\u0441\\u044f \\u043d\\u0430 \\u0441\\u0430\\u0439\\u0442\", \"url\": \"/\"}, {\"title\": \"\\u0418\\u0437\\u043c\\u0435\\u043d\\u0438\\u0442\\u044c \\u043f\\u0430\\u0440\\u043e\\u043b\\u044c\", \"url\": \"/admin/password_change/\"}, {\"title\": \"\\u0412\\u044b\\u0439\\u0442\\u0438\", \"url\": \"/admin/logout/\"}]',0),(10,'Приложения','jet.dashboard.modules.AppList',NULL,2,1,0,'{\"exclude\": [\"auth.*\"], \"models\": null}','',0),(11,'Администрирование','jet.dashboard.modules.AppList',NULL,2,2,0,'{\"exclude\": null, \"models\": [\"auth.*\"]}','',0),(12,'Последние действия','jet.dashboard.modules.RecentActions',NULL,2,0,1,'{\"include_list\": null, \"user\": null, \"exclude_list\": null, \"limit\": 10}','',0),(13,'Новости от Django','jet.dashboard.modules.Feed',NULL,2,1,1,'{\"limit\": 5, \"feed_url\": \"http://www.djangoproject.com/rss/weblog/\"}','',0),(14,'Поддержка','jet.dashboard.modules.LinkList',NULL,2,2,1,'{\"layout\": \"stacked\", \"draggable\": true, \"collapsible\": true, \"deletable\": true}','[{\"title\": \"\\u0414\\u043e\\u043a\\u0443\\u043c\\u0435\\u043d\\u0442\\u0430\\u0446\\u0438\\u044f \\u043f\\u043e Django\", \"url\": \"http://docs.djangoproject.com/\", \"external\": true}, {\"title\": \"\\u0413\\u0443\\u0433\\u043b-\\u0433\\u0440\\u0443\\u043f\\u043f\\u0430 \\\"django-users\\\"\", \"url\": \"http://groups.google.com/group/django-users\", \"external\": true}, {\"title\": \"IRC \\u043a\\u0430\\u043d\\u0430\\u043b Django\", \"url\": \"irc://irc.freenode.net/django\", \"external\": true}]',0),(15,'Модели приложения','jet.dashboard.modules.ModelList','base_user',2,0,0,'{\"exclude\": null, \"models\": [\"base_user.*\"]}','',0),(16,'Последние действия','jet.dashboard.modules.RecentActions','base_user',2,1,0,'{\"include_list\": [\"base_user.*\"], \"user\": null, \"exclude_list\": null, \"limit\": 10}','',0);
/*!40000 ALTER TABLE `dashboard_userdashboardmodule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_base_user_myuser_id` (`user_id`),
  CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_base_user_myuser_id` FOREIGN KEY (`user_id`) REFERENCES `base_user_myuser` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2019-09-01 21:09:56.994687','1','ataxanr@gmail.com',2,'[{\"changed\": {\"fields\": [\"usertype\"]}}]',9,1),(2,'2019-09-03 00:17:43.671747','2','yunis@mail.com',2,'[{\"changed\": {\"fields\": [\"usertype\"]}}]',9,2);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (4,'admin','logentry'),(6,'auth','group'),(5,'auth','permission'),(9,'base_user','myuser'),(10,'base_user','userconfrimationkeys'),(7,'contenttypes','contenttype'),(3,'dashboard','userdashboardmodule'),(1,'jet','bookmark'),(2,'jet','pinnedapplication'),(33,'model_logging','logentry'),(30,'panel','currency'),(25,'panel','customer'),(24,'panel','customernote'),(32,'panel','customerphonenumber'),(31,'panel','department'),(15,'panel','employeepermit'),(17,'panel','employeesalary'),(18,'panel','incomeoutcome'),(11,'panel','log'),(13,'panel','outcomename'),(27,'panel','outcometype'),(19,'panel','outcometype2'),(28,'panel','package'),(34,'panel','packagedailynote'),(22,'panel','packageorder'),(29,'panel','packagepaymentdates'),(26,'panel','packageproduct'),(16,'panel','packageproductimage'),(12,'panel','packagestatus'),(23,'panel','packagetype'),(20,'panel','product'),(14,'panel','producttype'),(21,'panel','waitingpackage'),(8,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2019-09-01 21:01:25.624110'),(2,'base_user','0001_initial','2019-09-01 21:01:25.676118'),(3,'admin','0001_initial','2019-09-01 21:01:25.725181'),(4,'admin','0002_logentry_remove_auto_add','2019-09-01 21:01:25.737798'),(5,'admin','0003_auto_20190901_2101','2019-09-01 21:01:25.900767'),(6,'contenttypes','0002_remove_content_type_name','2019-09-01 21:01:25.965020'),(7,'auth','0001_initial','2019-09-01 21:01:26.098176'),(8,'auth','0002_alter_permission_name_max_length','2019-09-01 21:01:26.128902'),(9,'auth','0003_alter_user_email_max_length','2019-09-01 21:01:26.143094'),(10,'auth','0004_alter_user_username_opts','2019-09-01 21:01:26.162766'),(11,'auth','0005_alter_user_last_login_null','2019-09-01 21:01:26.182225'),(12,'auth','0006_require_contenttypes_0002','2019-09-01 21:01:26.185421'),(13,'auth','0007_alter_validators_add_error_messages','2019-09-01 21:01:26.203318'),(14,'auth','0008_alter_user_username_max_length','2019-09-01 21:01:26.218213'),(15,'auth','0009_auto_20190901_2101','2019-09-01 21:01:26.546185'),(16,'panel','0001_initial','2019-09-01 21:01:28.491948'),(17,'base_user','0002_auto_20190901_2101','2019-09-01 21:01:28.858575'),(18,'contenttypes','0003_auto_20190901_2101','2019-09-01 21:01:28.944102'),(19,'dashboard','0001_initial','2019-09-01 21:01:28.964063'),(20,'dashboard','0002_auto_20190901_2101','2019-09-01 21:01:28.971764'),(21,'jet','0001_initial','2019-09-01 21:01:29.018968'),(22,'jet','0002_delete_userdashboardmodule','2019-09-01 21:01:29.029133'),(23,'jet','0003_auto_20190901_2101','2019-09-01 21:01:29.039604'),(24,'model_logging','0001_initial','2019-09-01 21:01:29.216433'),(25,'model_logging','0002_add_new_data_field','2019-09-01 21:01:29.218577'),(26,'model_logging','0003_data_migration','2019-09-01 21:01:29.220656'),(27,'model_logging','0004_remove_old_field','2019-09-01 21:01:29.222350'),(28,'sessions','0001_initial','2019-09-01 21:01:29.240508'),(29,'sessions','0002_auto_20190901_2101','2019-09-01 21:01:29.257408'),(30,'model_logging','0001_squashed_0004_remove_old_field','2019-09-01 21:01:29.261804'),(31,'panel','0002_auto_20190902_2307','2019-09-02 23:07:16.959687'),(32,'panel','0003_auto_20190911_1357','2019-09-11 13:57:44.576438'),(33,'panel','0004_auto_20190912_0331','2019-09-12 03:32:04.459803');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('6qqb0s8191ptuxa0eejih6w3e87j3t7e','YzJmMmU2ZTUyOTVlN2MxZDlmNzA5NjcwMTEzZTU3ODMxZmYxN2M1Mzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGJmODU0YTNmZGZjMDViODAzODE1MWQxNGI5ZTAyYjk5M2QzODAxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2019-09-17 00:25:17.245907'),('isgbnb8n5oejg5liv0oca5n4py6v9skp','YTdlYjczZTYwZjkyYTI0NTVlMjAwNjcwNjRjZDUyYTczOWVmYTQ0Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiM2ZiNmNmMjc5MTdiNTA3MzEyYjQwOGZmYjQxNjU4YWIxZGIzMGZmNCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2019-09-15 21:08:57.652987'),('sz6gg2fkt2h1g68jzea1ok7odbb1frak','YzJmMmU2ZTUyOTVlN2MxZDlmNzA5NjcwMTEzZTU3ODMxZmYxN2M1Mzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGJmODU0YTNmZGZjMDViODAzODE1MWQxNGI5ZTAyYjk5M2QzODAxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2019-09-17 00:16:31.111344');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jet_bookmark`
--

DROP TABLE IF EXISTS `jet_bookmark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jet_bookmark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(200) NOT NULL,
  `title` varchar(255) NOT NULL,
  `user` int(10) unsigned NOT NULL,
  `date_add` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jet_bookmark`
--

LOCK TABLES `jet_bookmark` WRITE;
/*!40000 ALTER TABLE `jet_bookmark` DISABLE KEYS */;
/*!40000 ALTER TABLE `jet_bookmark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jet_pinnedapplication`
--

DROP TABLE IF EXISTS `jet_pinnedapplication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jet_pinnedapplication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(255) NOT NULL,
  `user` int(10) unsigned NOT NULL,
  `date_add` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jet_pinnedapplication`
--

LOCK TABLES `jet_pinnedapplication` WRITE;
/*!40000 ALTER TABLE `jet_pinnedapplication` DISABLE KEYS */;
/*!40000 ALTER TABLE `jet_pinnedapplication` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_logging_logentry`
--

DROP TABLE IF EXISTS `model_logging_logentry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_logging_logentry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` datetime(6) NOT NULL,
  `operation` varchar(255) NOT NULL,
  `model_path` varchar(255) NOT NULL,
  `data` longtext NOT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `model_logging_logentr_creator_id_0fb1d015_fk_base_user_myuser_id` (`creator_id`),
  KEY `model_logging_logentry_user_id_f0d573c8_fk_base_user_myuser_id` (`user_id`),
  CONSTRAINT `model_logging_logentr_creator_id_0fb1d015_fk_base_user_myuser_id` FOREIGN KEY (`creator_id`) REFERENCES `base_user_myuser` (`id`),
  CONSTRAINT `model_logging_logentry_user_id_f0d573c8_fk_base_user_myuser_id` FOREIGN KEY (`user_id`) REFERENCES `base_user_myuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_logging_logentry`
--

LOCK TABLES `model_logging_logentry` WRITE;
/*!40000 ALTER TABLE `model_logging_logentry` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_logging_logentry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panel_currency`
--

DROP TABLE IF EXISTS `panel_currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panel_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `percent` decimal(19,5) NOT NULL,
  `short_title` varchar(10) NOT NULL,
  `removed` tinyint(1) NOT NULL,
  `date` datetime(6) NOT NULL,
  `value` decimal(19,5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panel_currency`
--

LOCK TABLES `panel_currency` WRITE;
/*!40000 ALTER TABLE `panel_currency` DISABLE KEYS */;
INSERT INTO `panel_currency` VALUES (1,'AZN',0.00000,'Azn',0,'2019-09-11 13:58:57.935162',1.00000),(2,'$',0.00000,'Usd',0,'2019-09-11 13:59:28.790677',1.70000);
/*!40000 ALTER TABLE `panel_currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panel_customer`
--

DROP TABLE IF EXISTS `panel_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panel_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) NOT NULL,
  `serial_number` varchar(255) NOT NULL,
  `fin` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `contact_number` varchar(255) NOT NULL,
  `birthdate` date DEFAULT NULL,
  `id_number` char(32) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `profile_picture` varchar(100) DEFAULT NULL,
  `passport_picture1` varchar(100) DEFAULT NULL,
  `passport_picture2` varchar(100) DEFAULT NULL,
  `date` datetime(6) NOT NULL,
  `department_id` int(11),
  `gender` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_number` (`id_number`),
  KEY `panel_customer_department_id_93395493_fk_panel_department_id` (`department_id`),
  CONSTRAINT `panel_customer_department_id_93395493_fk_panel_department_id` FOREIGN KEY (`department_id`) REFERENCES `panel_department` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panel_customer`
--

LOCK TABLES `panel_customer` WRITE;
/*!40000 ALTER TABLE `panel_customer` DISABLE KEYS */;
INSERT INTO `panel_customer` VALUES (1,'Jolie Ancelina','az12345678','njkgyfydyhgb','ctgujl;j;jk;','0557450405','1970-02-03','ab6647edb19848a2bdb9430a84e216a8',1,'photos/2019/9/11/7bf5872bc3ca46808802b3b635bf963b.jpg','','','2019-09-11 14:49:53.133479',1,'man'),(2,'Pit Bred','13213123','jklkskkk8','azerbaycan','0556550400','1994-02-02','78f8be2ea16b453eba53ffb099f1299d',1,'photos/2019/9/11/2417c551962c4bf99ca2ccf348edb697.jpg','','','2019-09-11 14:55:57.781159',2,'man');
/*!40000 ALTER TABLE `panel_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panel_customernote`
--

DROP TABLE IF EXISTS `panel_customernote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panel_customernote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `date` datetime(6) NOT NULL,
  `customer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `panel_customernote_customer_id_43bf5960_fk_panel_customer_id` (`customer_id`),
  CONSTRAINT `panel_customernote_customer_id_43bf5960_fk_panel_customer_id` FOREIGN KEY (`customer_id`) REFERENCES `panel_customer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panel_customernote`
--

LOCK TABLES `panel_customernote` WRITE;
/*!40000 ALTER TABLE `panel_customernote` DISABLE KEYS */;
/*!40000 ALTER TABLE `panel_customernote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panel_customerphonenumber`
--

DROP TABLE IF EXISTS `panel_customerphonenumber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panel_customerphonenumber` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(255) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `panel_customerphonenum_customer_id_a5c8df55_fk_panel_customer_id` (`customer_id`),
  CONSTRAINT `panel_customerphonenum_customer_id_a5c8df55_fk_panel_customer_id` FOREIGN KEY (`customer_id`) REFERENCES `panel_customer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panel_customerphonenumber`
--

LOCK TABLES `panel_customerphonenumber` WRITE;
/*!40000 ALTER TABLE `panel_customerphonenumber` DISABLE KEYS */;
INSERT INTO `panel_customerphonenumber` VALUES (1,'050 9999999',1),(2,'0125134177',1),(3,'055 555 55 10',2);
/*!40000 ALTER TABLE `panel_customerphonenumber` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panel_department`
--

DROP TABLE IF EXISTS `panel_department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panel_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `address` varchar(10) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `removed` tinyint(1) NOT NULL,
  `date` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panel_department`
--

LOCK TABLES `panel_department` WRITE;
/*!40000 ALTER TABLE `panel_department` DISABLE KEYS */;
INSERT INTO `panel_department` VALUES (1,'MƏRKƏZ','YASAMAL','192.168.0.1',0,'2019-09-11 14:00:41.096603'),(2,'ƏHMƏLİ','sarayevo 8','192.168.1.1',0,'2019-09-11 14:01:10.252143'),(3,'SUMQAYIT','SPUTNİK','168.192',0,'2019-09-11 14:01:58.573907'),(4,'XALQLAR','QARAYEV','1921144.2255',0,'2019-09-11 14:02:39.142562');
/*!40000 ALTER TABLE `panel_department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panel_employeepermit`
--

DROP TABLE IF EXISTS `panel_employeepermit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panel_employeepermit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reason` varchar(100) NOT NULL,
  `description` longtext NOT NULL,
  `day` int(11) NOT NULL,
  `time` time(6) NOT NULL,
  `date` datetime(6) NOT NULL,
  `employee_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `panel_employeepermit_employee_id_3acc3203_fk_base_user_myuser_id` (`employee_id`),
  CONSTRAINT `panel_employeepermit_employee_id_3acc3203_fk_base_user_myuser_id` FOREIGN KEY (`employee_id`) REFERENCES `base_user_myuser` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panel_employeepermit`
--

LOCK TABLES `panel_employeepermit` WRITE;
/*!40000 ALTER TABLE `panel_employeepermit` DISABLE KEYS */;
INSERT INTO `panel_employeepermit` VALUES (1,'penalty','hjvbjb',1,'01:25:00.000000','2019-09-11 14:27:24.333639',3);
/*!40000 ALTER TABLE `panel_employeepermit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panel_employeesalary`
--

DROP TABLE IF EXISTS `panel_employeesalary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panel_employeesalary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `work_minute` int(11) NOT NULL,
  `salary_amount` decimal(19,2) NOT NULL,
  `result_amount` decimal(19,2) NOT NULL,
  `penalty` decimal(19,2) NOT NULL,
  `date` datetime(6) NOT NULL,
  `employee_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `panel_employeesalary_employee_id_0e834e9f_uniq` (`employee_id`,`month`,`year`),
  CONSTRAINT `panel_employeesalary_employee_id_80358409_fk_base_user_myuser_id` FOREIGN KEY (`employee_id`) REFERENCES `base_user_myuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panel_employeesalary`
--

LOCK TABLES `panel_employeesalary` WRITE;
/*!40000 ALTER TABLE `panel_employeesalary` DISABLE KEYS */;
/*!40000 ALTER TABLE `panel_employeesalary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panel_incomeoutcome`
--

DROP TABLE IF EXISTS `panel_incomeoutcome`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panel_incomeoutcome` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `reason` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `amount` decimal(19,2) NOT NULL,
  `notes` longtext NOT NULL,
  `date` datetime(6) NOT NULL,
  `created_date` datetime(6) NOT NULL,
  `author_id` int(11) NOT NULL,
  `department_id` int(11) DEFAULT NULL,
  `outcome_name_id` int(11),
  `outcome_type_id` int(11),
  `outcome_type2_id` int(11),
  `package_payment_date_id` int(11),
  `user_id` int(11) NOT NULL,
  `user_to_id` int(11),
  `currency_id` int(11),
  PRIMARY KEY (`id`),
  KEY `panel_incomeoutcome_author_id_c00fa805_fk_base_user_myuser_id` (`author_id`),
  KEY `panel_incomeoutcom_department_id_17643200_fk_panel_department_id` (`department_id`),
  KEY `panel_incomeout_outcome_name_id_fcc90823_fk_panel_outcomename_id` (`outcome_name_id`),
  KEY `panel_incomeout_outcome_type_id_d3dd2c21_fk_panel_outcometype_id` (`outcome_type_id`),
  KEY `panel_incomeo_outcome_type2_id_fea11bfb_fk_panel_outcometype2_id` (`outcome_type2_id`),
  KEY `D2088d096ca5f8bc178a9bf5715cc069` (`package_payment_date_id`),
  KEY `panel_incomeoutcome_user_id_6e624418_fk_base_user_myuser_id` (`user_id`),
  KEY `panel_incomeoutcome_user_to_id_09f589a4_fk_base_user_myuser_id` (`user_to_id`),
  KEY `panel_incomeoutcome_currency_id_fef3b691_fk_panel_currency_id` (`currency_id`),
  CONSTRAINT `D2088d096ca5f8bc178a9bf5715cc069` FOREIGN KEY (`package_payment_date_id`) REFERENCES `panel_packagepaymentdates` (`id`),
  CONSTRAINT `panel_incomeo_outcome_type2_id_fea11bfb_fk_panel_outcometype2_id` FOREIGN KEY (`outcome_type2_id`) REFERENCES `panel_outcometype2` (`id`),
  CONSTRAINT `panel_incomeout_outcome_name_id_fcc90823_fk_panel_outcomename_id` FOREIGN KEY (`outcome_name_id`) REFERENCES `panel_outcomename` (`id`),
  CONSTRAINT `panel_incomeout_outcome_type_id_d3dd2c21_fk_panel_outcometype_id` FOREIGN KEY (`outcome_type_id`) REFERENCES `panel_outcometype` (`id`),
  CONSTRAINT `panel_incomeoutcom_department_id_17643200_fk_panel_department_id` FOREIGN KEY (`department_id`) REFERENCES `panel_department` (`id`),
  CONSTRAINT `panel_incomeoutcome_author_id_c00fa805_fk_base_user_myuser_id` FOREIGN KEY (`author_id`) REFERENCES `base_user_myuser` (`id`),
  CONSTRAINT `panel_incomeoutcome_currency_id_fef3b691_fk_panel_currency_id` FOREIGN KEY (`currency_id`) REFERENCES `panel_currency` (`id`),
  CONSTRAINT `panel_incomeoutcome_user_id_6e624418_fk_base_user_myuser_id` FOREIGN KEY (`user_id`) REFERENCES `base_user_myuser` (`id`),
  CONSTRAINT `panel_incomeoutcome_user_to_id_09f589a4_fk_base_user_myuser_id` FOREIGN KEY (`user_to_id`) REFERENCES `base_user_myuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panel_incomeoutcome`
--

LOCK TABLES `panel_incomeoutcome` WRITE;
/*!40000 ALTER TABLE `panel_incomeoutcome` DISABLE KEYS */;
/*!40000 ALTER TABLE `panel_incomeoutcome` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panel_log`
--

DROP TABLE IF EXISTS `panel_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panel_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `object` varchar(255) NOT NULL,
  `operation` varchar(255) NOT NULL,
  `date` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panel_log`
--

LOCK TABLES `panel_log` WRITE;
/*!40000 ALTER TABLE `panel_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `panel_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panel_outcomename`
--

DROP TABLE IF EXISTS `panel_outcomename`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panel_outcomename` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `date` datetime(6) NOT NULL,
  `type_id` int(11),
  PRIMARY KEY (`id`),
  KEY `panel_outcomename_type_id_22067d2a_fk_panel_outcometype_id` (`type_id`),
  CONSTRAINT `panel_outcomename_type_id_22067d2a_fk_panel_outcometype_id` FOREIGN KEY (`type_id`) REFERENCES `panel_outcometype` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panel_outcomename`
--

LOCK TABLES `panel_outcomename` WRITE;
/*!40000 ALTER TABLE `panel_outcomename` DISABLE KEYS */;
/*!40000 ALTER TABLE `panel_outcomename` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panel_outcometype`
--

DROP TABLE IF EXISTS `panel_outcometype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panel_outcometype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `date` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panel_outcometype`
--

LOCK TABLES `panel_outcometype` WRITE;
/*!40000 ALTER TABLE `panel_outcometype` DISABLE KEYS */;
/*!40000 ALTER TABLE `panel_outcometype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panel_outcometype2`
--

DROP TABLE IF EXISTS `panel_outcometype2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panel_outcometype2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `date` datetime(6) NOT NULL,
  `type_name_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `panel_outcometype2_type_name_id_44a36680_fk_panel_outcomename_id` (`type_name_id`),
  CONSTRAINT `panel_outcometype2_type_name_id_44a36680_fk_panel_outcomename_id` FOREIGN KEY (`type_name_id`) REFERENCES `panel_outcomename` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panel_outcometype2`
--

LOCK TABLES `panel_outcometype2` WRITE;
/*!40000 ALTER TABLE `panel_outcometype2` DISABLE KEYS */;
/*!40000 ALTER TABLE `panel_outcometype2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panel_package`
--

DROP TABLE IF EXISTS `panel_package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panel_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pledge_number` varchar(255) NOT NULL,
  `first_date` datetime(6) DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `notes` longtext,
  `amount` decimal(19,2) NOT NULL,
  `percent` decimal(10,2) NOT NULL,
  `debt_amount` decimal(19,2) NOT NULL,
  `status` varchar(100) NOT NULL,
  `product_amount` decimal(10,2) NOT NULL,
  `pay_date` date DEFAULT NULL,
  `date` datetime(6) NOT NULL,
  `delay_day` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `department_id` int(11) NOT NULL,
  `packet_type_id` int(11) NOT NULL,
  `product_id` int(11),
  `product_type_id` int(11),
  `annuitet` tinyint(1) NOT NULL,
  `closed_reason` longtext,
  `total_debt_extra` decimal(19,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `panel_package_currency_id_963abd8f_fk_panel_currency_id` (`currency_id`),
  KEY `panel_package_customer_id_eb51ccc8_fk_panel_customer_id` (`customer_id`),
  KEY `panel_package_department_id_3fa446d8_fk_panel_department_id` (`department_id`),
  KEY `panel_package_packet_type_id_8cd5e03d_fk_panel_packagetype_id` (`packet_type_id`),
  KEY `panel_package_product_id_99688c2e_fk_panel_product_id` (`product_id`),
  KEY `panel_package_product_type_id_d6f688c7_fk_panel_producttype_id` (`product_type_id`),
  CONSTRAINT `panel_package_currency_id_963abd8f_fk_panel_currency_id` FOREIGN KEY (`currency_id`) REFERENCES `panel_currency` (`id`),
  CONSTRAINT `panel_package_customer_id_eb51ccc8_fk_panel_customer_id` FOREIGN KEY (`customer_id`) REFERENCES `panel_customer` (`id`),
  CONSTRAINT `panel_package_department_id_3fa446d8_fk_panel_department_id` FOREIGN KEY (`department_id`) REFERENCES `panel_department` (`id`),
  CONSTRAINT `panel_package_packet_type_id_8cd5e03d_fk_panel_packagetype_id` FOREIGN KEY (`packet_type_id`) REFERENCES `panel_packagetype` (`id`),
  CONSTRAINT `panel_package_product_id_99688c2e_fk_panel_product_id` FOREIGN KEY (`product_id`) REFERENCES `panel_product` (`id`),
  CONSTRAINT `panel_package_product_type_id_d6f688c7_fk_panel_producttype_id` FOREIGN KEY (`product_type_id`) REFERENCES `panel_producttype` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panel_package`
--

LOCK TABLES `panel_package` WRITE;
/*!40000 ALTER TABLE `panel_package` DISABLE KEYS */;
INSERT INTO `panel_package` VALUES (1,'123456','2019-09-11 14:57:44.000000',NULL,'KJKHJH',1000.00,6.00,1000.00,'active',0.00,NULL,'2019-09-11 15:16:36.021696',0,1,2,1,1,NULL,NULL,1,'',0.00),(2,'1234455','2019-09-11 15:22:28.000000',NULL,'',2000.00,4.50,2000.00,'waiting',0.00,NULL,'2019-09-11 15:32:25.715661',0,1,1,1,1,NULL,NULL,1,'',0.00),(3,'3232kj3k','2019-06-12 03:49:15.000000',NULL,'ssd',2334.00,5.00,2334.00,'active',0.00,NULL,'2019-09-12 03:50:26.514718',64,1,2,3,1,NULL,NULL,0,'',350.10);
/*!40000 ALTER TABLE `panel_package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panel_packagedailynote`
--

DROP TABLE IF EXISTS `panel_packagedailynote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panel_packagedailynote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL,
  `date` datetime(6) NOT NULL,
  `package_id` int(11),
  `user_id` int(11),
  PRIMARY KEY (`id`),
  KEY `panel_packagedailynote_package_id_615b8167_fk_panel_package_id` (`package_id`),
  KEY `panel_packagedailynote_user_id_fe828600_fk_base_user_myuser_id` (`user_id`),
  CONSTRAINT `panel_packagedailynote_package_id_615b8167_fk_panel_package_id` FOREIGN KEY (`package_id`) REFERENCES `panel_package` (`id`),
  CONSTRAINT `panel_packagedailynote_user_id_fe828600_fk_base_user_myuser_id` FOREIGN KEY (`user_id`) REFERENCES `base_user_myuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panel_packagedailynote`
--

LOCK TABLES `panel_packagedailynote` WRITE;
/*!40000 ALTER TABLE `panel_packagedailynote` DISABLE KEYS */;
/*!40000 ALTER TABLE `panel_packagedailynote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panel_packageorder`
--

DROP TABLE IF EXISTS `panel_packageorder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panel_packageorder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accepted` tinyint(1) NOT NULL,
  `delivery_date` date DEFAULT NULL,
  `order_date` datetime(6) NOT NULL,
  `notes` longtext,
  `date` datetime(6) NOT NULL,
  `package_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `panel_packageorder_package_id_aebc43fd_fk_panel_package_id` (`package_id`),
  CONSTRAINT `panel_packageorder_package_id_aebc43fd_fk_panel_package_id` FOREIGN KEY (`package_id`) REFERENCES `panel_package` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panel_packageorder`
--

LOCK TABLES `panel_packageorder` WRITE;
/*!40000 ALTER TABLE `panel_packageorder` DISABLE KEYS */;
/*!40000 ALTER TABLE `panel_packageorder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panel_packagepaymentdates`
--

DROP TABLE IF EXISTS `panel_packagepaymentdates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panel_packagepaymentdates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `delay_day` int(11) NOT NULL,
  `paid_date` datetime(6) NOT NULL,
  `pay_date` datetime(6) DEFAULT NULL,
  `month_count` int(11) NOT NULL,
  `day_count` int(11) NOT NULL,
  `pay_amount` decimal(19,2) NOT NULL,
  `calculated_percent` decimal(19,2) NOT NULL,
  `paid_percent` decimal(19,2) NOT NULL,
  `percent_debt` decimal(19,2) NOT NULL,
  `description` varchar(500) NOT NULL,
  `date` datetime(6) NOT NULL,
  `package_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `panel_packagepaymentdate_package_id_11241ce8_fk_panel_package_id` (`package_id`),
  KEY `panel_packagepaymentdate_user_id_c3fe78aa_fk_base_user_myuser_id` (`user_id`),
  CONSTRAINT `panel_packagepaymentdate_package_id_11241ce8_fk_panel_package_id` FOREIGN KEY (`package_id`) REFERENCES `panel_package` (`id`),
  CONSTRAINT `panel_packagepaymentdate_user_id_c3fe78aa_fk_base_user_myuser_id` FOREIGN KEY (`user_id`) REFERENCES `base_user_myuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panel_packagepaymentdates`
--

LOCK TABLES `panel_packagepaymentdates` WRITE;
/*!40000 ALTER TABLE `panel_packagepaymentdates` DISABLE KEYS */;
/*!40000 ALTER TABLE `panel_packagepaymentdates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panel_packageproduct`
--

DROP TABLE IF EXISTS `panel_packageproduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panel_packageproduct` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_amount` decimal(10,2) NOT NULL,
  `date` datetime(6) NOT NULL,
  `package_id` int(11) DEFAULT NULL,
  `product_type_id` int(11),
  PRIMARY KEY (`id`),
  KEY `panel_packageproduct_package_id_0f95252c_fk_panel_package_id` (`package_id`),
  KEY `panel_packagepr_product_type_id_daa10ba9_fk_panel_producttype_id` (`product_type_id`),
  CONSTRAINT `panel_packagepr_product_type_id_daa10ba9_fk_panel_producttype_id` FOREIGN KEY (`product_type_id`) REFERENCES `panel_producttype` (`id`),
  CONSTRAINT `panel_packageproduct_package_id_0f95252c_fk_panel_package_id` FOREIGN KEY (`package_id`) REFERENCES `panel_package` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panel_packageproduct`
--

LOCK TABLES `panel_packageproduct` WRITE;
/*!40000 ALTER TABLE `panel_packageproduct` DISABLE KEYS */;
INSERT INTO `panel_packageproduct` VALUES (1,12.00,'2019-09-11 15:16:36.030564',1,6),(2,12.01,'2019-09-11 15:16:36.032467',1,9),(3,100.00,'2019-09-12 03:50:26.526577',3,6);
/*!40000 ALTER TABLE `panel_packageproduct` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panel_packageproductimage`
--

DROP TABLE IF EXISTS `panel_packageproductimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panel_packageproductimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(100) DEFAULT NULL,
  `date` datetime(6) NOT NULL,
  `package_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `panel_packageproductimag_package_id_bc134639_fk_panel_package_id` (`package_id`),
  CONSTRAINT `panel_packageproductimag_package_id_bc134639_fk_panel_package_id` FOREIGN KEY (`package_id`) REFERENCES `panel_package` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panel_packageproductimage`
--

LOCK TABLES `panel_packageproductimage` WRITE;
/*!40000 ALTER TABLE `panel_packageproductimage` DISABLE KEYS */;
INSERT INTO `panel_packageproductimage` VALUES (1,'photos/2019/9/11/31208f3cbe1d46389971b2b5e5f6effc.jpg','2019-09-11 15:16:36.024673',1),(2,'photos/2019/9/11/eb1f3d8616ab4f93a04905764b0875e1.jpg','2019-09-11 15:32:25.720376',2),(3,'photos/2019/9/12/c643f44d63c14dd6977987982927b980.png','2019-09-12 03:50:26.517718',3),(4,'photos/2019/9/12/40bd9bed19484346b1fc8ac62a00d020.jpeg','2019-09-12 03:50:26.519496',3),(5,'photos/2019/9/12/10eb3492e3dc49c782536b56204b0e82.jpeg','2019-09-12 03:50:26.521812',3);
/*!40000 ALTER TABLE `panel_packageproductimage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panel_packagestatus`
--

DROP TABLE IF EXISTS `panel_packagestatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panel_packagestatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `date` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panel_packagestatus`
--

LOCK TABLES `panel_packagestatus` WRITE;
/*!40000 ALTER TABLE `panel_packagestatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `panel_packagestatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panel_packagetype`
--

DROP TABLE IF EXISTS `panel_packagetype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panel_packagetype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `month_count` int(10) unsigned NOT NULL,
  `description` longtext NOT NULL,
  `removed` tinyint(1) NOT NULL,
  `date` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panel_packagetype`
--

LOCK TABLES `panel_packagetype` WRITE;
/*!40000 ALTER TABLE `panel_packagetype` DISABLE KEYS */;
INSERT INTO `panel_packagetype` VALUES (1,'10 GUN','ten-daily',0,'BKBK',0,'2019-09-11 14:03:20.864489'),(2,'GUNLUK','daily',0,'BJBJKBJKB',0,'2019-09-11 14:03:54.540569'),(3,'GUNLUK 1 AY SONRA','daily',1,'BHJBHJ',0,'2019-09-11 14:05:53.018642');
/*!40000 ALTER TABLE `panel_packagetype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panel_product`
--

DROP TABLE IF EXISTS `panel_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panel_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `date` datetime(6) NOT NULL,
  `removed` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panel_product`
--

LOCK TABLES `panel_product` WRITE;
/*!40000 ALTER TABLE `panel_product` DISABLE KEYS */;
INSERT INTO `panel_product` VALUES (1,'QIZIL','2019-09-11 14:06:35.734276',1),(2,'AVTOMOBİL','2019-09-11 14:06:53.825396',0),(3,'QIZIL','2019-09-11 14:12:22.725468',0);
/*!40000 ALTER TABLE `panel_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panel_producttype`
--

DROP TABLE IF EXISTS `panel_producttype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panel_producttype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `removed` tinyint(1) NOT NULL,
  `date` datetime(6) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `panel_producttype_product_id_7e00905b_fk_panel_product_id` (`product_id`),
  CONSTRAINT `panel_producttype_product_id_7e00905b_fk_panel_product_id` FOREIGN KEY (`product_id`) REFERENCES `panel_product` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panel_producttype`
--

LOCK TABLES `panel_producttype` WRITE;
/*!40000 ALTER TABLE `panel_producttype` DISABLE KEYS */;
INSERT INTO `panel_producttype` VALUES (1,'585',0,'2019-09-11 14:07:27.359401',1),(2,'750',0,'2019-09-11 14:07:34.052390',1),(3,'900',0,'2019-09-11 14:07:42.071175',1),(4,'BMW',1,'2019-09-11 14:09:11.183657',2),(5,'BMW',0,'2019-09-11 14:09:30.185939',2),(6,'MERCEDES',0,'2019-09-11 14:09:43.141102',2),(7,'750',0,'2019-09-11 14:12:45.894092',3),(8,'585',0,'2019-09-11 14:12:52.218595',3),(9,'375',0,'2019-09-11 14:13:06.935506',3);
/*!40000 ALTER TABLE `panel_producttype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panel_waitingpackage`
--

DROP TABLE IF EXISTS `panel_waitingpackage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panel_waitingpackage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) NOT NULL,
  `date` datetime(6) NOT NULL,
  `update_date` datetime(6) DEFAULT NULL,
  `package_id` int(11) NOT NULL,
  `debt_amount` decimal(19,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `panel_waitingpackage_package_id_272154ae_fk_panel_package_id` (`package_id`),
  CONSTRAINT `panel_waitingpackage_package_id_272154ae_fk_panel_package_id` FOREIGN KEY (`package_id`) REFERENCES `panel_package` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panel_waitingpackage`
--

LOCK TABLES `panel_waitingpackage` WRITE;
/*!40000 ALTER TABLE `panel_waitingpackage` DISABLE KEYS */;
INSERT INTO `panel_waitingpackage` VALUES (1,1,'2019-09-11 15:38:19.119382','2019-09-11 15:38:19.119413',2,2000.00);
/*!40000 ALTER TABLE `panel_waitingpackage` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-14  6:28:15
