﻿import base64
import copy
import hashlib
import random

from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.forms import formset_factory, inlineformset_factory
from django.http import JsonResponse, Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from dateutil import relativedelta as rdelta
# Create your views here.
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils import timezone
from flynsarmy_paginator import FlynsarmyPaginator

from general.views import base_auth
from panel.forms import CustomerForm, PackageForm, EmployeeForm, EmployeeEditForm, CurrencyForm, DepartmentForm, \
    CustomerSearchForm, CustomerNotesForm, IncomeOutcomeForm, PackageEditForm, PackageTypeForm, PackageSearchForm, \
    PackagePaymentPayDateForm, PackageOrderForm, PackagePaymentPayForm, EmployeePermitForm, ProductForm, \
    ProductTypeForm, SalaryForm, PackageProductTypeForm, StatisticDailyForm, IncomeOutcomeMainForm, \
    PackageClosedReasonForm, IncomeOutcomeSearchForm, CustomerMainSearchForm, AttachmentForm, AnnuitetCalculatedForm, \
    WaitingPackageEditForm, PackageAdminEditForm
from panel.models import *
from django.utils.translation import ugettext as _

GUser = get_user_model()

from uuid import getnode as get_mac

@login_required(login_url='base-user:login')
def dashboard(request):
    now = timezone.now()
    context = base_auth(req=request)

    mac = get_mac()

    # Customer.objects.all().delete()

    annuitet_calculated_form = AnnuitetCalculatedForm(request.POST or None,initial={
        'amount':'%.1f' % (0),
        'percent':'%.1f' % (0),
        'month_count':3,
        'a_first_date':"{}.{}.{}".format(now.day,now.month,now.year)
    })

    context['annuitet_calculated_form'] = annuitet_calculated_form




    # return '\n'.join(url.format(year) for year in range(2000, 2016))
    context['mac_address'] =  ':'.join(("%012X" % mac)[i:i+2] for i in range(0, 12, 2))
    return render(request, 'panel/dashboard.html', context=context)


def upload_base64(request):
    import uuid
    from base64 import b64decode
    from django.core.files.base import ContentFile
    if request.method=='POST' and request.is_ajax():
        import base64

        image_base64 = request.POST.get('image')
        from django.core.files.base import ContentFile
        format, imgstr = image_base64.split(';base64,')
        ext = format.split('/')[-1]
        image_name = str(uuid.uuid4()) + '.' + ext
        data = ContentFile(base64.b64decode(imgstr), name=image_name)  # You can save this as file

        # open('test.jpg', 'rb')
        # print(image_base64)
        # image_data = b64decode(image_base64)
        # image_name = str(uuid.uuid4()) + ".jpg"
        # data = ContentFile(image_data, image_name)
        return JsonResponse(
            data={
                'image_name':image_name,
                # 'data':data,
            }
        )


@login_required(login_url='base-user:login')
def general_annuitet(request):
    now = timezone.now()
    context = base_auth(req=request)
    print("nese")
    search_form = AnnuitetCalculatedForm(request.POST or None)
    if request.method=='POST' and request.is_ajax() and search_form.is_valid():
        cleaned_data = search_form.cleaned_data
        amount = float(cleaned_data.get('amount',None))
        const_amount = float(cleaned_data.get('amount',None))
        month_count = int(cleaned_data.get('month_count',None))
        percent = float(cleaned_data.get('percent',None))
        first_date = str(cleaned_data.get('a_first_date',None))
        main_html = ''
        try:
            first_date = datetime.datetime.strptime(first_date, '%d.%m.%Y')

            # main_amount =

            k = (percent/100 * pow((1 + percent/100),month_count))/(pow((1 + percent/100),month_count) - 1) if (pow((1 + percent/100),month_count) - 1) != 0 else 0
            middle_amount = k * amount
            print("*(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(*")
            print(middle_amount)
            print("*(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(*")
            list_item = ''

            for i in range(1,month_count+1):
                percent_amount = amount * percent/100
                if month_count == i:
                    main_amount = amount
                else:
                    main_amount = middle_amount - percent_amount
                list_item = "{}{}".format(list_item,
                                        "<tr>"
                                        "<td>{}</td>"
                                        "<td>{}</td>"
                                        "<td>{}</td>"
                                        "<td>{}</td>"
                                        "<td>{}</td>"
                                        "</tr>".format(
                                            (first_date + relativedelta(months=+i)).date(),
                                            round(middle_amount,1),
                                            round(main_amount,1),
                                            round(percent_amount,1),
                                            round(amount,1),
                                        )
                                        )
                amount -= main_amount
            main_html = '{}'.format(
                render_to_string(
                    "panel/packages/include/annunitet/main.html",
                    {
                        'list_html':list_item,
                    }
                )
            )
            print(amount)
            general_paid = middle_amount*month_count
            data = {
                    'code':'success',
                    'result':main_html,
                    'middle_amount':round(middle_amount,1),
                    'effective_percent':round((general_paid - const_amount)/const_amount*100/month_count if const_amount != 0 and month_count != 0 else 0,1),
                    'general_percent':round((general_paid-const_amount),1),
                    }
        except:
            data = {
                'code': 'error',
            }
        return JsonResponse(data=data)
    else:
        raise Http404







@login_required(login_url='base-user:login')
def package_annuitet(request,id):
    now = timezone.now()
    context = base_auth(req=request)
    search_form = AnnuitetCalculatedForm(request.POST or None)
    if request.method=='POST' and request.is_ajax() and search_form.is_valid():
        cleaned_data = search_form.cleaned_data
        package_item = get_object_or_404(Package,id=id)
        amount = float(cleaned_data.get('amount',None))
        const_amount = float(cleaned_data.get('amount',None))
        month_count = int(cleaned_data.get('month_count',None))
        percent = float(cleaned_data.get('percent',None))
        # main_amount =
        k = (percent/100 * pow((1 + percent/100),month_count))/(pow((1 + percent/100),month_count) - 1)
        middle_amount = k * amount
        print("*(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(*")
        print(middle_amount)
        print("*(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(**(*(*")
        list_item = ''

        for i in range(1,month_count+1):
            percent_amount = amount * percent/100
            if month_count == i:
                main_amount = amount
            else:
                main_amount = middle_amount - percent_amount
            list_item = "{}{}".format(list_item,
                                    "<tr>"
                                    "<td>{}</td>"
                                    "<td>{}</td>"
                                    "<td>{}</td>"
                                    "<td>{}</td>"
                                    "<td>{}</td>"
                                    "</tr>".format(
                                        (package_item.first_date + relativedelta(months=+i)).date(),
                                        round(middle_amount,1),
                                        round(main_amount,1),
                                        round(percent_amount,1),
                                        round(amount,1),
                                    )
                                    )
            amount -= main_amount
        main_html = '{}'.format(
            render_to_string(
                "panel/packages/include/annunitet/main.html",
                {
                    'list_html':list_item,
                }
            )
        )
        print(amount)
        general_paid = middle_amount*month_count
        data = {'code':'success',
                'result':main_html,
                'middle_amount':round(middle_amount,1),
                'effective_percent':round((general_paid - const_amount)/const_amount*100/month_count if const_amount != 0 and month_count != 0 else 0,1),
                'general_percent':round((general_paid-const_amount),1),
                }
        return JsonResponse(data=data)
    else:
        raise Http404



@login_required(login_url='base-user:login')
def customer_list(request):
    now = timezone.now()
    context = base_auth(req=request)

    search_form = CustomerMainSearchForm(request.POST or None)
    if request.method=='POST' and request.is_ajax() and search_form.is_valid():
        customer_list = Customer.objects.filter().order_by('birthdate')
        cleaned_data = search_form.cleaned_data
        s_full_name = cleaned_data.get('full_name',None)
        s_serial_number = cleaned_data.get('serial_number',None)
        s_fin = cleaned_data.get('fin',None)
        gender = cleaned_data.get('gender',None)
        id_number = cleaned_data.get('id_number',None)
        show_photo = cleaned_data.get('show_photo',None)
        _result_html = ''
        _list_item_html = ''
        if s_full_name:
            customer_list = customer_list.filter(full_name__icontains=s_full_name)
        if s_serial_number:
            customer_list = customer_list.filter(serial_number__icontains=s_serial_number)
        if s_fin:
            customer_list = customer_list.filter(fin__icontains=s_fin)
        if id_number:
            customer_list = customer_list.filter(id_number__icontains=id_number)
        if gender:
            customer_list = customer_list.filter(gender=gender)
        for_i = 0
        if customer_list:
            for customer_item in customer_list.order_by('-date'):
                for_i+=1
                _list_item_html = "{}{}".format(_list_item_html,render_to_string(
                    "panel/customers/include/_listitem.html",
                {
                    "customer_item":customer_item,
                    "for_i":for_i,
                }))

        main_html = "{}".format(
            render_to_string(
                'panel/customers/include/main.html',
                {
                    'list_html': _list_item_html
                }
            )
        )
        data = {
            'main_result':main_html
        }
        return JsonResponse(data=data)
    context['search_form'] = search_form
    return render(request, 'panel/customers/customers.html', context=context)


@login_required(login_url='base-user:login')
def customer_list_type(request,type):
    now = timezone.now()
    context = base_auth(req=request)
    search_form = CustomerMainSearchForm(request.POST or None)

    if type == 'birthday':
        title = 'Ad günü olanlar'
    else:
        title = 'Müştəri siyahısı'

    if request.method=='POST' and request.is_ajax() and search_form.is_valid():
        if type == 'birthday':
            customer_list = Customer.objects.filter(birthdate__day=now.day,birthdate__month=now.month).order_by('birthdate')
        else:
            customer_list = Customer.objects.filter().order_by('birthdate')
        cleaned_data = search_form.cleaned_data
        s_full_name = cleaned_data.get('full_name',None)
        s_serial_number = cleaned_data.get('serial_number',None)
        s_fin = cleaned_data.get('fin',None)
        id_number = cleaned_data.get('id_number',None)
        status = cleaned_data.get('status',None)
        gender = cleaned_data.get('gender',None)
        show_photo = cleaned_data.get('show_photo',None)
        _result_html = ''
        _list_item_html = ''
        if s_full_name:
            customer_list = customer_list.filter(full_name__icontains=s_full_name)
        if s_serial_number:
            customer_list = customer_list.filter(serial_number__icontains=s_serial_number)
        if s_fin:
            customer_list = customer_list.filter(fin__icontains=s_fin)
        if id_number:
            customer_list = customer_list.filter(id_number__icontains=id_number)
        if gender:
            customer_list = customer_list.filter(gender=gender)

        pagination_html = ''

        try:
            page_num = int(request.POST.get('page-num', 2))
        except:
            page_num = 0
        for_i = (page_num-1)*50
        customer_list = customer_list[50 * (page_num - 1):50 * page_num]
        if customer_list:
            for customer_item in customer_list:
                if status:
                    if status == 'saled':
                        if customer_item.get_package_status()[3] != status:
                            continue
                    elif customer_item.get_package_status()[2] != status:
                        continue
                for_i+=1
                _list_item_html = "{}{}".format(_list_item_html,render_to_string(
                    "panel/customers/include/_listitem.html",
                {
                    "customer_item":customer_item,
                    "show_photo":show_photo,
                    "for_i":for_i,
                }))

            # pagination_html = "{}".format(
            #     render_to_string(
            #         "panel/include/pagination.html",
            #         {
            #             "datas": customer_s,
            #         }
            #     )
            # )
        if page_num < 2:
            main_html = "{}".format(
                render_to_string(
                    'panel/customers/include/main.html',
                    {
                        'list_html': _list_item_html,
                        'show_photo': show_photo,
                    }
                )
            )
        else:
            main_html = _list_item_html
        data = {
            'main_result':"{}".format(main_html)
        }
        return JsonResponse(data=data)
    context['search_form'] = search_form
    context['title'] = title


    return render(request, 'panel/customers/customers.html', context=context)


@login_required(login_url='base-user:login')
def customer_list_ajax(request):
    now = timezone.now()
    context = base_auth(req=request)
    search_form = CustomerSearchForm(request.POST or None)
    customer_list = Customer.objects
    if request.method=='POST' and request.is_ajax() and search_form.is_valid():
        cleaned_data = search_form.cleaned_data
        s_full_name = cleaned_data.get('s_full_name',None)
        s_serial_number = cleaned_data.get('s_serial_number',None)
        s_fin = cleaned_data.get('s_fin',None)
        id_number = cleaned_data.get('id_number',None)
        _result_html = ''
        if s_full_name or s_serial_number or s_fin or id_number:
            if s_full_name:
                customer_list = customer_list.filter(full_name__icontains=s_full_name)
            if s_serial_number:
                customer_list = customer_list.filter(serial_number__icontains=s_serial_number)
            if s_fin:
                customer_list = customer_list.filter(fin__icontains=s_fin)
            if id_number:
                customer_list = customer_list.filter(id_number__icontains=id_number)
        else:
            customer_list = customer_list.filter(id=0)
        for_i = 0
        if customer_list:
            for customer_item in customer_list.order_by('-date'):
                for_i+=1
                _result_html = "{}{}".format(_result_html,
                                             '<tr>'
                                                 '<td >{}</td>'
                                                 '<td>{}</td>'
                                                 '<td>{}</td>'
                                                 '<td>{}</td>'
                                                 '<th scope="row">{}</th>'
                                                 '<td>{}</td>'
                                             '</tr>'.format(
                                                 for_i,
                                                 customer_item.full_name,
                                                 customer_item.serial_number,
                                                 customer_item.fin,
                                                 customer_item.id_number,
                                                 '<a id="customer-button-item-{}" onclick="selectCustomer({},\'{}\',)" href="javascript:void(0);" class="customer-button-item btn btn-info">{}</a>'.format(customer_item.id,customer_item.id,customer_item.full_name,_('Select')),
                                             )
                                             )
        data = {'result':_result_html}
        return JsonResponse(data=data)
    else:
        raise Http404



@login_required(login_url='base-user:login')
def customer_create(request):
    now = timezone.now()
    context = base_auth(req=request)
    form = CustomerForm(request.POST or None, request.FILES or None)

    if request.method == 'POST':
        if form.is_valid():
            return HttpResponse(form.data)
            clean_data = form.cleaned_data
            full_name = clean_data.get('full_name')
            department = clean_data.get('department')
            serial_number = clean_data.get('serial_number')
            fin = clean_data.get('fin')
            gender = clean_data.get('gender')
            address = clean_data.get('address')
            contact_number = clean_data.get('contact_number')
            birthdate = clean_data.get('birthdate')
            profile_picture = clean_data.get('profile_picture')
            passport_picture1 = clean_data.get('passport_picture1')
            passport_picture2 = clean_data.get('passport_picture2')

            # random_string = str(random.random()).encode('utf8')

            customer_obj = Customer()
            customer_obj.department_id = department
            customer_obj.full_name = full_name
            customer_obj.serial_number = serial_number
            customer_obj.fin = fin
            customer_obj.gender = gender
            customer_obj.address = address
            customer_obj.contact_number = contact_number
            customer_obj.birthdate = datetime.datetime.strptime(birthdate, '%Y-%m-%d')
            if profile_picture:
                customer_obj.profile_picture = profile_picture
            if passport_picture1:
                customer_obj.passport_picture1 = passport_picture1
            if passport_picture2:
                customer_obj.passport_picture2 = passport_picture2
            customer_obj.save()
            context['success_message'] = _('Has Created Customer account')
            form = CustomerForm()
        else:
            return HttpResponse(form.errors)
    context['form'] = form
    return render(request, 'panel/customers/customer-add-edit.html', context=context)


@login_required(login_url='base-user:login')
def customer_edit(request,id):
    now = timezone.now()
    context = base_auth(req=request)
    # return Http404
    customer_obj = get_object_or_404(Customer,id=id)
    inital_data = {
        'full_name':customer_obj.full_name,
        # 'father_name':customer_obj.father_name,
        'department':customer_obj.department_id,
        'serial_number':customer_obj.serial_number,
        'fin':customer_obj.fin,
        'address':customer_obj.address,
        'gender':customer_obj.gender,
        'contact_number':customer_obj.contact_number,
        'birthdate':customer_obj.birthdate,
        'profile_picture':customer_obj.profile_picture,
        'passport_picture1':customer_obj.passport_picture1,
        'passport_picture2':customer_obj.passport_picture2,
    }
    form = CustomerForm(request.POST or None, request.FILES or None,initial=inital_data)
    if request.method == 'POST':
        if form.is_valid():
            clean_data = form.cleaned_data
            full_name = clean_data.get('full_name')
            # father_name = clean_data.get('father_name')
            department = clean_data.get('department')
            serial_number = clean_data.get('serial_number')
            fin = clean_data.get('fin')
            address = clean_data.get('address')
            gender = clean_data.get('gender')
            contact_number = clean_data.get('contact_number')
            birthdate = clean_data.get('birthdate')
            profile_picture = clean_data.get('profile_picture')
            passport_picture1 = clean_data.get('passport_picture1')
            passport_picture2 = clean_data.get('passport_picture2')

            # random_string = str(random.random()).encode('utf8')


            customer_obj.full_name = full_name
            # customer_obj.father_name = father_name
            customer_obj.department_id = department
            customer_obj.serial_number = serial_number
            customer_obj.fin = fin
            customer_obj.address = address
            customer_obj.gender = gender
            customer_obj.contact_number = contact_number
            customer_obj.birthdate = birthdate
            if profile_picture:
                # print("dkldkfldfkdlf")
                customer_obj.profile_picture = profile_picture
            if passport_picture1:
                customer_obj.passport_picture1 = passport_picture1
            if passport_picture2:
                customer_obj.passport_picture2 = passport_picture2
            customer_obj.save()
            context['success_message'] = _('Has Edited Customer account')
    context['form'] = form
    context['customer_obj'] = customer_obj
    context['customer_phone_numbers'] = CustomerPhoneNumber.objects.filter(customer=customer_obj)

    return render(request, 'panel/customers/customer-add-edit.html', context=context)




from django import forms
from .forms import CustomerModelForm

def add_customer(request):
    '''This function creates a brand new Customer object with related CustomerPhoneNumber objects using inlineformset_factory'''
    customer = Customer()
    context = base_auth(req=request)
    # customer_form = CustomerModelForm(instance=customer) # setup a form for the parent
    CustomerPhoneNumberInlineFormSet = inlineformset_factory(Customer, CustomerPhoneNumber,
                                                             fields=('number',)
                                                             ,widgets={'number': forms.TextInput(attrs={'class': 'form-control', })},extra=1)

    if request.method == "POST":
        customer_form = CustomerModelForm(request.POST, request.FILES)
        formset = CustomerPhoneNumberInlineFormSet(request.POST, request.FILES)

        if customer_form.is_valid():
            # print("----------------------------------------------------")
            created_customer = customer_form.save(commit=True)
            formset = CustomerPhoneNumberInlineFormSet(request.POST, request.FILES, instance=created_customer)
            created_customer.save()
            try:
                if formset.is_valid():
                    image_base64 = request.POST.get('image')
                    from django.core.files.base import ContentFile
                    format, imgstr = image_base64.split(';base64,')
                    ext = format.split('/')[-1]
                    image_name = str(uuid.uuid4()) + '.' + ext
                    data_img = ContentFile(base64.b64decode(imgstr), name=image_name)  # You can save this as file
                    created_customer.profile_picture = data_img
                    created_customer.save()
                    formset.save()
            except:
                pass
            return HttpResponseRedirect(reverse('panel:customer-edit', kwargs={'id':created_customer.id}))
    else:
        # print("****************************************")

        customer_form = CustomerModelForm(instance=customer)
        formset = CustomerPhoneNumberInlineFormSet()
    context['formset'] = formset
    context['customer_form'] = customer_form

    return render(request, "panel/customers/customer-add-edit.html", context)


def edit_customer(request, id):
    '''This function edits an Customer object and its related CustomerPhoneNumber objects using inlineformset_factory'''
    if id:
        customer = Customer.objects.get(pk=id)  # if this is an edit form, replace the customer instance with the existing one
    else:
        customer = Customer()
    context = base_auth(req=request)
    customer_form = CustomerModelForm(instance=customer) # setup a form for the parent
    CustomerPhoneNumberInlineFormSet = inlineformset_factory(Customer, CustomerPhoneNumber,
                                                             fields=('number',)
                                                             ,widgets={'number': forms.TextInput(attrs={'class': 'form-control', })},extra=1)
    formset = CustomerPhoneNumberInlineFormSet(instance=customer)

    if request.method == "POST":
        customer_form = CustomerModelForm(request.POST, request.FILES)

        if id:
            customer_form = CustomerModelForm(request.POST, instance=customer)

        # formset = CustomerPhoneNumberInlineFormSet(request.POST, request.FILES)

        if customer_form.is_valid():
            created_customer = customer_form.save(commit=False)
            formset = CustomerPhoneNumberInlineFormSet(request.POST, request.FILES, instance=created_customer)
            # # print(formset.data.fromkeys())
            # print(formset.data.items())
            created_customer.save()
            # print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
            try:
                if formset.is_valid():
                    from django.core.files.base import ContentFile
                    image_base64 = request.POST.get('image')
                    format, imgstr = image_base64.split(';base64,')
                    ext = format.split('/')[-1]
                    image_name = str(uuid.uuid4()) + '.' + ext
                    data_img = ContentFile(base64.b64decode(imgstr), name=image_name)  # You can save this as file
                    created_customer.profile_picture = data_img
                    formset.save()
            except:
                pass
            return HttpResponseRedirect(reverse('panel:customer-list-type',kwargs={'type':'all'}))

    formset = CustomerPhoneNumberInlineFormSet(instance=customer)
    context['formset'] = formset
    context['customer_form'] = customer_form
    context['id'] = id
    context['customer_obj'] = customer
    return render(request, "panel/customers/customer-add-edit.html", context)






@login_required(login_url='base-user:login')
def product_type_ajax(request):
    import datetime
    now = datetime.datetime.now()
    if request.method == 'POST' and request.is_ajax():
        id = request.POST.get('id')
        datas = ProductType.objects.filter(removed=False,product_id=id)
        result_html = ''
        for data_item in datas:
            result_html = "{}{}".format(result_html,'<option value="{}">{}</option>'.format(data_item.id, data_item.title))
        return JsonResponse(data={'result':result_html})

    else:
        raise Http404



@login_required(login_url='base-user:login')
def package_atachment_upload(request,id):
    import datetime
    now = datetime.datetime.now()
    package_item = get_object_or_404(Package,id=id)
    attachment_form = AttachmentForm(request.POST or None,request.FILES or None)
    if request.method == 'POST':
        if attachment_form.is_valid():
            clean_data = attachment_form.cleaned_data
            attachments = clean_data.get('attachments',None)
            for each in attachments:
                PackageProductImage.objects.create(image=each,package=package_item)
        return HttpResponseRedirect(reverse('panel:package-edit', kwargs={'id':id}))
    else:
        raise Http404




@login_required(login_url='base-user:login')
def package_closed_reason(request,id):
    import datetime
    now = datetime.datetime.now()
    pcr_form = PackageClosedReasonForm(request.POST)
    if request.method == 'POST' and request.is_ajax() and pcr_form.is_valid():
        cleaned_data  = pcr_form.cleaned_data
        closed_reason = cleaned_data.get('closed_reason','')
        package_item = get_object_or_404(Package,id=id)
        package_item.closed_reason = closed_reason
        package_item.save()
        result_html = ''
        return JsonResponse(data={'result':'success'})

    else:
        raise Http404





@login_required(login_url='base-user:login')
def package_list(request):
    now = timezone.now()
    user = request.user
    context = base_auth(req=request)
    form = PackageForm(request.POST or None, request.FILES or None)
    search_form = CustomerSearchForm(request.POST or None)
    package_search_form = PackageSearchForm(request.POST or None)
    context['form'] = form
    context['search_form'] = search_form
    context['package_search_form'] = package_search_form
    list_html = ''
    if request.method == 'POST' and request.is_ajax():
        if package_search_form.is_valid():
            message_code = 0
            cleaned_data = package_search_form.cleaned_data
            packages = Package.objects.filter()
            customer_name = cleaned_data.get('customer_name','')
            fin = cleaned_data.get('s_fin','')
            serial_number = cleaned_data.get('s_serial_number','')
            package_id = cleaned_data.get('package_id',None)
            currency = cleaned_data.get('currency',None)
            start_date = cleaned_data.get('start_date',None)
            end_date = cleaned_data.get('end_date',None)
            percent = cleaned_data.get('percent',None)
            amount_min = cleaned_data.get('amount_min',None)
            amount_max = cleaned_data.get('amount_max',None)
            status = cleaned_data.get('status',None)
            annuitet = cleaned_data.get('annuitet',None)
            department = cleaned_data.get('department',None)

            if package_id:
                packages = packages.filter(pledge_number__icontains=package_id)
            if currency:
                packages = packages.filter(currency_id=currency)
            if percent:
                packages = packages.filter(percent=percent)
            if annuitet:
                packages = packages.filter(annuitet=annuitet)
            if status:
                if status == 'paided-all':
                    packages = packages.filter(status__in=['paided','ordered','closed'])
                else:
                    packages = packages.filter(status=status)
            if user.usertype == 2:
                department = user.department_id
            if department:
                packages = packages.filter(department_id=department)




            if customer_name:
                packages = packages.filter(customer__full_name__icontains=customer_name)
            if fin:
                packages = packages.filter(customer__fin__icontains=fin)
            if serial_number:
                packages = packages.filter(customer__serial_number__icontains=serial_number)

            if amount_min:
                packages = packages.filter(amount__gte=amount_min)
            if amount_max:
                packages = packages.filter(amount__lte=amount_max)

            if start_date:
                packages = packages.filter(first_date__gte=start_date)
            if end_date:
                packages = packages.filter(first_date__lte=end_date)




            packages= packages.order_by('-date')
            general_amount = 0.0
            general_debt_amount = 0.0
            general_paid_debt_amount = 0.0
            general_delay_day = 0
            try:
                page_num = int(request.POST.get('page-num', 2))
            except:
                page_num = 0

            packages_list = packages[50*(page_num-1):50 * page_num]

            if user.usertype == 1 and page_num < 2:
                for package_item in packages:
                    general_amount += float(package_item.amount)
                    general_delay_day += package_item.delay_day
                    general_debt_amount += float(package_item.debt_amount)
                    general_paid_debt_amount += float(package_item.amount - package_item.debt_amount)


            for package_item in packages_list:

                list_html = '{}{}'.format(list_html,render_to_string(
                    "panel/packages/include/list/list-item.html",
                    {
                        'package_item':package_item,
                        'non_operate':context['non_operate'],
                        'difference': package_item.amount - package_item.debt_amount
                     }
                ))
            if user.usertype == 1 and page_num < 2:
                total_html = "{}".format(
                    render_to_string(
                        'panel/packages/include/total-result-table.html',
                        {
                            'general_amount':"{0:.1f}".format(round(general_amount,1)),
                            'general_debt_amount':"{0:.1f}".format(round(general_debt_amount,1)),
                            'general_paid_debt_amount':"{0:.1f}".format(round(general_paid_debt_amount,1)),
                            'general_delay_day':general_delay_day,
                        }
                    )
                )
            else:
                total_html = ''
            if page_num < 2:
                main_html = "{}".format(
                    render_to_string(
                        'panel/packages/include/list/main.html',
                        {
                            'non_operate':context['non_operate'],
                            'list_html':list_html
                        }
                    )
                )
            else:
                main_html = list_html
            message_code = 1
            data = {
                'total_result':total_html,
                'main_result':main_html,
                'message_code':message_code,
            }
            return JsonResponse(data=data)
        else:
            pass
            # print(package_search_form.errors)
    return render(request, 'panel/packages/packages.html', context=context)





@login_required(login_url='base-user:login')
def package_daily_note_ajax(request,id):
    import datetime
    now = datetime.datetime.now()
    if request.method == 'POST' and request.is_ajax():
        value = request.POST.get('value')
        today_PackageDailyNote = PackageDailyNote.objects.filter(package_id=id,date__year=now.year,date__month=now.month,date__day=now.day).last()
        if today_PackageDailyNote:
            today_PackageDailyNote.content = value
        else:
            today_PackageDailyNote = PackageDailyNote(package_id=id,content=value,user=request.user)
        today_PackageDailyNote.save()
        result_html = ''
        return JsonResponse(data={'result':value})

    else:
        raise Http404






@login_required(login_url='base-user:login')
def package_daily_note_list(request,id):
    import datetime
    now = datetime.datetime.now()
    context = base_auth(req=request)
    package_item = get_object_or_404(Package,id=id)
    packageDailyNotes = PackageDailyNote.objects.filter(package_id=id,)

    # context['page_title'] = "{} - {}".format(package_item.customer.full_name, package_item.pledge_number)
    context['list'] = packageDailyNotes
    context['package_item'] = package_item
    return render(request, 'panel/packages/delay-notes/detail-page.html', context=context)





@login_required(login_url='base-user:login')
def package_list_type(request,type):
    now = timezone.now()
    context = base_auth(req=request)
    form = PackageForm(request.POST or None, request.FILES or None)
    search_form = CustomerSearchForm(request.POST or None)
    package_search_form = PackageSearchForm(request.POST or None)
    context['form'] = form
    context['search_form'] = search_form
    context['package_search_form'] = package_search_form
    list_html = ''
    if type == 'delay':
        page_title = 'Gecikənlər'
        pass
    else:
        raise Http404
    if request.method == 'POST' and request.is_ajax():
        if package_search_form.is_valid():
            message_code = 0
            cleaned_data = package_search_form.cleaned_data
            if type == 'delay':
                packages = Package.objects.filter(Q(delay_day__gt=0) | Q(last_payment_date=now.date())).filter(status='active').order_by('-delay_day')
            else:
                packages = Package.objects.filter()
            customer_name = cleaned_data.get('customer_name','')
            fin = cleaned_data.get('s_fin','')
            serial_number = cleaned_data.get('s_serial_number','')
            package_id = cleaned_data.get('package_id',None)
            currency = cleaned_data.get('currency',None)
            start_date = cleaned_data.get('start_date',None)
            end_date = cleaned_data.get('end_date',None)
            percent = cleaned_data.get('percent',None)
            amount_min = cleaned_data.get('amount_min',None)
            amount_max = cleaned_data.get('amount_max',None)
            status = cleaned_data.get('status',None)
            department = cleaned_data.get('department',None)

            if package_id:
                packages = packages.filter(pledge_number=package_id)
            if currency:
                packages = packages.filter(currency_id=currency)
            if percent:
                packages = packages.filter(percent=percent)
            if status:
                packages = packages.filter(status=status)
            if department:
                packages = packages.filter(department_id=department)




            if customer_name:
                packages = packages.filter(customer__full_name__icontains=customer_name)
            if fin:
                packages = packages.filter(customer__fin__icontains=fin)
            if serial_number:
                packages = packages.filter(customer__serial_number__icontains=serial_number)

            if amount_min:
                packages = packages.filter(amount__gte=amount_min)
            if amount_max:
                packages = packages.filter(amount__lte=amount_max)

            if start_date:
                packages = packages.filter(first_date__gte=start_date)
            if end_date:
                packages = packages.filter(first_date__lte=end_date)




            packages= packages.order_by('-delay_day')
            general_amount = 0.0
            general_debt_amount = 0.0
            general_paid_debt_amount = 0.0
            general_delay_day = 0
            big_data = {
                'more_red':0,
                'red':0,
                'yellow':0,
            }
            for package_item in packages:
                general_amount += float(package_item.amount)
                general_delay_day += package_item.delay_day
                general_debt_amount += float(package_item.debt_amount)
                general_paid_debt_amount += float(package_item.amount - package_item.debt_amount)
                if package_item.delay_day == 0:
                    big_data['yellow'] += 1
                elif package_item.delay_day > 0 and package_item.delay_day <= 31:
                    big_data['red'] += 1
                elif package_item.delay_day > 31:
                    # if
                    big_data['more_red'] += 1

            for package_item in packages[:50]:
                today_daily_note = ''
                today_PackageDailyNote = PackageDailyNote.objects.filter(package=package_item,date__year=now.year,date__month=now.month,date__day=now.day).last()
                if today_PackageDailyNote:
                    today_daily_note = today_PackageDailyNote.content if today_PackageDailyNote.content else ''
                list_html = '{}{}'.format(list_html,render_to_string(
                    "panel/packages/include/list/delay/list.html",
                    {
                        'package_item':package_item,
                        'non_operate':context['non_operate'],
                        'difference': package_item.amount - package_item.debt_amount,
                        'today_daily_note': today_daily_note
                     }
                ))

            total_html = "{}".format(
                render_to_string(
                    'panel/packages/include/total-result-table.html',
                    {
                        'big_data':big_data,
                        'general_amount':"{0:.1f}".format(round(general_amount,1)),
                        'general_debt_amount':"{0:.1f}".format(round(general_debt_amount,1)),
                        'general_paid_debt_amount':"{0:.1f}".format(round(general_paid_debt_amount,1)),
                        'general_delay_day':general_delay_day,
                    }
                )
            )
            main_html = "{}".format(
                render_to_string(
                    'panel/packages/include/list/delay/main.html',
                    {
                        'non_operate':context['non_operate'],
                        'list_html':list_html
                    }
                )
            )
            message_code = 1
            data = {
                'total_result':total_html,
                'main_result':main_html,
                'message_code':message_code,
            }
            return JsonResponse(data=data)
        else:
            pass
            # print(package_search_form.errors)
    context['page_title'] = page_title
    context['type'] = type
    return render(request, 'panel/packages/packages-list-type.html', context=context)




@login_required(login_url='base-user:login')
def package_list_type_infinity(request,type):
    now = timezone.now()
    context = base_auth(req=request)
    form = PackageForm(request.POST or None, request.FILES or None)
    search_form = CustomerSearchForm(request.POST or None)
    package_search_form = PackageSearchForm(request.POST or None)
    context['form'] = form
    context['search_form'] = search_form
    context['package_search_form'] = package_search_form
    list_html = ''
    if type == 'delay':
        page_title = 'Gecikənlər'
        pass
    else:
        raise Http404
    if request.method == 'POST' and request.is_ajax():
        if package_search_form.is_valid():
            message_code = 0
            cleaned_data = package_search_form.cleaned_data
            if type == 'delay':
                packages = Package.objects.filter(Q(delay_day__gt=0) | Q(last_payment_date=now.date())).filter(status='active').order_by('-delay_day')
            else:
                pass
                packages = Package.objects.filter(status='active')
            if request.user.usertype != 1:
                packages = packages.filter(department=request.user.department)
            customer_name = cleaned_data.get('customer_name','')
            fin = cleaned_data.get('s_fin','')
            serial_number = cleaned_data.get('s_serial_number','')
            package_id = cleaned_data.get('package_id',None)
            currency = cleaned_data.get('currency',None)
            start_date = cleaned_data.get('start_date',None)
            end_date = cleaned_data.get('end_date',None)
            percent = cleaned_data.get('percent',None)
            amount_min = cleaned_data.get('amount_min',None)
            amount_max = cleaned_data.get('amount_max',None)
            status = cleaned_data.get('status',None)
            department = cleaned_data.get('department',None)
            # page_num = request.POST.get('page-num',1)

            if package_id:
                packages = packages.filter(pledge_number=package_id)
            if currency:
                packages = packages.filter(currency_id=currency)
            if percent:
                packages = packages.filter(percent=percent)
            if status:
                packages = packages.filter(status=status)
            if department:
                packages = packages.filter(department_id=department)




            if customer_name:
                packages = packages.filter(customer__full_name__icontains=customer_name)
            if fin:
                packages = packages.filter(customer__fin__icontains=fin)
            if serial_number:
                packages = packages.filter(customer__serial_number__icontains=serial_number)

            if amount_min:
                packages = packages.filter(amount__gte=amount_min)
            if amount_max:
                packages = packages.filter(amount__lte=amount_max)

            if start_date:
                packages = packages.filter(first_date__gte=start_date)
            if end_date:
                packages = packages.filter(first_date__lte=end_date)




            packages = packages.order_by('-delay_day')
            general_amount = 0.0
            general_debt_amount = 0.0
            general_paid_debt_amount = 0.0
            general_delay_day = 0
            try:
                page_num = int(request.POST.get('page-num', 2))
            except:
                page_num = 0

            packages_list = packages[50*(page_num-1):50 * page_num]

            if len(packages_list)>0:
                for package_item in packages_list:
                    general_amount += float(package_item.amount)
                    general_delay_day += package_item.delay_day
                    general_debt_amount += float(package_item.debt_amount)
                    general_paid_debt_amount += float(package_item.amount - package_item.debt_amount)

                    today_daily_note = ''
                    today_PackageDailyNote = PackageDailyNote.objects.filter(package=package_item,date__year=now.year,date__month=now.month,date__day=now.day).last()
                    if today_PackageDailyNote:
                        today_daily_note = today_PackageDailyNote.content if today_PackageDailyNote.content else ''
                    list_html = '{}{}'.format(list_html,render_to_string(
                        "panel/packages/include/list/delay/list.html",
                        {
                            'package_item':package_item,
                            'non_operate':context['non_operate'],
                            'difference': package_item.amount - package_item.debt_amount,
                            'today_daily_note': today_daily_note
                         }
                    ))
            message_code = 1
            data = {
                # 'total_result':total_html,
                'main_result':list_html,
                'message_code':message_code,
            }
            return JsonResponse(data=data)
        else:
            pass
            # print(package_search_form.errors)
    context['page_title'] = page_title
    return render(request, 'panel/packages/packages-list-type.html', context=context)





@login_required(login_url='base-user:login')
def package_waiting_list(request,type):
    now = timezone.now()
    context = base_auth(req=request)
    search_form = PackageSearchForm(request.POST or None)
    context['search_form'] = search_form
    if type == 'active':
        context['waiting_packages'] = WaitingPackage.objects.filter(active=True).order_by('-date')
    elif type == 'deactive':
        context['waiting_packages'] = WaitingPackage.objects.filter(active=False).order_by('-date')
    else:
        raise Http404
    context['type'] = type
    result_html = ''
    result_list = []
    for waiting_packages_item in context['waiting_packages']:
        if type == 'active':
                result_html = "{}{}".format(result_html,
                    render_to_string(
                        'panel/packages/waiting/list_item.html',
                        {
                            'waiting_package_item': waiting_packages_item,
                            'type': type,
                            'user': request.user,
                        }
                    )
                )
        else:
            if waiting_packages_item.package_id in result_list:
                pass
            else:
                result_list.append(waiting_packages_item.package_id)
                result_html = "{}{}".format(result_html,
                    render_to_string(
                        'panel/packages/waiting/list_item.html',
                        {
                            'waiting_package_item': waiting_packages_item,
                            'type': type,
                        }
                    )
                )
    context['result_html'] = result_html
    return render(request, 'panel/packages/waiting/waiting-list.html', context=context)



@login_required(login_url='base-user:login')
def package_waiting_edit(request,id):
    now = timezone.now()
    # user = request.user
    user = request.user
    # return HttpResponse(user.usertype)
    if user.usertype != 1:
        raise Http404
    context = base_auth(req=request)
    data_obj = get_object_or_404(WaitingPackage,id=id)
    inital_data = {
        'date':"{}".format(data_obj.date.strftime("%d-%m-%Y %H:%M")),
    }
    form = WaitingPackageEditForm(request.POST or None,initial=inital_data)


    if request.method == 'POST':
        if form.is_valid():
            clean_data = form.cleaned_data
            date_str = clean_data.get('date',None)
            try:
                data_obj.date=datetime.datetime.strptime(date_str, '%d-%m-%Y %H:%M')
                # data_obj.date=datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S.%f')
                data_obj.save()
                context['success_message'] = _('Tarix dəyişdi')
                context['code_message'] = 1
            except:
                context['success_message'] = _('Tarixi Düzgün daxil edin')
                context['code_message'] = 0

            # employee_form = EmployeeEditForm(user_id=user_obj.id)
        # else:
        #     return HttpResponse(employee_form.errors)

    context['form'] = form
    context['data_obj'] = data_obj
    response = render(request, 'panel/packages/waiting/edit-form.html', context=context)
    return response



@login_required(login_url='base-user:login')
def package_waiting_list_package(request,p_id,type):
    now = timezone.now()
    context = base_auth(req=request)
    package_item = get_object_or_404(Package,id=p_id)
    search_form = CustomerSearchForm(request.POST or None)
    context['search_form'] = search_form
    if type == 'active':
        context['waiting_packages'] = WaitingPackage.objects.filter(active=True,package_id=p_id).order_by('-date')
    elif type == 'deactive':
        context['waiting_packages'] = WaitingPackage.objects.filter(active=False,package_id=p_id).order_by('-date')
    else:
        raise Http404
    context['type'] = type


    result_html = ''
    for waiting_packages_item in context['waiting_packages']:
        result_html = "{}{}".format(result_html,
            render_to_string(
                'panel/packages/waiting/list_item.html',
                {
                    'waiting_package_item': waiting_packages_item,
                    'type': type,
                }
            )
        )

    context['package_item'] = package_item
    context['result_html'] = result_html
    context['title'] = "{} - {}".format(package_item.id,package_item.customer.full_name)

    return render(request, 'panel/packages/waiting/general.html', context=context)





@login_required(login_url='base-user:login')
def package_ordered_list(request):
    now = timezone.now()
    context = base_auth(req=request)
    search_form = CustomerSearchForm(request.POST or None)
    context['search_form'] = search_form
    context['ordered_packages'] = PackageOrder.objects.filter().order_by('-date')

    return render(request, 'panel/packages/ordereds/list.html', context=context)




@login_required(login_url='base-user:login')
def package_create(request):
    import datetime
    now = datetime.datetime.now()
    context = base_auth(req=request)
    inital_data = {
        # 'first_date':'{}-{}-{}'.format(now.year,now.month,now.day),
        'first_date':'{}'.format(now.strftime("%d-%m-%Y %H:%M:%S")),
        'product_amount':0,
        'status':'active',
    }
    user = request.user
    # print(user.department)
    if user.usertype == 2:
        inital_data.update({'department':user.department_id})

    form = PackageForm(request.POST or None, request.FILES or None,initial=inital_data)
    PackageProductTypeFormSET = formset_factory(PackageProductTypeForm)
    formset = PackageProductTypeFormSET(request.POST or None)
    if request.method == 'POST':
        if form.is_valid() :
            clean_data = form.cleaned_data
            customer_id = clean_data.get('customer_id')
            pledge_number = clean_data.get('pledge_number')
            currency = clean_data.get('currency')

            notes = clean_data.get('notes')

            amount = clean_data.get('amount')
            percent = clean_data.get('percent')

            packet_type = clean_data.get('packet_type')
            department = clean_data.get('department')

            first_date = clean_data.get('first_date')
            annuitet = clean_data.get('annuitet')

            package_obj = Package()
            package_obj.customer_id = customer_id
            package_obj.pledge_number = pledge_number
            package_obj.currency_id = currency

            if user.usertype == 2:
                package_obj.first_date = now
            else:
                package_obj.first_date = datetime.datetime.strptime(first_date, '%d-%m-%Y %H:%M:%S')
            # package_obj.end_date = end_date
            package_obj.notes = notes
            package_obj.user = user
            package_obj.annuitet = annuitet

            package_obj.amount = amount
            package_obj.debt_amount = amount
            package_obj.percent = percent
            package_obj.packet_type_id = packet_type

            if user.usertype == 2:
                package_obj.department = user.department
            else:
                package_obj.department_id = department
            # package_obj.amount_dollar = amount_dollar
            package_obj.status = 'active'
            # attachments = clean_data.get('attachments')



            # package_obj.product_id = product
            # package_obj.product_type_id = product_type
            # package_obj.product_amount = product_amount
            # package_obj.commission_fee = commission_fee
            # package_obj.money_rate = 1.2
            # package_obj.pay_date = pay_date
            package_obj.save()

            IncomeOutcome_obj = IncomeOutcome(
                author_id=package_obj.user_id if package_obj.user else 5,
                user_id=package_obj.user_id if package_obj.user else 5,
                title='{}{}'.format(package_obj.customer.full_name, ' - Faiz odeme'),
                reason='outcome',
                type='credit',
                amount=package_obj.amount,
                currency=package_obj.currency,
                package=package_obj,
                notes='{} {}'.format(package_obj.customer.full_name, ' - Faiz odeme'),
                date=package_obj.first_date,
                department=package_obj.department,
            )
            IncomeOutcome_obj.save()

            # attachments = clean_data.get('attachments')
            # for each in attachments:
            #     PackageProductImage.objects.create(image=each,package=package_obj)
            # print(formset)
            for formset_item in formset:
                # print("----*****----*****----*****----*****----*****----*****----*****----*****----*****----*****----*****")
                if formset_item.is_valid():
                    # formset_item.save()
                    clean_data_fs = formset_item.cleaned_data
                    product_type = clean_data_fs.get('product_type')
                    product_amount = clean_data_fs.get('product_amount')
                    # product_type = formset_item.cleaned_data['product_type']
                    # product_amount = formset_item.cleaned_data['product_amount']
                    try:
                        product_type_obj = PackageProduct()
                        # print("$$$$$$$$$$$$$$$$$$$$$$$$$$ product_type = {} ----- product_amount = {}".format(product_type,product_amount))
                        product_type_obj.product_type_id = product_type
                        product_type_obj.package = package_obj
                        product_type_obj.product_amount = product_amount
                        product_type_obj.save()
                    except:
                        pass
                else:
                    pass
                    # print("-6-6-6-6-6-66-6-6 formset_item.is_valid() 6-6-6-66-6-6-6-6-6-")
                    # print(formset_item.errors)

            url_next = reverse('panel:package-edit',kwargs={'id':package_obj.id})
            return HttpResponseRedirect(url_next)
            # context['success_message'] = _('Has Created Package')
            # form = PackageForm()
        # else:
        #     return HttpResponse(employee_form.errors)
        else:
            pass
            # print("******%$**************")
            # print(form.errors)
            # print(formset.errors)
            # print(formset.non_form_errors())
    context['form'] = form
    context['formset'] = formset
    context['currencies'] = Currency.objects.filter(removed=False)
    context['search_form'] = CustomerSearchForm(request.POST)
    context['products'] = Product.objects.filter(removed=False)

    return render(request, 'panel/packages/forms/package-add-edit.html', context=context)



@login_required(login_url='base-user:login')
def package_edit(request,id):
    import datetime
    now = datetime.datetime.now()
    context = base_auth(req=request)
    user = request.user
    package_item = get_object_or_404(Package,id=id)
    # get_pays = PackagePaymentDates.objects.filter(package=package_item)
    # for it in get_pays:
    #     if it.month_count == 0:
    #         it.month_count = 1
    #         it.save()
    # return HttpResponse(package_item.commission_fee)
    inital_data = {
        'customer_name':package_item.customer.full_name,
        'pledge_number':package_item.pledge_number,
        'currency':package_item.currency_id,
        'first_date':"{}".format(package_item.first_date.strftime("%d-%m-%Y %H:%M")),
        'percent':package_item.percent,
        'packet_type':package_item.packet_type_id,
        'product':package_item.product_id,
        'product_type':package_item.product_type_id,
        'product_amount':package_item.product_amount,
        # 'commission_fee':package_item.commission_fee,
        'status':package_item.status,
        'notes':package_item.notes,
        'amount':package_item.amount,
        'debt_amount':package_item.debt_amount,
        'annuitet':package_item.annuitet,
    }
    if user.usertype == 1 or package_item.first_date.date() == now.date():
        form = PackageAdminEditForm(request.POST or None,request.FILES or None,initial=inital_data)
    else:
        form = PackageEditForm(request.POST or None,request.FILES or None,initial=inital_data)
    attachment_form = AttachmentForm(request.POST or None,request.FILES or None)
    payment_inital_data = {
        'pd_paid_date':now.date(),
        'pd_percent_amount':'%.1f' % (round(package_item.debt_amount * package_item.percent / 100,1)),
        'pd_percent':package_item.percent
    }

    package_payment_pay_date_form = PackagePaymentPayDateForm(request.POST or None,initial=payment_inital_data)
    annuitet_calculated_form = AnnuitetCalculatedForm(request.POST or None,initial={
        'amount':'%.1f' % (package_item.amount),
        'percent':'%.1f' % (package_item.percent),
        'month_count':3,
        'a_first_date':'{}.{}.{}'.format(package_item.get_last_payment_real().day,package_item.get_last_payment_real().month,package_item.get_last_payment_real().year,),
    })

    context['form'] = form
    context['annuitet_calculated_form'] = annuitet_calculated_form
    context['attachment_form'] = attachment_form
    context['package_payment_pay_date_form'] = package_payment_pay_date_form
    if request.method == 'POST':
        if form.is_valid():
            clean_data = form.cleaned_data
            pledge_number = clean_data.get('pledge_number')
            currency = clean_data.get('currency')

            notes = clean_data.get('notes')

            amount = clean_data.get('amount')
            percent = clean_data.get('percent')

            packet_type = clean_data.get('packet_type')
            department = clean_data.get('department')

            first_date = clean_data.get('first_date')
            annuitet = clean_data.get('annuitet')
            package_item.notes = notes
            package_item.annuitet = annuitet

            if user.usertype == 1 or package_item.first_date.date() == now.date():
                package_item.amount = amount
                package_item.percent = percent
                package_item.packet_type_id = packet_type
                package_item.pledge_number = pledge_number
                package_item.currency_id = currency

                if user.usertype == 2:
                    pass
                else:
                    package_item.first_date = datetime.datetime.strptime(first_date, '%d-%m-%Y %H:%M')

                if user.usertype == 2:
                    pass
                    # package_item.department = user.department
                else:
                    package_item.department_id = department

            package_item.save()
        else:
            return HttpResponse(form.errors)
    context['package_item'] = package_item
    waiting_package_item = WaitingPackage.objects.filter(package=package_item,active=True).order_by('-date').first()
    package_payment_dates = PackagePaymentDates.objects.filter(package=package_item)
    context['package_payment_dates'] = package_payment_dates
    context['waiting_package_item'] = waiting_package_item

    context['product_images'] = PackageProductImage.objects.filter(package=package_item)
    context['pcr_form'] = PackageClosedReasonForm(request.POST)


    return render(request, 'panel/packages/forms/edit-page.html', context=context)



import datetime
import calendar

def add_months(sourcedate, months):
    month = sourcedate.month - 1 + months
    year = sourcedate.year + month // 12
    month = month % 12 + 1
    day = min(sourcedate.day, calendar.monthrange(year,month)[1])
    return datetime.date(year, month, day)


@login_required(login_url='base-user:login')
def package_payment_add(request,id):
    payment_inital_data = {}
    package_item = get_object_or_404(Package,id=id)
    package_payment_pay_date_form = PackagePaymentPayForm(request.POST or None)
    message_code = 0
    message = ''
    if request.method == 'POST' and request.is_ajax():
        if package_payment_pay_date_form.is_valid():
            clean_data = package_payment_pay_date_form.cleaned_data
            pd_pay_date = clean_data.get('pd_pay_date')
            pd_paid_date = clean_data.get('pd_paid_date')
            pd_pay_amount = clean_data.get('pd_pay_amount')
            pd_amount = clean_data.get('pd_amount')
            pd_debt_amount = clean_data.get('pd_debt_amount')
            pd_percent_amount = clean_data.get('pd_percent_amount')
            pd_percent = clean_data.get('pd_percent')

            package_payment_dates_obj = PackagePaymentDates()

            package_payment_dates_obj.package_id = id
            package_payment_dates_obj.pay_date = pd_pay_date
            package_payment_dates_obj.paid_date = pd_paid_date
            package_payment_dates_obj.pay_amount = pd_pay_amount
            package_payment_dates_obj.amount = pd_amount
            package_payment_dates_obj.debt_amount = pd_debt_amount
            package_payment_dates_obj.percent_amount = pd_percent_amount
            package_payment_dates_obj.percent = pd_percent
            package_payment_dates_obj.paid = True
            package_payment_dates_obj.save()
            message_code = 1
        else:
            message = str(package_payment_pay_date_form.errors)
    data = {'mesaage':message,'message_code':message_code}
    return JsonResponse(data=data)




from dateutil.relativedelta import relativedelta

@login_required(login_url='base-user:login')
def package_payment_remove(request,id):
    import datetime
    now = timezone.now()
    context = base_auth(req=request)
    if request.method == 'POST' and request.is_ajax():
        pachage_payment_item = PackagePaymentDates.objects.get(id=id)
        package_item = copy.deepcopy(get_object_or_404(Package,id=pachage_payment_item.package_id))
        is_readonly = False
        if request.user.usertype == 2:
            if pachage_payment_item.date + datetime.timedelta(days=1) <= now:
                is_readonly = True
        if is_readonly == False:
            pachage_payment_item.delete()
        paid_amount = 0.0
        percent_amount = 0.0
        package_payment_dates = PackagePaymentDates.objects.filter(package_id=package_item.id).order_by('date')
        for package_payment_date_item in package_payment_dates:
            paid_amount += float(package_payment_date_item.pay_amount)
        package_item.debt_amount = float(package_item.amount ) -  paid_amount

        package_item.last_payment_date = package_item.get_last_payment_real()
        package_item.save()

        return JsonResponse({'result':'success'})
    raise Http404

@login_required(login_url='base-user:login')
def package_payment_new_add(request,id):
    import datetime
    now = timezone.now()
    context = base_auth(req=request)
    package_item = get_object_or_404(Package,id=id)
    main_form = PackagePaymentPayForm(request.POST)
    message_code = 0
    if request.method == 'POST' and request.is_ajax() and main_form.is_valid():
        cleaned_data = main_form.cleaned_data
        pd_delay_day = cleaned_data.get('pd_delay_day')
        pd_paid_date = cleaned_data.get('pd_paid_date')
        pd_month_count = cleaned_data.get('pd_month_count')
        pd_day_count = cleaned_data.get('pd_day_count')
        pd_pay_amount = cleaned_data.get('pd_pay_amount')
        pd_calculated_percent = cleaned_data.get('pd_calculated_percent')
        pd_paid_percent = cleaned_data.get('pd_paid_percent')
        pd_percent_debt = cleaned_data.get('pd_percent_debt')
        pd_description = cleaned_data.get('pd_description')

        package_payment_dates_obj = PackagePaymentDates()

        # print("-------**----------------&^&^&^&^&------------**-------")
        # print(pd_delay_day)
        # print("-----------------------&^&^&^&^&-----------------------")

        package_payment_dates_obj.package = package_item
        package_payment_dates_obj.user = request.user
        if request.user.usertype == 2:
            package_payment_dates_obj.paid_date = now
        else:
            package_payment_dates_obj.paid_date = datetime.datetime.strptime(pd_paid_date, '%d-%m-%Y')
        package_payment_dates_obj.delay_day = pd_delay_day
        package_payment_dates_obj.month_count = pd_month_count
        package_payment_dates_obj.day_count = pd_day_count
        package_payment_dates_obj.pay_amount = pd_pay_amount
        package_payment_dates_obj.calculated_percent = pd_calculated_percent
        package_payment_dates_obj.paid_percent = pd_paid_percent
        package_payment_dates_obj.percent_debt = pd_percent_debt
        package_payment_dates_obj.description = pd_description
        package_payment_dates_obj.save()
        # print("""-------------------------------------------------------------------""")
        # print(package_payment_dates_obj.paid_percent)
        # print(package_payment_dates_obj.pay_amount)
        # print("""-------------------------------------------------------------------""")
        if package_payment_dates_obj.paid_percent > 0:
            IncomeOutcome_obj = IncomeOutcome(
                author = request.user,
                user = request.user,
                title = '{}{}'.format(package_item.customer.full_name,' - Faiz odeme'),
                reason = 'income',
                type = 'percent',
                amount = package_payment_dates_obj.paid_percent,
                package_payment_date = package_payment_dates_obj,
                notes = '{} {}'.format(package_item.customer.full_name,' - Faiz odeme'),
                date=now,
                currency=package_item.currency,
            )
            IncomeOutcome_obj.save()
        if package_payment_dates_obj.pay_amount > 0.0:
            IncomeOutcome_obj = IncomeOutcome(
                author = request.user,
                user = request.user,
                title = '{}{}'.format(package_item.customer.full_name,' - Azaltma etmək'),
                reason = 'income',
                type = 'main',
                amount = package_payment_dates_obj.pay_amount,
                package_payment_date = package_payment_dates_obj,
                notes = '{} {}'.format(package_item.customer.full_name,' - Azaltma etmək'),
                date=now,
                currency=package_item.currency,
            )
            IncomeOutcome_obj.save()
        package_item = get_object_or_404(Package,id=id)
        package_item.last_payment_date = package_item.get_last_payment_real()
        package_item.debt_amount -= pd_pay_amount
        package_item.save()
        # print("-----------------------&^&^&^&^&-------------------")
        # print(package_item.delay_day)
        # print("-----------------------&^&^&^&^&-------------------")
        message_code = 1
        return JsonResponse({
            'result':'success',
            'message_code':message_code,
        })
    else:
        raise Http404



@login_required(login_url='base-user:login')
def package_payment_new_generate(request,id):
    import datetime
    now = timezone.now()
    context = base_auth(req=request)
    user = request.user
    package_item = get_object_or_404(Package,id=id)
    package_payment_dates = PackagePaymentDates.objects.filter(package_id=id).order_by('date')
    if request.method == 'POST' and request.is_ajax():
        paid_amount_no_paid_date = package_item.first_date.date()
        month_count = 0
        for package_payment_date_item in package_payment_dates:
            month_count += package_payment_date_item.month_count
        paid_amount_no_paid_date += relativedelta(months=+month_count)
        delay_paid_date = package_item.first_date.date() + relativedelta(months=+(month_count + 1))
        if now.date() > delay_paid_date:

            dif_date = now.date() - delay_paid_date
            # print("now.date() > delay_paid_date 88 ")

            general_dif_date = relativedelta(now.date(),paid_amount_no_paid_date)
            result_html = "{}".format(
                render_to_string(
                    "panel/packages/include/payment-date.html",
                    {
                        'request': request,
                        'user':user,
                        # 'range': range(0,day_count+1),
                        'dif_days':dif_date.days if dif_date.days >= 0 else 0,
                        'should_paid_date':now.date() - datetime.timedelta(days=dif_date.days),
                        'percent':package_item.percent,
                        'dif_days_gen':0,
                        'day_count':0,
                        'percent_debt':"{0:.1f}".format(0.0),
                        'calculated_amount':"{0:.1f}".format(round(float(package_item.debt_amount * package_item.percent/100),1)),
                        'now_date':"{}-{}-{}".format("{:02d}".format(now.day),"{:02d}".format(now.month),"{:04d}".format(now.year),),
                    }
                )
            )
        else:
            general_dif_date = relativedelta(now.date(),paid_amount_no_paid_date)
            dif_date = now.date() - delay_paid_date


            if package_item.packet_type.type != 'ten-daily' and package_item.packet_type.type == 'daily' and (package_item.packet_type.month_count == 0 or (now.date() >= package_item.first_date.date() + relativedelta(months=+package_item.packet_type.month_count))):
                dif_days_gen = general_dif_date.days
                diff_date = delay_paid_date - paid_amount_no_paid_date
                if general_dif_date.days < 0:
                    dif_days_gen = 0
                elif general_dif_date.days == 0:
                    if now.date() == paid_amount_no_paid_date:
                        dif_days_gen = 1

                # # print(
                #     "now.date() - datetime.timedelta(days=dif_date.days)now.date() - datetime.timedelta(days=dif_date.days)")
                # # print(now.date() + datetime.timedelta(days=dif_days_gen))
                # # print(
                #     "now.date() - datetime.timedelta(days=dif_date.days)now.date() - datetime.timedelta(days=dif_date.days)")

                result_html = "{}".format(
                    render_to_string(
                        "panel/packages/include/payment-date.html",
                        {

                            'request': request,
                            'user':user,
                            'range': range(0,diff_date.days),
                            'dif_days':0,
                            'should_paid_date':now.date() + datetime.timedelta(days=dif_days_gen),
                            'dif_days_gen':dif_days_gen,
                            'percent':package_item.percent,
                            'day_count':0,
                            'percent_debt':"{0:.1f}".format(0),
                            'calculated_amount':"{0:.1f}".format(0),
                            'now_date':"{}-{}-{}".format("{:02d}".format(now.day),"{:02d}".format(now.month),"{:04d}".format(now.year),),
                        }
                    )
                )
            else:
                # 10 gunluk
                if general_dif_date.days < 0:
                    dif_days_gen = 0
                elif general_dif_date.days == 0:
                    if now.date() == paid_amount_no_paid_date:
                        dif_days_gen = 10
                    else:
                        dif_days_gen = 0
                elif general_dif_date.days > 0 and general_dif_date.days <= 10:
                    dif_days_gen = 10
                elif general_dif_date.days > 10 and general_dif_date.days <= 20:
                    dif_days_gen = 20
                else:
                    dif_days_gen = 30
                # print(
                #     "now.date() - datetime.timedelta(days=dif_date.days)now.date() - datetime.timedelta(days=dif_date.days)")
                # print(now.date() - datetime.timedelta(days=dif_days_gen))
                # print(
                #     "now.date() - datetime.timedelta(days=dif_date.days)now.date() - datetime.timedelta(days=dif_date.days)")

                result_html = "{}".format(
                    render_to_string(
                        "panel/packages/include/ten-payment-date.html",
                        {

                            'request': request,
                            'user':user,
                            # 'range': range(0,day_count+1),
                            'dif_days':0,
                            'should_paid_date':now.date() + datetime.timedelta(days=dif_days_gen),
                            'dif_days_gen':dif_days_gen,
                            'percent':package_item.percent,
                            'day_count':0,
                            'percent_debt':"{0:.1f}".format(0),
                            'calculated_amount':"{0:.1f}".format(0),
                            'now_date':"{}-{}-{}".format("{:02d}".format(now.day),"{:02d}".format(now.month),"{:04d}".format(now.year),),
                        }
                    )
                )
        return JsonResponse({'result':'success','result_html':result_html})



    raise Http404




#
# @login_required(login_url='base-user:login')
# def package_payment_new_generate(request,id):
#     import datetime
#     now = timezone.now()
#     context = base_auth(req=request)
#     user = request.user
#     package_item = get_object_or_404(Package,id=id)
#     package_payment_dates = PackagePaymentDates.objects.filter(package_id=id).order_by('date')
#     if request.method == 'POST' and request.is_ajax():
#         total_month_count = 0
#         total_day_count = 0
#         result_html = ''
#         paid_amount_delay_date = package_item.first_date.date() + relativedelta(months=+1)
#         paid_amount_no_paid_date = package_item.first_date.date()
#         delay_date = package_item.first_date.date() + relativedelta(months=+1)
#         no_paid_date = package_item.first_date.date()
#         percent_debt_general = 0.0
#         for package_payment_date_item in package_payment_dates:
#             diff_date = (package_item.first_date.date() + relativedelta(months=+(total_month_count + 1))) - (
#                         package_item.first_date.date() + relativedelta(months=+(total_month_count)))
#             # print(
#                 "------------****************-8-8-8-88-88-8")
#             # print(diff_date)
#             # print("------------****************-8-8-8-88-88-8")
#
#             total_month_count += package_payment_date_item.month_count
#             total_day_count += package_payment_date_item.day_count
#             # delay_date += datetime.timedelta(days=package_payment_date_item.day_count) + relativedelta(months=+(package_payment_date_item.month_count))
#             if package_payment_date_item.day_count>0:
#                 no_paid_date += datetime.timedelta(days=package_payment_date_item.day_count)
#                 delay_date += datetime.timedelta(days=package_payment_date_item.day_count)
#                 if package_payment_date_item.pay_amount == 0:
#                     paid_amount_no_paid_date += datetime.timedelta(days=package_payment_date_item.day_count)
#                     paid_amount_delay_date += datetime.timedelta(days=package_payment_date_item.day_count)
#             else:
#                 # print("no_paid_date")
#                 # print(no_paid_date)
#                 no_paid_date = package_item.first_date.date() + relativedelta(months=+(total_month_count))
#                 # print(no_paid_date)
#                 delay_date = package_item.first_date.date() + relativedelta(months=+(total_month_count))
#                 if package_payment_date_item.pay_amount == 0:
#                     paid_amount_no_paid_date += datetime.timedelta(days=package_payment_date_item.day_count)
#                     paid_amount_delay_date += datetime.timedelta(days=total_month_count)
#             # else:
#             #     percent_debt_general =
#         # if(package_payment_dates.last().day_count>0):
#
#
#         last_pay_date = package_payment_dates.last()
#         # print("delay_date.daysdelay_date.daysdelay_date.daysdelay_date.daysdelay_date.daysdelay_date.days")
#         # print(delay_date)
#         # # print(delay_date.month)
#         # # print(delay_date.day)
#         # # print(package_item.first_date.day)
#         # # print("delay_date.daysdelay_date.daysdelay_date.daysdelay_date.daysdelay_date.daysdelay_date.days")
#         diff_date = (delay_date + relativedelta(months=+1)) - delay_date
#         # print(diff_date)
#         # if last_pay_date and delay_date.day != package_item.first_date.day:
#         if diff_date.days > delay_date.day:
#             diff_date = (package_item.first_date.date() + relativedelta(months=+(total_month_count + 1))) - (package_item.first_date.date() + relativedelta(months=+(total_month_count)))
#             # print("&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^%-----------^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^^%^")
#             # print(diff_date)
#             percent_debt = package_item.debt_amount * (package_item.percent*(diff_date.days - delay_date.day)/diff_date.days) / 100
#             # print("&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^^%^")
#             # print("&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^^%^ diff_date.days - delay_date.day = {}".format(diff_date.days - delay_date.day))
#             # print("&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^^%^ diff_date.days = {}".format(diff_date.days))
#             day_count = diff_date.days - delay_date.day
#         else:
#             percent_debt = -1 * package_item.debt_amount * package_item.percent/100
#             diff_date = (package_item.first_date.date() + relativedelta(months=+(total_month_count + 1))) - package_item.first_date.date() + relativedelta(months=+(total_month_count))
#             day_count = 0
#             # print("&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^^%^")
#             # print(diff_date)
#             # print("&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^&^&^&&&^&^&^^%^")
#         dif_date = now.date() - delay_date
#         if (package_item.first_date.date() +  relativedelta(months=+(package_item.packet_type.month_count)) <= no_paid_date) or package_item.packet_type.month_count == 0:
#             result_html = "{}".format(
#                 render_to_string(
#                     "panel/packages/include/payment-date.html",
#                     {
#
#                         'request': request,
#                         'user':user,
#                         'range': range(0,day_count+1),
#                         'dif_days':dif_date.days if dif_date.days >= 0 else 0,
#                         'percent':package_item.percent,
#                         'day_count':day_count,
#                         'percent_debt':"{0:.1f}".format(percent_debt),
#                         'calculated_amount':"{0:.1f}".format(package_item.debt_amount * package_item.percent/100),
#                         'now_date':"{}-{}-{}".format("{:04d}".format(now.year),"{:02d}".format(now.month),"{:02d}".format(now.day),),
#                     }
#                 )
#             )
#         else:
#             result_html = "{}".format(
#                 render_to_string(
#                     "panel/packages/include/ten-payment-date.html",
#                     {
#
#                         'request': request,
#                         'user':user,
#                         'range': range(0,day_count+1),
#                         'dif_days':dif_date.days if dif_date.days >= 0 else 0,
#                         'percent':package_item.percent,
#                         'day_count':day_count,
#                         'percent_debt':"{0:.1f}".format(percent_debt),
#                         'calculated_amount':"{0:.1f}".format(package_item.debt_amount * package_item.percent/100),
#                         'now_date':"{}-{}-{}".format("{:04d}".format(now.year),"{:02d}".format(now.month),"{:02d}".format(now.day),),
#                     }
#                 )
#             )
#         return JsonResponse({'result':'success','result_html':result_html})
#     raise Http404


@login_required(login_url='base-user:login')
def package_payment_new_check(request,id):
    import datetime
    context = base_auth(req=request)
    package_item = get_object_or_404(Package,id=id)
    package_payment_dates = PackagePaymentDates.objects.filter(package_id=id).order_by('date')
    if request.method == 'POST' and request.is_ajax():
        # general_month_count = 0


        pd_pay_amount = float(request.POST.get('pd_pay_amount',0.0))
        pd_month_count = int(request.POST.get('pd_month_count',0))
        pd_paid_date = request.POST.get('pd_paid_date',None)
        if request.user.usertype == 2:
            now = timezone.now()
        else:
            now = datetime.datetime.strptime(pd_paid_date,'%d-%m-%Y')
        # print("-------------------------------------------------&&&------------------------")
        # print(now)
        # print("-------------------------------------------------&&&------------------------")
        # # print("delay_date.daysdelay_date.daysdelay_date.daysdelay_date.daysdelay_date.daysdelay_date.days")
        # # print("delay_date.daysdelay_date.daysdelay_date.daysdelay_date.daysdelay_date.daysdelay_date.days")
        # # print("delay_date.daysdelay_date.daysdelay_date.daysdelay_date.daysdelay_date.daysdelay_date.days")
        # # print("delay_date.daysdelay_date.daysdelay_date.daysdelay_date.daysdelay_date.daysdelay_date.days")
        #
        paid_amount = 0.0
        percent_amount = 0.0
        paid_percent_amount = 0.0
        calculated_percent_amount = 0.0

        month_count = 0
        paid_amount_no_paid_date = package_item.first_date.date()
        for package_payment_date_item in package_payment_dates:
            calculated_percent_amount += float(package_payment_date_item.calculated_percent)
            paid_percent_amount += float(package_payment_date_item.paid_percent)
            month_count += package_payment_date_item.month_count
        percent_debt_amount = calculated_percent_amount - paid_percent_amount

        paid_amount_no_paid_date += relativedelta(months=+month_count)
        delay_paid_date = package_item.first_date.date() + relativedelta(months=+(month_count + 1))

        # print("----------------------------------------------------------------------")
        # print(percent_debt_amount)
        # print("----------------------------------------------------------------------")


        if now.date() > delay_paid_date:
            dif_date = now.date() - delay_paid_date
            # print("now.date() > delay_paid_date")

            general_dif_date = relativedelta(now.date(), paid_amount_no_paid_date)
            calculated_amount = float(package_item.debt_amount * int(
                pd_month_count) * package_item.percent / 100)
            dif_delay_days = dif_date.days if dif_date.days >= 0 else 0
            if pd_month_count == general_dif_date.months:
                if package_item.packet_type.type != 'ten-daily' and package_item.packet_type.type == 'daily' and (
                        package_item.packet_type.month_count == 0 or (
                        now.date() >= package_item.first_date.date() + relativedelta(
                        months=+package_item.packet_type.month_count))):
                    dif_days_gen = general_dif_date.days
                    diff_date = delay_paid_date - paid_amount_no_paid_date
                    if general_dif_date.days < 0:
                        dif_days_gen = 0
                    elif general_dif_date.days == 0:
                        if now.date() == paid_amount_no_paid_date:
                            dif_days_gen = 1
                    calculated_amount = pd_pay_amount * \
                                        float(package_item.percent) * (dif_days_gen/diff_date.days) / 100
                    # print("***********************************************")
                else:
                    # 10 gunluk
                    if general_dif_date.days < 0:
                        dif_days_gen = 0
                    elif general_dif_date.days == 0:
                        if now.date() == paid_amount_no_paid_date:
                            dif_days_gen = 10
                        else:
                            dif_days_gen = 0
                    elif general_dif_date.days > 0 and general_dif_date.days <= 10:
                        dif_days_gen = 10
                    elif general_dif_date.days > 10 and general_dif_date.days <= 20:
                        dif_days_gen = 20
                    else:
                        dif_days_gen = 30
                    # print("sdksjdksjdksjdksjdksjdsdksjdksjdksjdksjdksjdsdksjdksjdksjdksjdksjdsdksjdksjdksjdksjdksjd")
                    # print(dif_days_gen)
                    calculated_amount += float(pd_pay_amount * float(package_item.percent) * (dif_days_gen/30) / 100)

            calculated_amount = "{0:.1f}".format(round(calculated_amount, 1))
            
        else:
            dif_delay_days = 0
            general_dif_date = relativedelta(now.date(), paid_amount_no_paid_date)
            if pd_month_count > 0:
                calculated_amount = "{0:.1f}".format(round(package_item.debt_amount * int(
                    pd_month_count) * package_item.percent / 100,1))
            else:
                if package_item.packet_type.type != 'ten-daily' and package_item.packet_type.type == 'daily' and (
                        package_item.packet_type.month_count == 0 or (
                        now.date() >= package_item.first_date.date() + relativedelta(
                        months=+package_item.packet_type.month_count))):
                    dif_days_gen = general_dif_date.days
                    diff_date = delay_paid_date - paid_amount_no_paid_date
                    if general_dif_date.days < 0:
                        dif_days_gen = 0
                    elif general_dif_date.days == 0:
                        if now.date() == paid_amount_no_paid_date:
                            dif_days_gen = 1
                    calculated_amount = "{0:.1f}".format(round(pd_pay_amount *
                         float(package_item.percent) * (dif_days_gen/diff_date.days) / 100,1))

                    # print("***********************************************")
                else:
                    # 10 gunluk
                    if general_dif_date.days < 0:
                        dif_days_gen = 0
                    elif general_dif_date.days == 0:
                        if now.date() == paid_amount_no_paid_date:
                            dif_days_gen = 10
                        else:
                            dif_days_gen = 0
                    elif general_dif_date.days > 0 and general_dif_date.days <= 10:
                        dif_days_gen = 10
                    elif general_dif_date.days > 10 and general_dif_date.days <= 20:
                        dif_days_gen = 20
                    else:
                        dif_days_gen = 30
                    # print("sdksjdksjdksjdksjdksjdsdksjdksjdksjdksjdksjdsdksjdksjdksjdksjdksjdsdksjdksjdksjdksjdksjd")
                    # print(dif_days_gen)
                    calculated_amount = "{0:.1f}".format(round(pd_pay_amount *
                         float(package_item.percent) * (dif_days_gen/30) / 100,1))


        # # print("delay_date.daysdelay_date.daysdelay_date.daysdelay_date.daysdelay_date.daysdelay_date.days")
        # # print("delay_date.daysdelay_date.daysdelay_date.daysdelay_date.daysdelay_date.daysdelay_date.days")
        # # print("delay_date.daysdelay_date.daysdelay_date.daysdelay_date.daysdelay_date.daysdelay_date.days")
        # # print("delay_date.daysdelay_date.daysdelay_date.daysdelay_date.daysdelay_date.daysdelay_date.days")


        # calculated_amount = "{0:.1f}".format(package_item.debt_amount * int(pd_month_count) * package_item.percent/100 + package_item.debt_amount * int(pd_month_count) * package_item.percent/100)

        total_debt = float(package_item.debt_amount) + (-percent_debt_amount)
        return JsonResponse({
            'result':'success',
            'dif_days':dif_delay_days,
            'package_status':package_item.status,
            'general_dif_date':package_item.status,
            'calculated_amount':calculated_amount,
            'pd_month_count':pd_month_count,
            'general_dif_month': general_dif_date.months if general_dif_date.months >= 0 else 0,
            'paid_amount':paid_amount,
            'percent_amount':"%.1f" % round(percent_amount,1),
            'paid_percent_amount':"%.1f" % round(paid_percent_amount,1),
            'percent_debt_amount':(round(percent_debt_amount,1) if percent_debt_amount <= 0 else 0.0),
            'total_debt':"%.1f" % (round(total_debt if total_debt >= 0 else 0.0,1))
        })
    raise Http404

@login_required(login_url='base-user:login')
def package_payment_edit(request,id):
    import datetime
    now = timezone.now()
    context = base_auth(req=request)
    package_item = get_object_or_404(Package,id=id)
    package_payment_dates = PackagePaymentDates.objects.filter(package_id=id).order_by('date')
    active_list = []
    month_count_generaly = 0
    total_amount_gen = package_item.amount
    pay_amount_gen = float(0)
    debt_amount_gen = float(package_item.amount)
    if request.method == 'POST' and request.is_ajax():
        i_counter = 0
        calculated_amount = 0.0
        paid_amount_no_paid_date = package_item.first_date.date()
        if package_payment_dates.count()>0:
            month_count = 0
            last_package_payment_date = package_payment_dates.last()
            for package_payment_date_item in package_payment_dates:
                i_counter += 1
                if package_payment_dates.count() > i_counter:

                    month_count += package_payment_date_item.month_count

            paid_amount_no_paid_date += relativedelta(months=+month_count)
            delay_paid_date = package_item.first_date.date() + relativedelta(months=+(month_count + 1))
            # pd_month_count = 0
            # pd_pay_amount = 0.0

            # paid_date = request.POST.get('paid_date-{}[]'.format(last_package_payment_date.id))
            pd_month_count = int(request.POST.get('month_count-{}[]'.format(last_package_payment_date.id),0))
            paid_date = request.POST.get('paid_date-{}[]'.format(last_package_payment_date.id),'')
            pd_pay_amount = float(request.POST.get('pay_amount-{}[]'.format(last_package_payment_date.id),0.0))
            paid_percent = float(request.POST.get('paid_percent-{}[]'.format(last_package_payment_date.id),0.0))
            description = request.POST.get('description-{}[]'.format(last_package_payment_date.id))
            if now.date() > delay_paid_date:
                dif_days_gen = 0
                dif_date = now.date() - delay_paid_date
                # print("now.date() > delay_paid_date")

                general_dif_date = relativedelta(now.date(), paid_amount_no_paid_date)
                calculated_amount = "{0:.1f}".format(round(package_item.debt_amount * int(
                    pd_month_count) * package_item.percent / 100,1))
                dif_delay_days = dif_date.days if dif_date.days >= 0 else 0
            else:
                dif_delay_days = 0
                # dif_days_gen = 0
                dif_days_gen = 0
                general_dif_date = relativedelta(now.date(), paid_amount_no_paid_date)
                if pd_month_count > 0:
                    calculated_amount = "{0:.1f}".format(round(package_item.debt_amount * int(
                        pd_month_count) * package_item.percent / 100,1))
                else:
                    if package_item.packet_type.type != 'ten-daily' and package_item.packet_type.type == 'daily' and (
                            package_item.packet_type.month_count == 0 or (
                            now.date() >= package_item.first_date.date() + relativedelta(
                            months=+package_item.packet_type.month_count))):
                        dif_days_gen = general_dif_date.days
                        diff_date = delay_paid_date - paid_amount_no_paid_date
                        if general_dif_date.days < 0:
                            dif_days_gen = 0
                        elif general_dif_date.days == 0:
                            if now.date() == paid_amount_no_paid_date:
                                dif_days_gen = 1
                        calculated_amount = "{0:.1f}".format(round(pd_pay_amount *
                             float(package_item.percent) * (dif_days_gen/diff_date.days) / 100,1))

                    else:
                        # 10 gunluk
                        if general_dif_date.days < 0:
                            dif_days_gen = 0
                        elif general_dif_date.days == 0:
                            if now.date() == paid_amount_no_paid_date:
                                dif_days_gen = 10
                            else:
                                dif_days_gen = 0
                        elif general_dif_date.days > 0 and general_dif_date.days <= 10:
                            dif_days_gen = 10
                        elif general_dif_date.days > 10 and general_dif_date.days <= 20:
                            dif_days_gen = 20
                        else:
                            dif_days_gen = 30
                        # print("sdksjdksjdksjdksjdksjdsdksjdksjdksjdksjdksjdsdksjdksjdksjdksjdksjdsdksjdksjdksjdksjdksjd")
                        # print(dif_days_gen)
                        calculated_amount = "{0:.1f}".format(round(pd_pay_amount *
                             float(package_item.percent) * (dif_days_gen/30) / 100,1))


            # print("------------------------------------------------------------------------------------------")
            # print(calculated_amount)
            # print(pd_pay_amount)
            # print(paid_percent)
            # print(description)
            # print("------------------------------------------------------------------------------------------")
            last_package_payment_date.calculated_percent = float(calculated_amount)
            last_package_payment_date.pay_amount = pd_pay_amount

            last_package_payment_date.pay_amount = pd_pay_amount
            if request.user.usertype != 2:
                last_package_payment_date.paid_date = datetime.datetime.strptime(paid_date,'%d-%m-%Y')
            last_package_payment_date.month_count = pd_month_count
            last_package_payment_date.day_count = dif_days_gen
            last_package_payment_date.paid_percent = paid_percent
            last_package_payment_date.percent_debt = -1 * (float(calculated_amount) - float(paid_percent))
            last_package_payment_date.description = description
            # last_package_payment_date.description = calculated_amount - float(paid_percent)
            is_readonly = False
            if request.user.usertype == 2:
                if last_package_payment_date.date + datetime.timedelta(days=1) <= now:
                    is_readonly = True
            if is_readonly == False:
                last_package_payment_date.save()
        return JsonResponse({
            'result':'success',
            # 'res_html':res_html,
            # 'dif_date_day': 0 if dif_date.days < 0 else dif_date.days,
            # 'general_dif_month': general_dif_date.months,
            # 'general_dif_day': general_dif_date.days,
            # 'debt_amount':package_item.debt_amount,
            # 'paid_amount':paid_amount,
            # 'general_month_count':general_month_count,
            # 'percent_amount':"%.2f" % round(percent_amount,2),
            # 'paid_percent_amount':"%.2f" % round(paid_percent_amount,2),
            # 'percent_debt_amount':(percent_debt_amount if percent_debt_amount <= 0 else 0.0),
            # 'total_debt':"%.2f" % (total_debt if total_debt >= 0 else 0.0)
         })
    raise Http404



@login_required(login_url='base-user:login')
def package_payment_refresh(request,id):
    import datetime
    user = request.user

    now = timezone.now()
    context = base_auth(req=request)
    package_item = get_object_or_404(Package,id=id)
    if request.method == 'POST' and request.is_ajax():
        res_html = ''

        package_payment_dates = PackagePaymentDates.objects.filter(package_id=id).order_by('date')
        paid_amount = 0.0
        # percent_amount = 0.0
        paid_percent_amount = 0.0
        calculated_percent_amount = 0.0
        month_count = 0
        paid_amount_no_paid_date = package_item.first_date.date()
        for package_payment_date_item in package_payment_dates:
            paid_amount += float(package_payment_date_item.pay_amount)
            calculated_percent_amount += float(package_payment_date_item.calculated_percent)
            paid_percent_amount += float(package_payment_date_item.paid_percent)
            month_count += package_payment_date_item.month_count
        percent_debt_amount = calculated_percent_amount - paid_percent_amount
        paid_amount_no_paid_date += relativedelta(months=+month_count)
        # print("percent_debt_amountpercent_debt_amountpercent_debt_amountpercent_debt_amountpercent_debt_amount")
        # print(percent_debt_amount)
        # print("percent_debt_amountpercent_debt_amountpercent_debt_amountpercent_debt_amountpercent_debt_amount")

        if percent_debt_amount<=0 and package_item.debt_amount <= 0:
            package_item.status = 'paided'
            package_item.delay_day = 0
        else:
            if package_item.status == 'paided':
                package_item.status = 'active'
        package_item.save()
        package_item_status = package_item.status
        add_button_html = "{}".format(render_to_string(
                "panel/packages/include/package_add_button.html",
                {
                    'package_item_status':package_item_status,
                    'non_operate':context['non_operate'],
                    'package_item':package_item,
                    'is_readonly':True if percent_debt_amount<=0 and package_item.debt_amount <= 0 else False,
                 }
            ))
        res_button_html = "{}".format(render_to_string(
                "panel/packages/include/package_status_button.html",
                {
                    'package_item_status':package_item_status,
                    'package_item':package_item,
                    'percent_debt_amount':percent_debt_amount,
                 }
            )
        )
        i_countet = 0
        for package_payment_date_item in package_payment_dates:
            i_countet += 1
            is_readonly =  True if (percent_debt_amount <= 0 and package_item.debt_amount <= 0) or i_countet < package_payment_dates.count() else False

            if is_readonly != True and user.usertype == 2:
                if package_payment_date_item.date + datetime.timedelta(days=1) <= now:
                    is_readonly = True

            res_html = "{}{}".format(res_html,render_to_string(
                "panel/packages/include/payment-list-item.html",
                {
                    'should_paid_date':package_payment_date_item.paid_date.date() - datetime.timedelta(days=package_payment_date_item.delay_day),
                    # 'should_paid_date':now.date() - datetime.timedelta(days=package_payment_date_item.delay_day),
                    'package_payment_date_item':package_payment_date_item,
                    'is_readonly':is_readonly,
                    'non_operate':context['non_operate'],
                 }
            ))

        # context['dif_date_day'] = 0 if dif_date_day < 0 else dif_date_day

        delay_paid_date = package_item.first_date.date() + relativedelta(months=+(month_count + 1))
        dif_date = now.date() - delay_paid_date
        package_item.delay_day = dif_date.days if dif_date.days >= 0 else 0
        package_item.save()
        package_item = get_object_or_404(Package, id=id)

        general_dif_date = relativedelta(now.date(),paid_amount_no_paid_date)
        # general_paid_dif_date = relativedelta(now.date(),delay_paid_date)
        # total_debt = float(package_item.debt_amount) + (percent_debt_amount)
        total_debt = float(package_item.debt_amount)
        total_debt_extra = 0.0
        # print("dif_date.daysdif_date.daysdif_date.daysdif_date.daysdif_date.days")
        # print(general_dif_date.days)
        # print("dif_date.daysdif_date.daysdif_date.daysdif_date.daysdif_date.days")
        if general_dif_date.days >= 0:

            total_debt_extra = float(package_item.debt_amount) * int(
                general_dif_date.months) * float(package_item.percent) / 100

            paid_add_diff_date = (delay_paid_date + relativedelta(months=+general_dif_date.months)) - (paid_amount_no_paid_date + relativedelta(months=+general_dif_date.months))
            # print("------------------------------------------------------------------------------------")
            # print((delay_paid_date + relativedelta(months=+general_dif_date.months)) - (paid_amount_no_paid_date + relativedelta(months=+general_dif_date.months)))
            # print(package_item.first_date.date() + relativedelta(
            #     months=+package_item.packet_type.month_count))
            # print(now.date())
            # print(
            #         now.date() >= package_item.first_date.date() + relativedelta(
            #     months=+package_item.packet_type.month_count))
            # print(
            #         package_item.packet_type.month_count == 0 or (
            #         now.date() >= package_item.first_date.date() + relativedelta(
            #     months=+package_item.packet_type.month_count)))
            # print(package_item.packet_type.type)
            # print(package_item.packet_type.type != 'ten-daily' and package_item.packet_type.type == 'daily')
            if package_item.packet_type.type != 'ten-daily' and package_item.packet_type.type == 'daily' and (
                    package_item.packet_type.month_count == 0 or (
                    now.date() >= package_item.first_date.date() + relativedelta(
                months=+package_item.packet_type.month_count))):
                dif_days_gen = general_dif_date.days
                if general_dif_date.days < 0:
                    dif_days_gen = 0
                elif general_dif_date.days == 0:
                    if now.date() == paid_amount_no_paid_date:
                        dif_days_gen = 1
                # print("paid_add_diff_datepaid_add_diff_datepaid_add_diff_datepaid_add_diff_datepaid_add_diff_datepaid_add_diff_date")
                # print(paid_add_diff_date)
                # print("paid_add_diff_datepaid_add_diff_datepaid_add_diff_datepaid_add_diff_datepaid_add_diff_datepaid_add_diff_date")
                total_debt_extra += float(package_item.debt_amount) * \
                                    float(package_item.percent) * (
                                                                 dif_days_gen / paid_add_diff_date.days) / 100

                # print("***********************************************")
            else:
                # 10 gunluk
                if general_dif_date.days < 0:
                    dif_days_gen = 0
                elif general_dif_date.days == 0:
                    if now.date() == paid_amount_no_paid_date:
                        dif_days_gen = 10
                    else:
                        dif_days_gen = 0
                elif general_dif_date.days > 0 and general_dif_date.days <= 10:
                    dif_days_gen = 10
                elif general_dif_date.days > 10 and general_dif_date.days <= 20:
                    dif_days_gen = 20
                else:
                    dif_days_gen = 30
                # print("sdksjdksjdksjdksjdksjdsdksjdksjdksjdksjdksjdsdksjdksjdksjdksjdksjdsdksjdksjdksjdksjdksjd")
                # print(dif_days_gen)
                total_debt_extra += float(package_item.debt_amount) * \
                                    float(package_item.percent) * (dif_days_gen / 30) / 100
            # total_debt = float(package_item.debt_amount) + (percent_debt_amount)
        else:
            total_debt = float(package_item.debt_amount)
        package_item.total_debt_extra = total_debt_extra
        package_item.save()
        total_debt += total_debt_extra
        return JsonResponse({
            'result':'success',
                             'res_html':res_html,
                             'package_status':package_item.status,
                             'month_count':month_count,
                             'total_debt_extra':"{0:.1f}".format(round(total_debt_extra,1)),
                             'debt_amount':"%.1f" % round(package_item.debt_amount,1),
                             'dif_date_day': (dif_date.days if dif_date.days >= 0 else 0 ) if package_item.status in ['active','waiting'] else 0,
                             'paid_amount': "%.1f" % round(paid_amount,1),
                             'general_dif_day': (general_dif_date.days if general_dif_date.days >= 0 else 0) if package_item.status in ['active','waiting'] else 0,
                             'general_dif_month': (general_dif_date.months if general_dif_date.months >= 0 else 0) if package_item.status in ['active','waiting'] else 0,
                             'percent_amount': "%.1f" % round(calculated_percent_amount,1),
                             'paid_percent_amount': "%.1f" % round(paid_percent_amount,1),
                             'package_item_status': package_item_status,
                             'percent_debt_amount':"%.1f" % round((-1 * percent_debt_amount),1),
                             'next_month_debt_amount':"{0:.1f}".format(round(package_item.debt_amount * package_item.percent/100,1)) if package_item.status in ['active','waiting'] else 0,
                             'total_debt':"%.1f" % round((total_debt if total_debt >= 0 else 0.0) if package_item.status in ['active','waiting'] else 0,1),
                             'res_button_html': res_button_html,
                             'add_button_html': add_button_html,
                             'show_closed_reason': package_item.status != 'active' and package_item.status != 'waiting' and not (package_item.closed_reason != '' and package_item.closed_reason is not None),
                             })
    raise Http404





@login_required(login_url='base-user:login')
def salary_list(request):
    now = timezone.now()
    context = base_auth(req=request)
    employee_list = GUser.objects.filter(usertype=2).order_by('first_name')
    context['employee_list'] = employee_list
    salary_search_form = SalaryForm(request.POST or None)
    context['salary_search_form'] = salary_search_form
    list_html = ''
    if request.method == 'POST' and salary_search_form.is_valid() and request.is_ajax():
        for employee_list_item in employee_list:


            list_html = '{}{}'.format(list_html, render_to_string(
                "panel/employees/salary/include/list_item.html",
                {
                    'employee_item': employee_list_item,
                    'salary': 100,
                    'counter': 1,
                    # 'difference': package_item.amount - package_item.debt_amount
                }
            ))
        total_html = "{}".format(
            render_to_string(
                'panel/employees/salary/include/main.html',
                {
                    'list_html': list_html,
                    # 'general_debt_amount': general_debt_amount,
                    # 'general_paid_debt_amount': general_paid_debt_amount,
                    # 'general_delay_day': general_delay_day,
                }
            )
        )
        data = {'main_result':total_html}
        return JsonResponse(data=data)


    return render(request, 'panel/employees/salary/salary-list.html', context=context)


@login_required(login_url='base-user:login')
def employee_list(request):
    now = timezone.now()
    user = request.user
    if user.usertype == 2:
        raise Http404
    context = base_auth(req=request)
    # context['form'] = PackageForm(request.POST or None, request.FILES or None)
    context['employee_list'] = GUser.objects.filter(usertype=2).order_by('first_name')
    return render(request, 'panel/employees/employee-list.html', context=context)




@login_required(login_url='base-user:login')
def employee_create(request):
    now = timezone.now()
    user = request.user
    if user.usertype == 2:
        raise Http404
    context = base_auth(req=request)
    employee_form = EmployeeForm(request.POST or None, request.FILES or None)
    context['employee_form'] = employee_form


    context = base_auth(req=request)
    if request.method == 'POST':
        if employee_form.is_valid():
            clean_data = employee_form.cleaned_data
            name = clean_data.get('name')
            surname = clean_data.get('surname')
            email = clean_data.get('email')
            phone = clean_data.get('phone')
            password = clean_data.get('password')
            department = clean_data.get('department',None)
            permissions_department = clean_data.get('permissions_department',[])

            random_string = str(random.random()).encode('utf8')
            salt = hashlib.sha1(random_string).hexdigest()[:5]

            # activation_key = hashlib.sha1(salted).hexdigest()
            #
            # key_expires = datetime.datetime.today() + datetime.timedelta(1)
            password = make_password(str(password).strip(), salt=salt)

            user_obj = GUser(first_name=name,last_name=surname,email=email, username=email, phone=phone, password=password, usertype=2, is_active=True,department_id=department)
            user_obj.save()

            for department_item in Department.objects.filter(id__in=permissions_department):
                user_obj.permissions_department.add(department_item)
            user_obj.save()
            context['success_message'] = _('Has Created Employee account')
            employee_form = EmployeeForm()
        # else:
        #     return HttpResponse(employee_form.errors)

    context['employee_form'] = employee_form
    response = render(request, 'panel/employees/employee-add-edit.html', context=context)
    return response




@login_required(login_url='base-user:login')
def employee_edit(request,id):
    now = timezone.now()
    # user = request.user
    user = request.user
    if user.usertype == 2:
        raise Http404
    context = base_auth(req=request)
    user_obj = get_object_or_404(GUser,id=id)
    inital_data = {
        'name':user_obj.first_name,
        'surname':user_obj.last_name,
        'email':user_obj.email,
        'phone':user_obj.phone,
        'department':user_obj.department_id,
        'permissions_department':[x.id for x in user_obj.permissions_department.all()],
    }
    employee_form = EmployeeEditForm(id,request.POST or None, request.FILES or None,initial=inital_data)
    context['employee_form'] = employee_form


    context = base_auth(req=request)
    if request.method == 'POST':
        if employee_form.is_valid():
            clean_data = employee_form.cleaned_data
            department = clean_data.get('department',None)
            permissions_department = clean_data.get('permissions_department',[])
            name = clean_data.get('name',None)
            surname = clean_data.get('surname',None)
            email = clean_data.get('email',None)
            phone = clean_data.get('phone',None)

            password = clean_data.get('password')

            random_string = str(random.random()).encode('utf8')
            salt = hashlib.sha1(random_string).hexdigest()[:5]

            # activation_key = hashlib.sha1(salted).hexdigest()
            #
            # key_expires = datetime.datetime.today() + datetime.timedelta(1)

            user_obj.department_id=department
            user_obj.first_name=name
            user_obj.last_name=surname
            user_obj.email=email
            user_obj.username=email
            user_obj.phone=phone
            if password:
                password = make_password(password, salt=salt)
                user_obj.password=password

            user_obj.permissions_department.clear()
            for department_item in Department.objects.filter(id__in=permissions_department):
                user_obj.permissions_department.add(department_item)
            user_obj.save()
            context['success_message'] = _('Has Edited Employee account')
            # employee_form = EmployeeEditForm(user_id=user_obj.id)
        # else:
        #     return HttpResponse(employee_form.errors)

    context['employee_form'] = employee_form
    context['user_obj'] = user_obj
    response = render(request, 'panel/employees/employee-add-edit.html', context=context)
    return response





@login_required(login_url='base-user:login')
def log_list(request):
    now = timezone.now()
    context = base_auth(req=request)
    user = request.user
    if user.usertype != 1:
        raise Http404
    # context['form'] = PackageForm(request.POST or None, request.FILES or None)
    context['data_list'] = Log.objects.filter().order_by('-date')
    return render(request, 'panel/reports/logs/list.html', context=context)






@login_required(login_url='base-user:login')
def logo_outcome_data_ajax(request):
    now = timezone.now()
    context = base_auth(req=request)
    # context['form'] = PackageForm(request.POST or None, request.FILES or None)
    # result_html = ''
    result_html = '<option >---------</option>'
    if request.method == 'POST' and request.is_ajax():
        type = request.POST.get('type')
        id = request.POST.get('id')
        if type == 'type':
            datas = OutcomeName.objects.filter(type_id=id)
            for data_item in datas:
                result_html = "{}{}".format(
                    result_html,'<option value="{}">{}</option>'.format(data_item.id,data_item.title)
                )
        elif type == 'type_name':
            datas = OutcomeType2.objects.filter(type_name_id=id)
            for data_item in datas:
                result_html = "{}{}".format(
                    result_html,'<option value="{}">{}</option>'.format(data_item.id,data_item.title)
                )
        data = {'message_code':1,'result':result_html}
        return JsonResponse(data=data)

    return render(request, 'panel/reports/logs/list.html', context=context)




@login_required(login_url='base-user:login')
def log_detail(request,id):
    now = timezone.now()
    context = base_auth(req=request)
    # context['form'] = PackageForm(request.POST or None, request.FILES or None)
    context['data_item'] = get_object_or_404(Log,id=id)
    return render(request, 'panel/reports/logs/detail.html', context=context)


@login_required(login_url='base-user:login')
def income_outcome_list(request,o_slug):
    now = timezone.now()
    context = base_auth(req=request)
    # context['form'] = PackageForm(request.POST or None, request.FILES or None)
    if o_slug == 'income':
        title = "Gəlirlər"
    elif o_slug == 'outcome':
        title = "Xərclər"
    else:
        raise Http404
    context['title'] = title
    context['o_slug'] = o_slug

    search_form = IncomeOutcomeSearchForm(request.POST or None)
    if request.method == 'POST':
        if search_form.is_valid():
            clean_data = search_form.cleaned_data
            user_f = clean_data.get('user',None)
            user_to = clean_data.get('user_to',None)
            title = clean_data.get('title',None)
            currency = clean_data.get('currency',None)
            type = clean_data.get('type',None)
            amount_min = clean_data.get('amount_min',None)
            amount_max = clean_data.get('amount_max',None)
            start_date = clean_data.get('start_date',None)
            end_date = clean_data.get('end_date',None)
            if request.user.usertype != 2:
                data_list = IncomeOutcome.objects.filter(reason=o_slug).order_by('-date')
            else:
                data_list = IncomeOutcome.objects.filter(date__year=now.year, date__month=now.month,
                                                                    date__day=now.day, ).filter(reason=o_slug,).filter(Q(department=request.user.department) | Q(package_payment_date__package__department=request.user.department)).order_by('-date')


            if title:
                data_list = data_list.filter(title__icontains=title)
            if type:
                data_list = data_list.filter(type=type)

            if user_f:
                data_list = data_list.filter(user_id=user_f)
            if user_to:
                data_list = data_list.filter(user_to_id=user_to)
            if currency:
                data_list = data_list.filter(Q(currency_id=currency) | Q(package_payment_date__package__currency_id=currency))

            if start_date:
                data_list = data_list.filter(date__gte=start_date)
            if end_date:
                data_list = data_list.filter(date__lte=end_date)
            # if amount_min:
            #     data_list = data_list.filter(Q(amount__gte=amount_min) | Q(package_payment_date__pay_amount__gte=amount_min) | Q(package_payment_date__percent_debt__gte=amount_min))
            # if amount_max:
            #     data_list = data_list.filter(Q(amount__lte=amount_max) | Q(package_payment_date__pay_amount__lte=amount_max) | Q(package_payment_date__percent_debt__lte=amount_max))

            result_part_html = ''

            try:
                page_num = int(request.POST.get('page-num', 2))
            except:
                page_num = 0

            for data_list_item in data_list.order_by('-date')[100*(page_num-1):100 * page_num]:
                result_part_html = "{}{}".format(result_part_html,render_to_string(
                    "panel/reports/income-outcome/include/list-item.html",
                    {
                        'data_item':data_list_item,
                        'o_slug':o_slug,
                    }
                ))
            if page_num < 2:
                result_html = "{}".format(render_to_string(
                    "panel/reports/income-outcome/include/table.html",
                    {
                        'list_html':result_part_html
                    }
                ))
            else:
                result_html = result_part_html
            data = {
                'result':result_html
            }
            return JsonResponse(
                data=data,
            )

    context['search_form'] = search_form
    return render(request, 'panel/reports/income-outcome/list.html', context=context)


@login_required(login_url='base-user:login')
def income_outcome_case(request):
    now = timezone.now()
    context = base_auth(req=request)
    search_form = IncomeOutcomeSearchForm(request.POST or None,initial={
        'start_date':now.strftime("%Y-%m-%d"),
        'end_date':(now).strftime("%Y-%m-%d")
    })
    context['title'] = 'Kassa'
    if request.method == 'POST':
        if search_form.is_valid():
            clean_data = search_form.cleaned_data
            user_f = clean_data.get('user',None)
            user_to = clean_data.get('user_to',None)
            title = clean_data.get('title',None)
            currency = clean_data.get('currency',None)
            type = clean_data.get('type',None)
            amount_min = clean_data.get('amount_min',None)
            amount_max = clean_data.get('amount_max',None)
            start_date = clean_data.get('start_date',None)
            end_date = clean_data.get('end_date',None)

            try:
                page_num = int(request.POST.get('page-num', 2))
            except:
                page_num = 0

            total_start_day = 0.0
            total_end_day = 0.0
            data_list = IncomeOutcome.objects.filter().order_by('-date')
            if page_num < 2:
                select = {'positive': 'sum(if(reason="income",amount, 0))',
                          'negative': 'sum(if(reason="outcome", amount, 0))'}
                summary_day_start = data_list.filter(date__lt=now.date()).extra(select=select).values('positive', 'negative')[0]
                summary_day_end = data_list.filter(date__lt=(now.date() + datetime.timedelta(days=1))).extra(select=select).values('positive', 'negative')[0]
                total_start_day = summary_day_start['positive'] - summary_day_start['negative']
                total_end_day = summary_day_end['positive'] - summary_day_end['negative']
            if request.user.usertype == 2:
                pass
                data_list = data_list.filter(date__year=now.year, date__month=now.month,
                                                                    date__day=now.day,).order_by('-date')
            
            if type:
                data_list = data_list.filter(type=type)
            if currency:
                data_list = data_list.filter(currency_id=currency)
            if request.user.usertype != 2:
                if user_f:
                    data_list = data_list.filter(user_id=user_f)
                if user_to:
                    data_list = data_list.filter(user_to_id=user_to)

                if start_date:
                    data_list = data_list.filter(date__gte=start_date)
                if end_date:
                    data_list = data_list.filter(date__lte=(datetime.datetime.strptime(end_date, '%Y-%m-%d') + datetime.timedelta(days=1)))
            # if amount_min:
            #     data_list = data_list.filter(Q(amount__gte=amount_min) | Q(package_payment_date__pay_amount__gte=amount_min) | Q(package_payment_date__percent_debt__gte=amount_min))
            # if amount_max:
            #     data_list = data_list.filter(Q(amount__lte=amount_max) | Q(package_payment_date__pay_amount__lte=amount_max) | Q(package_payment_date__percent_debt__lte=amount_max))

            result_part_html = ''

            # select = {'positive': 'sum(if(reason="income", if(package_payment_date if(type="") ,amount), 0))',
            select = {'positive': 'sum(if(reason="income",amount, 0))',
                      'negative': 'sum(if(reason="outcome", amount, 0))'}
            summary = data_list.extra(select=select).values('positive', 'negative')[0]
            # summary = {}
            # summary['positive'], summary['negative'] = 0.0, 0.0
            # for item in data_list:
            #     if item.package_payment_date:
            #         summary['positive'] += float(item.package_payment_date.paid_percent + item.package_payment_date.pay_amount)
            #     elif item.package:
            #         summary['negative'] += float(item.package.amount)
            #     elif item.reason == 'income':
            #         summary['positive'] += float(item.amount)
            #     elif item.reason == 'outcome':
            #         summary['negative'] += float(item.amount)


            
            
            positive, negative = summary['positive'], summary['negative']
            # if page_num < 2:
            #     total_html = "{}".format(
            #         render_to_string(
            #             'panel/reports/income-outcome/include/total-result-table.html',
            #             {
            #                 'count':data_list.count(),
            #                 'income':positive,
            #                 'outcome':negative,
            #             }
            #         )
            #     )
            # else:
            #     total_html = ''
            for data_list_item in data_list.order_by('-date')[100*(page_num-1):100 * page_num]:
                o_slug = data_list_item.reason
                result_part_html = "{}{}".format(result_part_html,render_to_string(
                    "panel/reports/income-outcome/include/list-item.html",
                    {
                        'data_item':data_list_item,
                        'o_slug':o_slug,
                    }
                ))
            if page_num < 2:
                result_html = "{}".format(render_to_string(
                    "panel/reports/income-outcome/include/table.html",
                    {
                        'list_html':result_part_html,
                        'count':data_list.count(),
                        'income':positive,
                        'outcome':negative,
                        'total_start_day':total_start_day,
                        'total_end_day':total_end_day,
                    }
                ))
            else:
                result_html = result_part_html
            data = {
                'result':result_html,
                # 'total_html':total_html,
            }
            return JsonResponse(
                data=data,
            )

    context['search_form'] = search_form
    return render(request, 'panel/reports/income-outcome/main-list.html', context=context)



@login_required(login_url='base-user:login')
def income_outcome_create(request,o_slug):
    now = timezone.now()
    context = base_auth(req=request)
    user = request.user
    inital_data = {}
    if user.usertype == 2:
        inital_data.update({'department':user.department_id})
    form = IncomeOutcomeMainForm(request.POST or None,initial=inital_data)
    if o_slug == 'income':
        p_title = "Gəlirlər"
    elif o_slug == 'outcome':
        p_title = "Xərclər"
    else:
        raise Http404
    if request.method == 'POST':
        if form.is_valid():
            clean_data = form.cleaned_data
            user_f = clean_data.get('user',None)
            user_to = clean_data.get('user_to',None)
            title = clean_data.get('title',None)
            type = clean_data.get('type',None)
            amount = clean_data.get('amount',None)
            notes = clean_data.get('notes',None)
            department = clean_data.get('department',None)
            currency = clean_data.get('currency',None)
            date = clean_data.get('date',None)
            outcome_type = clean_data.get('outcome_type',None)
            outcome_name = clean_data.get('outcome_name',None)
            outcome_type2 = clean_data.get('outcome_type2',None)
            # post = form.save(commit=False)
            post = IncomeOutcome(
                user_id=user_f,
                user_to_id=user_to,
                currency_id=currency,
                title=title,
                type=type,
                amount=amount,
                notes=notes,
                date=date,
                outcome_type_id=outcome_type,
                outcome_name_id=outcome_name,
                outcome_type2_id=outcome_type2
            )
            # print("--------------------------------------------------")
            # # print(post.outcome_name)
            # # print(post.outcome_type2)
            # print("--------------------------------------------------")
            post.author = request.user
            if user.usertype == 2:
                post.department = user.department
            else:
                post.department_id = department
            post.reason = o_slug
            # if o_slug == 'outcome':
            #     post.type = 'outcome'
            post.save()
            context['success_message'] = _('Məlumat əlavə edildi')
            form = IncomeOutcomeMainForm()
        # else:
        #     return HttpResponse(employee_form.errors)
    context['title'] = p_title
    context['o_slug'] = o_slug
    context['form'] = form

    return render(request, 'panel/reports/income-outcome/form.html', context=context)

#
#
#
# @login_required(login_url='base-user:login')
# def income_outcome_create(request,o_slug):
#     now = timezone.now()
#     context = base_auth(req=request)
#     user = request.user
#     inital_data = {}
#     if user.usertype == 2:
#         inital_data.update({'department':user.department})
#     form = IncomeOutcomeForm(request.POST or None,initial=inital_data)
#     if o_slug == 'income':
#         title = "Gəlirlər"
#     elif o_slug == 'outcome':
#         title = "Xərclər"
#     else:
#         raise Http404
#     if request.method == 'POST':
#         if form.is_valid():
#             post = form.save(commit=False)
#             # print("--------------------------------------------------")
#             # print(post.outcome_name)
#             # print(post.outcome_type2)
#             # print("--------------------------------------------------")
#             post.author = request.user
#             if user.usertype == 2:
#                 post.department = user.department
#             post.reason = o_slug
#             if o_slug == 'outcome':
#                 post.type = 'outcome'
#             post.save()
#             context['success_message'] = _('Məlumat əlavə edildi')
#             form = IncomeOutcomeForm()
#         # else:
#         #     return HttpResponse(employee_form.errors)
#     context['title'] = title
#     context['o_slug'] = o_slug
#     context['form'] = form
#
#     return render(request, 'panel/reports/income-outcome/form.html', context=context)
#


@login_required(login_url='base-user:login')
def income_outcome_edit(request,o_slug,id):
    user = request.user
    now = timezone.now()
    if user.usertype != 2:
        data_obj = get_object_or_404(IncomeOutcome,id=id)
    else:
        data_obj = get_object_or_404(IncomeOutcome,id=id,date__year=now.year,date__month=now.month,date__day=now.day,department=user.department)

    if user.usertype != 1:
        if data_obj.date + datetime.timedelta(days=1) <= now:
            raise Http404
    context = base_auth(req=request)
    if o_slug == 'income':
        title = "Gəlirlər"
    elif o_slug == 'outcome':
        title = "Xərclər"
    else:
        raise Http404
    form = IncomeOutcomeMainForm(request.POST, initial={
        'user':data_obj.user_id,
        'user_to':data_obj.user_to_id,
        'title':data_obj.title,
        'type':data_obj.type,
        'amount':data_obj.amount,
        'notes':data_obj.notes,
        'department':data_obj.department_id,
        'currency':data_obj.currency_id,
        'date':data_obj.date,
        'outcome_type':data_obj.outcome_type_id,
        'outcome_name':data_obj.outcome_name_id,
        'outcome_type2':data_obj.outcome_type2_id,
    })
    if request.method == "POST":
        if form.is_valid():
            clean_data = form.cleaned_data
            user_f = clean_data.get('user',None)
            user_to = clean_data.get('user_to',None)
            title = clean_data.get('title',None)
            type = clean_data.get('type',None)
            amount = clean_data.get('amount',None)
            notes = clean_data.get('notes',None)
            department = clean_data.get('department',None)
            currency = clean_data.get('currency',None)
            date = clean_data.get('date',None)
            outcome_type = clean_data.get('outcome_type',None)
            outcome_name = clean_data.get('outcome_name',None)
            outcome_type2 = clean_data.get('outcome_type2',None)
            # post = form.save(commit=False)

            data_obj.user_id=user_f
            data_obj.user_to_id=user_to
            data_obj.currency_id=currency
            data_obj.title=title
            data_obj.type=type
            data_obj.amount=amount
            data_obj.notes=notes
            data_obj.date=date
            data_obj.outcome_type_id=outcome_type
            data_obj.outcome_name_id=outcome_name
            data_obj.outcome_type2_id=outcome_type2
            # print("--------------------------------------------------")
            # # print(post.outcome_name)
            # # print(post.outcome_type2)
            # print("--------------------------------------------------")
            if user.usertype == 2:
                data_obj.department = user.department
            else:
                data_obj.department_id = department
            # print("--------------------------------------------------")
            data_obj.save()
            context['success_message'] = _('Has Created Currency account')
    context['o_slug'] = o_slug
    context['title'] = title
    context['form'] = form
    context['data_obj'] = data_obj

    return render(request, 'panel/reports/income-outcome/form.html', context=context)







@login_required(login_url='base-user:login')
def currency_list(request):
    now = timezone.now()
    context = base_auth(req=request)
    user = request.user
    if user.usertype != 1:
        raise Http404
    # context['form'] = PackageForm(request.POST or None, request.FILES or None)
    context['data_list'] = Currency.objects.all().order_by('date')
    return render(request, 'panel/general/currency/list.html', context=context)



@login_required(login_url='base-user:login')
def currency_create(request):
    now = timezone.now()
    user = request.user
    if user.usertype != 1:
        raise Http404
    context = base_auth(req=request)
    form = CurrencyForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            currency = form.save(commit=False)
            currency.save()
            context['success_message'] = _('Has Created Currency account')
            form = CurrencyForm()
        # else:
        #     return HttpResponse(employee_form.errors)
    context['form'] = form

    return render(request, 'panel/general/currency/form.html', context=context)



@login_required(login_url='base-user:login')
def currency_edit(request,id):
    user = request.user
    if user.usertype != 1:
        raise Http404
    data_obj = get_object_or_404(Currency,id=id)
    context = base_auth(req=request)
    if request.method == "POST":
        form = CurrencyForm(request.POST, instance=data_obj)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            context['success_message'] = _('Has Created Currency account')
    else:
        form = CurrencyForm(instance=data_obj)
    context['form'] = form
    context['data_obj'] = data_obj

    return render(request, 'panel/general/currency/form.html', context=context)





@login_required(login_url='base-user:login')
def customer_notes_list(request,c_id):
    now = timezone.now()
    context = base_auth(req=request)
    # context['form'] = PackageForm(request.POST or None, request.FILES or None)
    context['customer_obj'] = get_object_or_404(Customer,id=c_id)
    context['data_list'] = CustomerNote.objects.filter(customer=context['customer_obj']).order_by('date')
    return render(request, 'panel/customers/notes/list.html', context=context)



@login_required(login_url='base-user:login')
def customer_notes_create(request,c_id):
    now = timezone.now()
    context = base_auth(req=request)
    context['customer_obj'] = get_object_or_404(Customer,id=c_id)
    form = CustomerNotesForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            customer_note = form.save(commit=False)
            customer_note.customer = context['customer_obj']
            customer_note.save()
            context['success_message'] = _('Has Created Note account')
            form = CustomerNotesForm()
        # else:
        #     return HttpResponse(employee_form.errors)
    context['form'] = form

    return render(request, 'panel/customers/notes/form.html', context=context)



@login_required(login_url='base-user:login')
def customer_notes_edit(request,c_id,id):
    context = base_auth(req=request)
    customer_obj = get_object_or_404(Customer,id=c_id)
    data_obj = get_object_or_404(CustomerNote,id=id)
    if request.method == "POST":
        form = CustomerNotesForm(request.POST, instance=data_obj)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            context['success_message'] = _('Has Created Note account')
    else:
        form = CustomerNotesForm(instance=data_obj)
    context['form'] = form
    context['data_obj'] = data_obj
    context['customer_obj'] = customer_obj

    return render(request, 'panel/customers/notes/form.html', context=context)





@login_required(login_url='base-user:login')
def department_list(request):
    now = timezone.now()
    context = base_auth(req=request)
    user = request.user
    if user.usertype != 1:
        raise Http404
    # context['form'] = PackageForm(request.POST or None, request.FILES or None)
    context['data_list'] = Department.objects.all().order_by('date')
    return render(request, 'panel/general/department/list.html', context=context)



@login_required(login_url='base-user:login')
def department_create(request):
    now = timezone.now()
    context = base_auth(req=request)
    user = request.user
    if user.usertype != 1:
        raise Http404
    form = DepartmentForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form_val = form.save(commit=False)
            form_val.save()
            context['success_message'] = _('Has Created Department account')
            form = DepartmentForm()
        # else:
        #     return HttpResponse(employee_form.errors)
    context['form'] = form

    return render(request, 'panel/general/department/form.html', context=context)



@login_required(login_url='base-user:login')
def department_edit(request,id):
    user = request.user
    if user.usertype != 1:
        raise Http404
    data_obj = get_object_or_404(Department,id=id)
    context = base_auth(req=request)
    if request.method == "POST":
        form = DepartmentForm(request.POST, instance=data_obj)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            context['success_message'] = _('Filialda dəyişiklik edildi')
    else:
        form = DepartmentForm(instance=data_obj)
    context['form'] = form
    context['data_obj'] = data_obj

    return render(request, 'panel/general/department/form.html', context=context)




@login_required(login_url='base-user:login')
def employee_permit_list(request):
    now = timezone.now()
    context = base_auth(req=request)
    # context['form'] = PackageForm(request.POST or None, request.FILES or None)
    context['data_list'] = EmployeePermit.objects.all().order_by('date')
    return render(request, 'panel/employees/permit/list.html', context=context)



@login_required(login_url='base-user:login')
def employee_permit_create(request):
    now = timezone.now()
    context = base_auth(req=request)
    form = EmployeePermitForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form_val = form.save(commit=False)
            form_val.save()
            context['success_message'] = _('Icazə qeyd edildi')
            form = EmployeePermitForm()
        # else:
        #     return HttpResponse(employee_form.errors)
    context['form'] = form

    return render(request, 'panel/employees/permit/add-edit.html', context=context)



@login_required(login_url='base-user:login')
def employee_permit_edit(request,id):
    data_obj = get_object_or_404(EmployeePermit,id=id)
    context = base_auth(req=request)
    if request.method == "POST":
        form = EmployeePermitForm(request.POST, instance=data_obj)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            context['success_message'] = _('Icazə dəyişdirildi')
    else:
        form = EmployeePermitForm(instance=data_obj)
    context['form'] = form
    context['data_obj'] = data_obj

    return render(request, 'panel/employees/permit/add-edit.html', context=context)



#--------------------------------------------------------------------
#--------------------------------------------------------------------

@login_required(login_url='base-user:login')
def package_type_list(request):
    user = request.user
    if user.usertype != 1:
        raise Http404
    now = timezone.now()
    context = base_auth(req=request)
    # context['form'] = PackageForm(request.POST or None, request.FILES or None)
    context['data_list'] = PackageType.objects.filter(removed=False).order_by('date')
    return render(request, 'panel/general/package_type/list.html', context=context)



@login_required(login_url='base-user:login')
def package_type_create(request):
    user = request.user
    if user.usertype != 1:
        raise Http404
    now = timezone.now()
    context = base_auth(req=request)
    form = PackageTypeForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form_val = form.save(commit=False)
            form_val.save()
            context['success_message'] = _('Has Created Package type account')
            form = PackageTypeForm()
        # else:
        #     return HttpResponse(employee_form.errors)
    context['form'] = form

    return render(request, 'panel/general/package_type/form.html', context=context)



@login_required(login_url='base-user:login')
def package_type_edit(request,id):
    user = request.user
    if user.usertype != 1:
        raise Http404
    data_obj = get_object_or_404(PackageType,id=id)
    context = base_auth(req=request)
    if request.method == "POST":
        form = PackageTypeForm(request.POST, instance=data_obj)
        if form.is_valid():
            post = form.save(commit=False)
            # post.author = request.user
            # post.published_date = timezone.now()
            post.save()
            context['success_message'] = _('Has edited package type account')
    else:
        form = PackageTypeForm(instance=data_obj)
    context['form'] = form
    context['data_obj'] = data_obj

    return render(request, 'panel/general/package_type/form.html', context=context)




@login_required(login_url='base-user:login')
def package_type_remove(request,id):
    user = request.user
    if user.usertype != 1:
        raise Http404
    data_obj = get_object_or_404(PackageType,id=id)
    data_obj.removed = True
    data_obj.save()
    return HttpResponseRedirect(reverse('panel:package_type-list',))




# ------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------
#--------------------------------------------------------------------

@login_required(login_url='base-user:login')
def product_list(request):
    now = timezone.now()
    context = base_auth(req=request)
    # context['form'] = PackageForm(request.POST or None, request.FILES or None)
    context['data_list'] = Product.objects.filter(removed=False).order_by('date')
    return render(request, 'panel/general/product/list.html', context=context)



@login_required(login_url='base-user:login')
def product_create(request):
    now = timezone.now()
    context = base_auth(req=request)
    form = ProductForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form_val = form.save(commit=False)
            form_val.save()
            context['success_message'] = _('Has Created Package type account')
            form = ProductForm()
        # else:
        #     return HttpResponse(employee_form.errors)
    context['form'] = form

    return render(request, 'panel/general/product/form.html', context=context)



@login_required(login_url='base-user:login')
def product_edit(request,id):
    data_obj = get_object_or_404(Product,id=id)
    context = base_auth(req=request)
    if request.method == "POST":
        form = ProductForm(request.POST, instance=data_obj)
        if form.is_valid():
            post = form.save(commit=False)
            # post.author = request.user
            # post.published_date = timezone.now()
            post.save()
            context['success_message'] = _('Has edited package type account')
    else:
        form = ProductForm(instance=data_obj)
    context['form'] = form
    context['data_obj'] = data_obj

    return render(request, 'panel/general/product/form.html', context=context)




@login_required(login_url='base-user:login')
def product_remove(request,id):
    data_obj = get_object_or_404(Product,id=id)
    data_obj.removed = True
    data_obj.save()
    return HttpResponseRedirect(reverse('panel:product-list',))




# ------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------
#--------------------------------------------------------------------

@login_required(login_url='base-user:login')
def product_type_list(request,p_id):
    now = timezone.now()
    context = base_auth(req=request)
    product_item = get_object_or_404(Product,id=p_id)
    # context['form'] = PackageForm(request.POST or None, request.FILES or None)
    context['product_item'] = product_item
    context['data_list'] = ProductType.objects.filter(product_id=p_id).filter(removed=False).order_by('date')
    return render(request, 'panel/general/product/type/list.html', context=context)




@login_required(login_url='base-user:login')
def product_generate(request):
    import datetime
    now = timezone.now()
    context = base_auth(req=request)
    if request.method == 'POST' and request.is_ajax():
        index = request.POST.get('index', 0)
        type = request.POST.get('type', 0)
        result_html = ''
        message_code = 0
        # try:
        if type and index:
            index = int(index)
            list_data_obj = ProductType.objects.filter(product_id=type,removed=False)
            part_html = ''
            for list_data_obj_item in list_data_obj:
                part_html = '{}<option value="{}">{}</option>'.format(part_html,list_data_obj_item.id,list_data_obj_item.title)
            result_html = "{}".format(
                render_to_string(
                    'panel/packages/include/product/form-item.html',
                    {
                        'options_html':part_html,
                        'index':index,
                    }
                )
            )
            message_code = 1
            index += 1
        # except:
        #     pass

        data = {
            'code':message_code,
            'index':index,
            'result_html':result_html
        }
        return JsonResponse(data=data)




@login_required(login_url='base-user:login')
def product_type_create(request,p_id):
    now = timezone.now()
    context = base_auth(req=request)
    product_item = get_object_or_404(Product,id=p_id)
    context['product_item'] = product_item
    form = ProductTypeForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form_val = form.save(commit=False)
            form_val.product = product_item
            form_val.save()
            context['success_message'] = _('Has Created Package type account')
            form = ProductTypeForm()
        # else:
        #     return HttpResponse(employee_form.errors)
    context['form'] = form

    return render(request, 'panel/general/product/type/form.html', context=context)



@login_required(login_url='base-user:login')
def product_type_edit(request,p_id,id):
    data_obj = get_object_or_404(ProductType,id=id)
    product_item = get_object_or_404(Product,id=p_id)
    context = base_auth(req=request)
    if request.method == "POST":
        form = ProductTypeForm(request.POST, instance=data_obj)
        if form.is_valid():
            post = form.save(commit=False)
            # post.author = request.user
            # post.published_date = timezone.now()
            post.save()
            context['success_message'] = _('Has edited package type account')
    else:
        form = ProductTypeForm(instance=data_obj)
    context['form'] = form
    context['data_obj'] = data_obj
    context['product_item'] = product_item

    return render(request, 'panel/general/product/type/form.html', context=context)




@login_required(login_url='base-user:login')
def product_type_remove(request,p_id,id):
    data_obj = get_object_or_404(ProductType,id=id)
    # product_item = get_object_or_404(Product,id=p_id)
    data_obj.removed = True
    data_obj.save()
    return HttpResponseRedirect(reverse('panel:product-type-list',kwargs={'p_id':p_id}))




# ------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------

@login_required(login_url='base-user:login')
def package_next(request,o_slug,id):
    import datetime
    now = timezone.now()
    context = base_auth(req=request)
    package_item = get_object_or_404(Package,id=id)
    if o_slug == 'to-order':
        package_item.status = 'ordered'
        package_item.save()
        PackageOrder.objects.filter(package=package_item).delete()
        package_order_item = PackageOrder.objects.create(package=package_item)
        next_url = reverse('panel:package-ordered-edit', kwargs={'id':package_order_item.id})
        return HttpResponseRedirect(next_url)
    if o_slug == 'from-waiting':
        package_item.status = 'active'
        package_item.save()
        WaitingPackage.objects.filter(package=package_item).update(active=False)
        # package_order_item = PackageOrder.objects.create(package=package_item)
        next_url = reverse('panel:package-edit', kwargs={'id':package_item.id})
        return HttpResponseRedirect(next_url)
    if o_slug == 'to-waiting':
        package_item.status = 'waiting'
        package_item.save()
        WaitingPackage.objects.filter(package=package_item).update(active=False)
        package_order_item = WaitingPackage.objects.create(package=package_item,debt_amount=package_item.debt_amount)
        next_url = reverse('panel:package-edit', kwargs={'id':package_item.id})
        return HttpResponseRedirect(next_url)



@login_required(login_url='base-user:login')
def package_ordered_edit(request,id):
    import datetime
    now = timezone.now()
    ordered_package_item = get_object_or_404(PackageOrder,id=id)
    context = base_auth(req=request)
    if request.method == "POST":
        form = PackageOrderForm(request.POST, instance=ordered_package_item)
        if form.is_valid():
            post = form.save(commit=False)

            post.author = request.user
            post.published_date = timezone.now()
            if post.accepted:
                if ordered_package_item.accepted is False:
                    ordered_package_item.package.end_date = now
                ordered_package_item.package.status = 'closed'
                ordered_package_item.package.save()
            post.save()
            context['success_message'] = _('Məlumat dəyişildi')
    else:
        form = PackageOrderForm(instance=ordered_package_item)
    context['form'] = form
    context['data_obj'] = ordered_package_item
    #




    return render(request, 'panel/packages/ordereds/forms/edit-page.html', context=context)






@login_required(login_url='base-user:login')
def package_product_remove(request,id):
    now = timezone.now()
    if request.method == 'POST' and request.is_ajax():
        # try:
        package_product_image_item = PackageProductImage.objects.get(id=id)
        package_product_image_item.delete()
        message_code = 1
        # except:
        #     message_code = 0
        data = {'message_code':message_code}
        return JsonResponse(data=data)
    else:
        raise Http404


from django.db.models import Q



@login_required(login_url='base-user:login')
def statistic_type(request,type):
    now = timezone.now()
    context = base_auth(req=request)
    form = PackageForm(request.POST or None, request.FILES or None)
    if type == 'monthly':
        search_form = StatisticDailyForm(request.POST or None,initial={'year':now.year,'month':now.month})
    elif type == 'general':
        search_form = StatisticDailyForm(request.POST or None,initial={'year':now.year,'month':now.month})
    else:
        search_form = StatisticDailyForm(request.POST or None,initial={'year':now.year,'month':now.month})
    context['form'] = form
    list_html = ''
    if type == 'monthly':
        page_title = 'Aylıq'
    elif type == 'customers':
        page_title = 'Müştəri'
    elif type == 'general':
        page_title = 'Ümumi'
    elif type == 'monthly-p2':
        page_title = 'monthly-p2'
    else:
        raise Http404
    if request.method == 'POST' and request.is_ajax():
        main_html = ''
        if search_form.is_valid():
            message_code = 0
            cleaned_data = search_form.cleaned_data
            date_day_str = '{}-{}-{}'

            total_result_html = ""
            if type == 'monthly':
                department_list = Department.objects.filter()
                currencies_list = Currency.objects.filter().order_by('-id')
                year = int(cleaned_data.get('year',0))
                month = int(cleaned_data.get('month',0))

                total_result_data = {}

                date_month = datetime.datetime.strptime('1-{}-{}'.format(month,year), '%d-%m-%Y')
                # print("------------------------------------------------------")
                # print(date_month)
                # print("------------------------------------------------------")

                all_result = IncomeOutcome.objects.filter().order_by('date')

                # result_data = {}
                day_count = calendar.monthrange(year=year, month=month)
                before_date_month = datetime.datetime.strptime('1-{}-{}'.format(month,year), '%d-%m-%Y')
                end_date_month = datetime.datetime.strptime('{}-{}-{}'.format(day_count[1],month,year), '%d-%m-%Y')
                currencies_list_i = 0
                month_head_text = ''
                for currencies_list_item in currencies_list:
                    currencies_list_i += 1
                    month_head_text = render_to_string(
                        'panel/statistic/month/head.html',
                        {
                            'date_text': "{}-{}-{}".format(day_count[1],month,year),
                            'department_list': department_list,
                            'currencies_list_item': currencies_list_item,
                        }
                    )
                    generally_must_be_html = ''
                    generally_must_be_result = 0
                    generally_before_month_portfel = ''
                    generally_before_month_portfel_result = 0
                    before_month_portfel = ''
                    before_month_portfel_result = 0

                    end_month_portfel = ''
                    end_month_portfel_result = 0

                    end_month_cash_html = ''
                    end_month_cash_result = 0

                    difference_month_portfel_html = ''
                    difference_month_portfel_result = 0

                    this_month_all_pack_result_html = ''
                    this_month_all_pack_result = 0

                    get_package_dates_html = ''
                    get_package_dates_result = 0

                    get_package_dates_debt_html = ''
                    get_package_dates_debt_result = 0


                    this_month_income_outcome_objs_general_html = ''
                    this_month_income_outcome_objs_general_result = 0

                    this_month_income_outcome_objs_give_office_html = ''
                    this_month_income_outcome_objs_give_office_result = 0

                    this_month_income_outcome_objs_get_office_html = ''
                    this_month_income_outcome_objs_get_office_result = 0
                    color = ['greenyellow','pink',] if currencies_list_item.short_title.lower() == 'usd' else ['pink','greenyellow',]

                    for department_list_item in department_list:

                        before_month_all_packages = Package.objects.filter(currency=currencies_list_item,date__lt=before_date_month,department=department_list_item)
                        before_month_all_payment_dates = PackagePaymentDates.objects.filter(package__currency=currencies_list_item,date__lt=date_month,package__department=department_list_item)

                        end_month_all_packages = Package.objects.filter(currency=currencies_list_item,date__lt=end_date_month,department=department_list_item)
                        end_month_all_payment_dates = PackagePaymentDates.objects.filter(package__currency=currencies_list_item,date__lt=end_date_month,package__department=department_list_item)

                        this_month_all_packages = Package.objects.filter(currency=currencies_list_item,date__lt=(end_date_month+datetime.timedelta(days=1)),date__gte=before_date_month,department=department_list_item)
                        this_month_all_payment_dates = PackagePaymentDates.objects.filter(package__currency=currencies_list_item,date__lt=(end_date_month+datetime.timedelta(days=1)),date__gte=before_date_month,package__department=department_list_item)
                        this_month_income_outcome_objs = IncomeOutcome.objects.filter(currency=currencies_list_item,package_payment_date=None).filter(date__lt=(end_date_month+datetime.timedelta(days=1)),date__gte=before_date_month,
                                                                                        department=department_list_item)
                        end_month_income_outcome_objs = IncomeOutcome.objects.filter(currency=currencies_list_item,package_payment_date=None).filter(date__lt=(end_date_month+datetime.timedelta(days=1)),
                                                                                        department=department_list_item)

    # "*********************************************************************************************************************************************************")
                        before_month_all_result_pack_result = 0.0
                        part_res = {}
                        for before_month_all_packages_item in before_month_all_packages:
                            before_month_all_result_pack_result += float(before_month_all_packages_item.amount)
                            part_res['p-{}'.format(before_month_all_packages_item.id)] = [before_month_all_packages_item.amount, before_month_all_packages_item.percent]
                        # print("------------------------------------------")
                        # print(part_res)
                        # print("------------------------------------------")
                        for before_month_all_payment_dates_item in before_month_all_payment_dates:
                            try:
                                part_res['p-{}'.format(before_month_all_payment_dates_item.package_id.id)][0] -= before_month_all_payment_dates_item.pay_amount
                            except:
                                pass
                            before_month_all_result_pack_result -= float(before_month_all_payment_dates_item.pay_amount)
                        # print("********************************************************")
                        # print(part_res)

                        part_res_count = 0.0
                        for key in part_res.keys():
                            part_res_count += float(part_res[key][0]) * float(part_res[key][1])/100

                        # print("********************************************************")
                        before_month_portfel = "{}{}".format(before_month_portfel,
                                                             "<td>{}</td>".format(before_month_all_result_pack_result))
                        before_month_portfel_result += (before_month_all_result_pack_result)
                        # generally_must_be_result +=
                        generally_before_month_portfel = "{}{}".format(generally_before_month_portfel,
                                                             "<td>{}</td>".format(before_month_all_result_pack_result))
                        generally_before_month_portfel_result += (before_month_all_result_pack_result)

    # "*********************************************************************************************************************************************************")
                        end_month_all_result_pack_result = 0.0
                        for end_month_all_packages_item in end_month_all_packages:
                            end_month_all_result_pack_result += float(end_month_all_packages_item.amount)
                        for end_month_all_payment_dates_item in end_month_all_payment_dates:
                            end_month_all_result_pack_result -= float(end_month_all_payment_dates_item.pay_amount)

                        end_month_portfel = "{}{}".format(end_month_portfel,
                                                             "<td>{}</td>".format(end_month_all_result_pack_result))
                        end_month_portfel_result += (end_month_all_result_pack_result)
    # "*********************************************************************************************************************************************************")
                        difference_month_portfel_html = "{}{}".format(difference_month_portfel_html,
                                                                 "<td>{}</td>".format(
                                                                     (end_month_all_result_pack_result - before_month_all_result_pack_result)))
                        difference_month_portfel_result += (end_month_all_result_pack_result - before_month_all_result_pack_result)
    # "*********************************************************************************************************************************************************")

                        this_month_all_pack_result_part = 0.0
                        for this_month_all_packages_item in this_month_all_packages:
                            this_month_all_pack_result_part += float(this_month_all_packages_item.amount)
                        this_month_all_pack_result += this_month_all_pack_result_part
                        this_month_all_pack_result_html = "{}{}".format(this_month_all_pack_result_html,
                                                             "<td style='background:{};'>{}</td>".format(color[0],this_month_all_pack_result_part))
    # "*********************************************************************************************************************************************************")
                        get_package_dates_result_part = 0.0
                        for this_month_all_payment_dates_item in this_month_all_payment_dates:
                            get_package_dates_result_part += float(this_month_all_payment_dates_item.pay_amount)
                        get_package_dates_result += get_package_dates_result_part
                        get_package_dates_html = "{}{}".format(get_package_dates_html,
                                                             "<td style='background:{};'>{}</td>".format(color[0],get_package_dates_result_part))
    # "*********************************************************************************************************************************************************")
                        get_package_dates_debt_result_part = 0.0
                        for this_month_all_payment_dates_item in this_month_all_payment_dates:
                            get_package_dates_debt_result_part += float(this_month_all_payment_dates_item.percent_debt)
                        get_package_dates_debt_result += get_package_dates_debt_result_part
                        get_package_dates_debt_html = "{}{}".format(get_package_dates_debt_html,
                                                             "<td style='background:{};'>{}</td>".format(color[0],get_package_dates_debt_result_part))
    # "*********************************************************************************************************************************************************")

                        this_month_income_outcome_objs_general_result_part = 0.0
                        for this_month_income_outcome_objs_item in this_month_income_outcome_objs.filter(reason='outcome').exclude(type='office'):
                            this_month_income_outcome_objs_general_result_part += float(this_month_income_outcome_objs_item.amount)
                        this_month_income_outcome_objs_general_result += this_month_income_outcome_objs_general_result_part
                        this_month_income_outcome_objs_general_html = "{}{}".format(this_month_income_outcome_objs_general_html,
                                                             "<td style='background:{};'>{}</td>".format(color[0],this_month_income_outcome_objs_general_result_part))
    # "*********************************************************************************************************************************************************")

                        this_month_income_outcome_objs_get_office_result_part = 0.0
                        for this_month_income_outcome_objs_item in this_month_income_outcome_objs.filter(reason='income').filter(type='office'):
                            this_month_income_outcome_objs_get_office_result_part += float(this_month_income_outcome_objs_item.amount)
                        this_month_income_outcome_objs_get_office_result += this_month_income_outcome_objs_get_office_result_part
                        this_month_income_outcome_objs_get_office_html = "{}{}".format(this_month_income_outcome_objs_get_office_html,
                                                             "<td style='background:{};'>{}</td>".format(color[0],this_month_income_outcome_objs_get_office_result_part))
    # "*********************************************************************************************************************************************************")

                        this_month_income_outcome_objs_get_office_result_part = 0.0
                        for this_month_income_outcome_objs_item in this_month_income_outcome_objs.filter(reason='outcome').filter(type='office'):
                            this_month_income_outcome_objs_get_office_result_part += float(this_month_income_outcome_objs_item.amount)
                        this_month_income_outcome_objs_get_office_result += this_month_income_outcome_objs_get_office_result_part
                        this_month_income_outcome_objs_give_office_html = "{}{}".format(this_month_income_outcome_objs_give_office_html,
                                                             "<td style='background:{};'>{}</td>".format(color[0],this_month_income_outcome_objs_get_office_result_part))
    # "*********************************************************************************************************************************************************")
                        end_month_day_cash_result_part = 0.0
                        last_month_day_cash_result_part = 0.0
                        for end_month_income_outcome_objs_item in end_month_income_outcome_objs:
                            if end_month_income_outcome_objs_item.reason == 'income':
                                end_month_day_cash_result_part += float(end_month_income_outcome_objs_item.amount)
                            else:
                                end_month_day_cash_result_part -= float(end_month_income_outcome_objs_item.amount)
                        for end_month_all_payment_dates_item in end_month_all_payment_dates:
                            end_month_day_cash_result_part += float(
                                end_month_all_payment_dates_item.pay_amount + end_month_all_payment_dates_item.paid_percent)
                        end_month_cash_html = "{}{}".format(end_month_cash_html,
                                                              "<td>{}</td>".format(end_month_day_cash_result_part))
                        end_month_cash_result += end_month_day_cash_result_part

    # "*********************************************************************************************************************************************************")

                    before_month_portfel = "{}{}".format(before_month_portfel,
                                                         "<td style='background:{};'>{}</td>".format(color[1],before_month_portfel_result))
                    generally_before_month_portfel = "{}{}".format(generally_before_month_portfel,
                                                         "<td>{}</td>".format(before_month_portfel_result))

                    generally_before_month_portfel = "<tr style='background:{};'><td>Olmalıdır{}</td>{}</tr>".format(color[0],currencies_list_item.short_title,generally_before_month_portfel)
                    # print("____________________________~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
                    # print(generally_before_month_portfel)
                    # print("____________________________~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
                    difference_month_portfel_html = "{}{}".format(difference_month_portfel_html,
                                                         "<td>{}</td>".format(difference_month_portfel_result))
                    end_month_portfel = "{}{}".format(end_month_portfel,
                                                         "<td style='background:{};'>{}</td>".format(color[1],end_month_portfel_result))
                    this_month_all_pack_result_html = "{}{}".format(this_month_all_pack_result_html,
                                                         "<td style='background:#0a8dd3;color:white;'>{}</td>".format(this_month_all_pack_result))
                    get_package_dates_html = "{}{}".format(get_package_dates_html,
                                                         "<td style='background:red;color:white;'>{}</td>".format(get_package_dates_result))
                    get_package_dates_debt_html = "{}{}".format(get_package_dates_debt_html,
                                                         "<td style='background:forestgreen;color:white;'>{}</td>".format(get_package_dates_debt_result))
                    this_month_income_outcome_objs_general_html = "{}{}".format(this_month_income_outcome_objs_general_html,
                                                         "<td style='background:yellow;'>{}</td>".format(this_month_income_outcome_objs_general_result))
                    this_month_income_outcome_objs_give_office_html = "{}{}".format(this_month_income_outcome_objs_give_office_html,
                                                         "<td style='background:yellow;'>{}</td>".format(this_month_income_outcome_objs_give_office_result))
                    this_month_income_outcome_objs_get_office_html = "{}{}".format(this_month_income_outcome_objs_get_office_html,
                                                         "<td style='background:yellow;'>{}</td>".format(this_month_income_outcome_objs_get_office_result))
                    end_month_cash_html = "{}{}".format(end_month_cash_html,
                                                         "<td style='background:yellow;'>{}</td>".format(end_month_cash_result))

                    main_html = "{}{}".format(main_html,
                        render_to_string(
                            'panel/statistic/month/table.html',
                            {
                                'color':color,
                                'department_list':department_list,
                                'before_month_portfel':before_month_portfel,
                                'difference_month_portfel_html':difference_month_portfel_html,
                                'end_month_portfel':end_month_portfel,
                                'given_packages_html':this_month_all_pack_result_html,
                                'get_package_dates_html':get_package_dates_html,
                                'get_package_dates_debt_html':get_package_dates_debt_html,
                                'this_month_income_outcome_objs_general_html':this_month_income_outcome_objs_general_html,
                                'this_month_income_outcome_objs_give_office_html':this_month_income_outcome_objs_give_office_html,
                                'this_month_income_outcome_objs_get_office_html':this_month_income_outcome_objs_get_office_html,
                                'end_month_cash_html':end_month_cash_html,


                                'head_text':month_head_text,
                            }
                        )
                    )

                for i in range(1,day_count[1]+1):
                    date_text = "{}/{}/{}".format(i,month,year)



                    date_month_day = datetime.datetime.strptime(date_day_str.format(i,month, year), '%d-%m-%Y')
                    currencies_list_i = 0
                    for currencies_list_item in currencies_list:
                        color = ['#57f857', '#b8ebb8', ] if currencies_list_item.short_title.lower() == 'usd' else [
                            '#ffa5b4', 'pink', ]
                        currencies_list_i += 1
                        head_text = render_to_string(
                                    'panel/statistic/daily/head.html',
                                    {
                                    'color':color,
                                        'date_text':date_text,
                                        'department_list':department_list,
                                        'currencies_list_item':currencies_list_item,
                                    }
                                )

                        before_day_portfel_result = 0

                        before_month_portfel = ''
                        before_month_portfel_result = 0

                        before_month_day_portfel = ''
                        before_month_day_portfel_result = 0

                        last_month_day_portfel = ''
                        last_month_day_portfel_result = 0

                        before_month_day_cash = ''
                        before_month_day_cash_result = 0

                        last_month_day_cash = ''
                        last_month_day_cash_result = 0

                        difference_month_portfel = ''
                        difference_month_portfel_result = 0

                        difference_month_day_portfel = ''
                        difference_month_day_portfel_result = 0

                        given_packages_html = ''
                        given_packages_result = 0

                        get_package_dates_html = ''
                        get_package_dates_result = 0

                        get_package_dates_debt_html = ''
                        get_package_dates_debt_result = 0

                        daily_outcome_html = ''
                        daily_outcome_result = 0

                        office_outcome_html = ''
                        office_outcome_result = 0

                        office_income_html = ''
                        office_income_result = 0

                        for department_list_item in department_list:

                            # ------------------------------------------------------------------------
                            # print("&&&&&&&&&&&&&&&&&&&&&&&&&&&")
                            # print(date_month_day)
                            # print("&&&&&&&&&&&&&&&&&&&&&&&&&&&")
                            before_month_all_packages = Package.objects.filter(currency=currencies_list_item,date__lte=date_month,department=department_list_item)
                            before_month_all_payment_dates = PackagePaymentDates.objects.filter(package__currency=currencies_list_item,date__lte=date_month,package__department=department_list_item)
                            before_month_all_result_pack_result = 0.0
                            before_month_day_all_packages = Package.objects.filter(currency=currencies_list_item,date__lt=date_month_day,department=department_list_item)
                            before_month_day_all_payment_dates = PackagePaymentDates.objects.filter(package__currency=currencies_list_item,date__lt=date_month_day,package__department=department_list_item)
                            before_month_day_all_result_pack_result = 0.0
                            # # print("***************************************************")
                            # # print("***************************************************")
                            for before_month_all_packages_item in before_month_all_packages:
                                before_month_all_result_pack_result += float(before_month_all_packages_item.amount)
                            for before_month_all_payment_dates_item in before_month_all_payment_dates:
                                before_month_all_result_pack_result -= float(before_month_all_payment_dates_item.pay_amount)

                            before_month_portfel = "{}{}".format(before_month_portfel,"<td>{}</td>".format(before_month_all_result_pack_result))
                            before_month_portfel_result += (before_month_all_result_pack_result)
                            # ------------------------------------------------------------------------
                            # # print("=========================================================================================")

                            # # print("=========================================================================================")
                            for before_month_day_all_packages_item in before_month_day_all_packages:
                                before_month_day_all_result_pack_result += float(before_month_day_all_packages_item.amount)
                            for before_month_day_all_payment_dates_item in before_month_day_all_payment_dates:
                                before_month_day_all_result_pack_result -= float(before_month_day_all_payment_dates_item.pay_amount)

                            before_month_day_portfel = "{}{}".format(before_month_day_portfel,"<td>{}</td>".format(before_month_day_all_result_pack_result))
                            before_month_day_portfel_result += (before_month_day_all_result_pack_result)
                            # =========================================================================================
                            # # print("=========================================================================================")
                            last_month_day_all_result_pack_part_result = 0.0

                            last_month_day_all_packages = Package.objects.filter(date__lt=(date_month_day + datetime.timedelta(days=1)),department=department_list_item)
                            last_month_day_all_payment_dates = PackagePaymentDates.objects.filter(date__lt=(date_month_day + datetime.timedelta(days=1)),package__department=department_list_item)
                            for last_month_day_all_packages_item in last_month_day_all_packages:
                                last_month_day_all_result_pack_part_result += float(last_month_day_all_packages_item.amount)
                            for last_month_day_all_payment_dates_item in last_month_day_all_payment_dates:
                                last_month_day_all_result_pack_part_result -= float(last_month_day_all_payment_dates_item.pay_amount)

                            last_month_day_portfel = "{}{}".format(last_month_day_portfel,"<td>{}</td>".format(last_month_day_all_result_pack_part_result))
                            last_month_day_portfel_result += (last_month_day_all_result_pack_part_result)
                            # =========================================================================================

                            difference_month_day_portfel = "{}{}".format(difference_month_day_portfel,"<td>{}</td>".format(last_month_day_all_result_pack_part_result - before_month_day_all_result_pack_result))
                            difference_month_portfel = "{}{}".format(difference_month_portfel,"<td>{}</td>".format(last_month_day_all_result_pack_part_result - before_month_all_result_pack_result))
                            # difference_month_day_portfel_result += (before_month_day_all_result_pack_result)
                            # =========================================================================================

                            today_all_packages = Package.objects.filter(first_date__day=i,first_date__month=month,first_date__year=year,department=department_list_item)

                            given_packages_result_part = 0.0
                            for today_all_packages_item in today_all_packages:
                                given_packages_result_part += float(today_all_packages_item.amount)
                            given_packages_html = "{}{}".format(given_packages_html, "<td>{}</td>".format(
                                    given_packages_result_part))
                            given_packages_result += given_packages_result_part



                            today_all_package_payments = PackagePaymentDates.objects.filter(package__currency=currencies_list_item,paid_date__day=i,paid_date__month=month,paid_date__year=year,package__department=department_list_item)


                            get_package_dates_result_part = 0.0
                            get_package_dates_debt_result_part = 0.0
                            for today_all_package_payments_item in today_all_package_payments:
                                try:
                                    total_result_data["d-{}-c-{}-income".format(department_list_item.id,currencies_list_item.id)] += float(today_all_package_payments_item.paid_percent)
                                except:
                                    total_result_data["d-{}-c-{}-income".format(department_list_item.id,currencies_list_item.id)] = float(today_all_package_payments_item.paid_percent)
                                get_package_dates_result_part += float(today_all_package_payments_item.pay_amount)
                                get_package_dates_debt_result_part += float(today_all_package_payments_item.paid_percent)
                            get_package_dates_html = "{}{}".format(get_package_dates_html, "<td>{}</td>".format(
                                    get_package_dates_result_part))
                            get_package_dates_result += get_package_dates_result_part
                            get_package_dates_debt_html = "{}{}".format(get_package_dates_debt_html, "<td>{}</td>".format(
                                    get_package_dates_debt_result_part))
                            get_package_dates_debt_result += get_package_dates_debt_result_part

                            # ==============================================================================
                            daily_outcome_objs = IncomeOutcome.objects.filter(currency=currencies_list_item,date__day=i,date__month=month,date__year=year,department=department_list_item,reason='outcome')
                            daily_outcome_result_part = 0.0
                            for daily_outcome_objs_item in daily_outcome_objs:
                                try:
                                    total_result_data["d-{}-c-{}-outcome".format(department_list_item.id,  currencies_list_item.id)] += float(daily_outcome_objs_item.amount)
                                except:
                                    total_result_data["d-{}-c-{}-outcome".format(department_list_item.id,currencies_list_item.id)] = float(daily_outcome_objs_item.amount)

                                daily_outcome_result_part += float(daily_outcome_objs_item.amount)
                            daily_outcome_html = "{}{}".format(daily_outcome_html,
                                                                        "<td>{}</td>".format(
                                                                            daily_outcome_result_part))
                            daily_outcome_result += daily_outcome_result_part
                            # ==============================================================================
                            daily_office_income_outcome_objs = IncomeOutcome.objects.filter(date__day=i,date__month=month,date__year=year,department=department_list_item,type='office')
                            # ==============================================================================
                            office_income_result_part = 0.0
                            for office_income_objs_item in daily_office_income_outcome_objs.filter(currency=currencies_list_item,reason='income'):
                                office_income_result_part += float(office_income_objs_item.amount)
                            office_income_html = "{}{}".format(office_income_html,
                                                                        "<td>{}</td>".format(
                                                                            office_income_result_part))
                            office_income_result += office_income_result_part
                            # ==============================================================================
                            # ==============================================================================
                            office_outcome_result_part = 0.0
                            for office_outcome_objs_item in daily_office_income_outcome_objs.filter(currency=currencies_list_item,reason='outcome'):
                                office_outcome_result_part += float(office_outcome_objs_item.amount)
                            office_outcome_html = "{}{}".format(office_outcome_html,
                                                                        "<td>{}</td>".format(
                                                                            office_outcome_result_part))
                            office_outcome_result += office_outcome_result_part
                            # ==============================================================================

                            # ==============================================================================
                            before_month_day_all_payment_dates = PackagePaymentDates.objects.filter(package__currency=currencies_list_item,date__lt=date_month_day,package__department=department_list_item)
                            last_month_day_all_payment_dates = PackagePaymentDates.objects.filter(package__currency=currencies_list_item,date__lt=(date_month_day + datetime.timedelta(days=1)),package__department=department_list_item)
                            before_daily_in_outcome_objs = IncomeOutcome.objects.filter(currency=currencies_list_item,package_payment_date=None).filter(date__lt=date_month_day ,department=department_list_item,reason='outcome')
                            last_daily_outcome_objs = IncomeOutcome.objects.filter(currency=currencies_list_item,package_payment_date=None).filter(date__lt=(date_month_day + datetime.timedelta(days=1)),department=department_list_item,reason='outcome')
                            before_month_day_cash_result_part = 0.0
                            last_month_day_cash_result_part = 0.0
                            for before_daily_in_outcome_objs_item in before_daily_in_outcome_objs:
                                if before_daily_in_outcome_objs_item.reason == 'income':
                                    before_month_day_cash_result_part += float(before_daily_in_outcome_objs_item.amount)
                                else:
                                    before_month_day_cash_result_part -= float(before_daily_in_outcome_objs_item.amount)
                            for before_month_day_all_payment_dates_item in before_month_day_all_payment_dates:
                                before_month_day_cash_result_part += float(before_month_day_all_payment_dates_item.pay_amount + before_month_day_all_payment_dates_item.paid_percent)
                            before_month_day_cash = "{}{}".format(before_month_day_cash,"<td>{}</td>".format(before_month_day_cash_result_part))
                            before_month_day_cash_result += before_month_day_cash_result_part

                            # -----------------****************************------------------------

                            last_month_day_cash_result_part = 0.0
                            for last_daily_in_outcome_objs_item in last_daily_outcome_objs:
                                if last_daily_in_outcome_objs_item.reason == 'income':
                                    last_month_day_cash_result_part += float(last_daily_in_outcome_objs_item.amount)
                                else:
                                    last_month_day_cash_result_part -= float(last_daily_in_outcome_objs_item.amount)
                            for last_month_day_all_payment_dates_item in last_month_day_all_payment_dates:
                                last_month_day_cash_result_part += float(last_month_day_all_payment_dates_item.pay_amount + last_month_day_all_payment_dates_item.paid_percent)
                            last_month_day_cash = "{}{}".format(last_month_day_cash,"<td>{}</td>".format(last_month_day_cash_result_part))
                            last_month_day_cash_result += last_month_day_cash_result_part
                            # -------------------****************************-----------------------------

                            # ==============================================================================

                            before_day_portfel_result = "{}{}".format(before_day_portfel_result,
                                                                 "<td>{}</td>".format(0)
                                                                 )
                        before_month_portfel = "{}{}".format(before_month_portfel,
                                                             "<td>{}</td>".format(before_month_portfel_result)
                                                             )
                        before_month_day_portfel = "{}{}".format(before_month_day_portfel,
                                                             "<td>{}</td>".format(before_month_day_portfel_result)
                                                             )
                        last_month_day_portfel = "{}{}".format(last_month_day_portfel,
                                                             "<td>{}</td>".format(last_month_day_portfel_result)
                                                             )
                        difference_month_day_portfel = "{}{}".format(difference_month_day_portfel,
                                                             "<td>{}</td>".format(last_month_day_portfel_result - before_month_day_portfel_result)
                                                             )
                        difference_month_portfel = "{}{}".format(difference_month_portfel,
                                                             "<td>{}</td>".format(last_month_day_portfel_result - before_month_portfel_result)
                                                             )
                        given_packages_html = "{}{}".format(given_packages_html,
                                                             "<td style='background: #0a8dd3;    color: #fff;'>{}</td>".format(given_packages_result))
                        get_package_dates_html = "{}{}".format(get_package_dates_html,
                                                             "<td style='background: red;    color: #fff;'>{}</td>".format(get_package_dates_result)
                                                             )
                        get_package_dates_debt_html = "{}{}".format(get_package_dates_debt_html,
                                                             "<td  style='background: forestgreen;    color: #fff;'>{}</td>".format(get_package_dates_debt_result)
                                                             )
                        daily_outcome_html = "{}{}".format(daily_outcome_html,
                                                             "<td>{}</td>".format(daily_outcome_result)
                                                             )

                        office_income_html = "{}{}".format(office_income_html,
                                                             "<td >{}</td>".format(office_income_result)
                                                             )
                        office_outcome_html = "{}{}".format(office_outcome_html,
                                                             "<td>{}</td>".format(office_outcome_result)
                                                             )
                        before_month_day_cash = "{}{}".format(before_month_day_cash,
                                                             "<td>{}</td>".format(before_month_day_cash_result)
                                                             )
                        last_month_day_cash = "{}{}".format(last_month_day_cash,
                                                             "<td>{}</td>".format(last_month_day_cash_result)
                                                             )
                        # print("__________________________________________________________________________________________________")
                        # print(total_result_data)
                        # print("__________________________________________________________________________________________________")
                        main_html = "{}{}".format(main_html,
                            render_to_string(
                                'panel/statistic/daily/item.html',
                                {
                                    "count_i":i,
                                    "currencies_list_i":currencies_list_i,
                                    'color':color,
                                    'before_month_portfel':before_month_portfel,
                                    'before_month_day_portfel':before_month_day_portfel,
                                    'last_month_day_portfel':last_month_day_portfel,
                                    'given_packages_html':given_packages_html,
                                    'get_package_dates_html':get_package_dates_html,
                                    'get_package_dates_debt_html':get_package_dates_debt_html,
                                    'daily_outcome_html':daily_outcome_html,
                                    'office_income_html':office_income_html,
                                    'office_outcome_html':office_outcome_html,
                                    'difference_month_day_portfel':difference_month_day_portfel,
                                    'difference_month_portfel':difference_month_portfel,
                                    'before_month_day_cash':before_month_day_cash,
                                    'last_month_day_cash':last_month_day_cash,


                                    # 'before_day_portfel_result':before_day_portfel_result,
                                    'same_day': True if currencies_list.count() != currencies_list_i else False,
                                    # 'non_operate':context['non_operate'],
                                    # 'list_html':list_html,
                                    'head_text':head_text,
                                }
                            )
                        )

                totally_result_html = ''
                totally_head_text = ''
                for currencies_list_item in currencies_list:
                    totally_head_text = render_to_string(
                        'panel/statistic/month/head.html',
                        {
                            'currencies_list_item': currencies_list_item,
                            'department_list': department_list,
                            'color':'green' if currencies_list_item.short_title.lower() == 'usd' else 'hotpink'
                        }
                    )
                    part_income_html = ''
                    part_income_res = 0.0
                    part_outcome_html = ''
                    part_outcome_res = 0.0
                    part_difference_html = ''
                    part_difference_res = 0.0
                    for department_list_item in department_list:
                        dep_part_income = 0
                        dep_part_outcome = 0
                        try:
                            dep_part_income = total_result_data["d-{}-c-{}-income".format(department_list_item.id,currencies_list_item.id)]
                            part_income_html = "{}<td>{}</td>".format(part_income_html,
                                                                      dep_part_income
                            )
                            part_income_res += dep_part_income
                        except:
                            dep_part_income = 0
                            part_income_html = "{}<td>{}</td>".format(part_income_html,
                               dep_part_income
                            )
                        try:
                            dep_part_outcome = total_result_data["d-{}-c-{}-outcome".format(department_list_item.id,currencies_list_item.id)]
                            part_outcome_html = "{}<td>{}</td>".format(part_outcome_html,
                                                                       dep_part_outcome
                            )
                            part_outcome_res += dep_part_outcome
                        except:
                            dep_part_outcome = 0
                            part_outcome_html = "{}<td>{}</td>".format(part_outcome_html,
                               dep_part_outcome
                            )
                        part_difference_html = "{}<td>{}</td>".format(part_difference_html,(
                            dep_part_income -
                            dep_part_outcome
                        ))
                        part_difference_res += (dep_part_income -
                                                dep_part_outcome)
                    # print(part_income_html)
                    # print(part_outcome_html)
                    color = ['#74fc74','#b8ebb8',] if currencies_list_item.short_title.lower() == 'usd' else ['pink','#ffe8ec',]
                    # print("-------------------________________________________")
                    result_both_part ="<tr style='background:{} '><td style='background: orange;'>%</td>{}<td>{}</td></tr><tr style='background:{} '><td style='background: darkkhaki;'>Xərc</td>{}<td>{}</td></tr><tr style='background:{}; '><td style='background: yellow;'>Qalıq</td>{}<td>{}</td></tr>".format(color[0],part_income_html,part_income_res,color[1],part_outcome_html,part_outcome_res,color[0],part_difference_html,part_difference_res)
                    totally_result_html = "{}{}".format(
                        totally_result_html,
                        render_to_string(
                            "panel/statistic/month/main/list-item.html",
                            {
                                "result_both_part" : result_both_part,
                                "totally_head_text" : totally_head_text,
                            }
                        )
                    )

                    # print(result_both_part)
                # print(totally_head_text)
                main_html = "<h2 class='text-center'>Qazanc</h2><hr/>{}<hr><h2 class='text-center'>Aylıq</h2><hr/>{}".format(totally_result_html,main_html)
            elif type == 'customers':
                department_list = Department.objects.filter()
                head_text = "{}".format(
                    render_to_string(
                        "panel/statistic/customers/head.html",
                        {
                            'department_list':department_list
                        }
                    )
                )

                year = int(cleaned_data.get('year',0))
                month = int(cleaned_data.get('month',0))

                day_count = calendar.monthrange(year=year, month=month)

                list_html = ''
                general_data = {}
                for i in range(1,day_count[1]+1):
                    date_text = "{}/{}/{}".format(i,month,year)
                    date_month_day = datetime.datetime.strptime(date_day_str.format(i,month, year), '%d-%m-%Y')

                    list_part_html = ""
                    for department_list_item in department_list:
                        all_customers_list = []
                        new_customers_list = []
                        package_items = Package.objects.filter(first_date__year=year,first_date__month=month,first_date__day=i).filter(department=department_list_item)
                        for package_item in package_items:
                            if package_item.customer_id not in  all_customers_list:
                                all_customers_list.append(package_item.customer_id)
                                package_items = Package.objects.filter(first_date__lte=date_month_day).filter(
                                    customer=package_item.customer)
                            if package_items.count()==0 and package_item.customer_id not in new_customers_list:
                                new_customers_list.append(package_item.customer_id)
                        part_percent = 0
                        if all_customers_list and new_customers_list:
                            part_percent = len(new_customers_list)/len(all_customers_list) * 100
                        list_part_html = "{}{}".format(list_part_html,"<td>{}</td><td>{}</td><td style='background: lawngreen;'>{}</td>".format(len(all_customers_list),len(new_customers_list),"{0:.1f}".format(round(part_percent,1)),))

                        try:
                            general_data['d-{}-a'.format(department_list_item.id)] += len(all_customers_list)
                            general_data['d-{}-n'.format(department_list_item.id)] += len(new_customers_list)
                        except:
                            general_data['d-{}-a'.format(department_list_item.id)] = len(all_customers_list)
                            general_data['d-{}-n'.format(department_list_item.id)] = len(new_customers_list)

                    list_html = "{}<tr><td>{}</td>{}</tr>".format(list_html,i,list_part_html)
                result_part = ''

                for department_list_item in department_list:
                    main_part_percent = 0
                    if general_data['d-{}-n'.format(department_list_item.id)] and general_data[
                        'd-{}-a'.format(department_list_item.id)]:
                        main_part_percent = general_data['d-{}-n'.format(department_list_item.id)] / \
                                            general_data['d-{}-a'.format(department_list_item.id)] * 100
                    result_part = "{}</td><td style='color:white;'>{}</td><td style='color:white;'>{}</td><td style='color:white;'>{}</td>".format(
                        result_part,
                        general_data['d-{}-a'.format(department_list_item.id)],
                        general_data['d-{}-n'.format(department_list_item.id)],
                        "{0:.1f}".format(main_part_percent),
                    )
                list_html = "<tr style='background: mediumpurple;'><td style='color:white;'>{}</tr>{}".format(result_part,list_html)
                main_html = "{}".format(
                    render_to_string(
                        "panel/statistic/customers/main.html",
                        {
                            'head_text':head_text,
                            'list_html':list_html,
                        }
                    )
                )
            elif type == 'general':

                department_list = Department.objects.filter()
                currencies_list = Currency.objects.filter().order_by('-id')

                year = int(cleaned_data.get('year',0))
                month = int(cleaned_data.get('month',0))
                total_result_data = {}

                date_month = datetime.datetime.strptime('1-{}-{}'.format(month,year), '%d-%m-%Y')

                day_count = calendar.monthrange(year=year, month=month)
                before_date_month = datetime.datetime.strptime('1-{}-{}'.format(month,year), '%d-%m-%Y')
                end_date_month = datetime.datetime.strptime('{}-{}-{}'.format(day_count[1],month,year), '%d-%m-%Y')
                packages = Package.objects.filter(first_date=end_date_month)
                currencies_list_i = 0

                main_html = '<div class="row">'
                customer_list = [ x.id for x in packages]

                main_html_1 = ''
                for currencies_list_item in currencies_list:
                    list_item_1 = ''
                    list_item_3 = ''
                    list_item_2 = ''
                    color = 'greenyellow' if currencies_list_item.short_title.lower() == 'usd' else 'pink'



                    customer_list_main = []
                    customer_package_res_main = 0.0
                    for department_list_item in department_list:
                        customer_list = []
                        customer_package_res = 0.0
                        pack_list = Package.objects.filter(currency=currencies_list_item,department=department_list_item,first_date__gt=before_date_month,first_date__lte=end_date_month)
                        for pack_list_item in pack_list:
                            customer_package_res += float(pack_list_item.amount)
                            if pack_list_item.customer_id not in customer_list:
                                customer_list.append(pack_list_item.customer_id)

                        list_item_1 = "{}{}".format(list_item_1,
                                                    "<tr><td style='background: {};'>{}</td><td>{}</td><td>{}</td></tr>".format(
                                                        color,
                                                        department_list_item.title,
                                                        len(customer_list),
                                                        customer_package_res,
                                                    ))
                        customer_package_res_main += customer_package_res
                        customer_package_res_main += len(customer_list)
                    main_html_1 = "{}".format(
                        render_to_string(
                            "panel/statistic/customers/pack/table-1.html",
                            {
                                'currency_item':currencies_list_item,
                                'list_html':list_item_1,
                                'color':color,
                                'title':" AY ƏRZİNDƏ KREDİT VERİLƏN MÜŞTƏRİLƏRİN SAYI VƏ MƏBLƏĞ",
                            }
                        )
                    )


                    customer_package_res_main_3 = 0.0
                    customer_package_debt_res_main_3 = 0.0
                    customer_package_res_main_dict_3 = {}

                    for department_list_item in department_list:
                        customer_list = []
                        customer_package_res = 0.0
                        customer_package_debt_res = 0.0
                        waiting_packages = WaitingPackage.objects.filter(package__currency=currencies_list_item,package__department=department_list_item).filter(date__gte=end_date_month,update_date__lte=end_date_month)

                        # pack_list = Package.objects.filter(currency=currencies_list_item,department=department_list_item,first_date__gt=before_date_month,first_date__lte=end_date_month)

                        for pack_list_item in waiting_packages:
                            customer_package_res += float(pack_list_item.debt_amount)
                            customer_package_debt_res += float(pack_list_item.debt_amount) * float(pack_list_item.package.percent)/100
                            if pack_list_item.package.customer_id not in customer_list:
                                try:
                                    list(customer_package_res_main_dict_3['c-{}-d-{}'.format(currencies_list_item.id,department_list_item.id)]).append(pack_list_item.package.customer_id)
                                except:
                                    customer_package_res_main_dict_3['c-{}-d-{}'.format(currencies_list_item.id,department_list_item.id)] = [pack_list_item.package.customer_id]
                                customer_list.append(pack_list_item.package.customer_id)

                        list_item_3 = "{}{}".format(list_item_3,
                                                    "<tr><td style='background: {};'>{}</td><td>{}</td><td>{}</td><td>{}</td></tr>".format(
                                                        color,
                                                        department_list_item.title,
                                                        len(customer_list),
                                                        customer_package_res,
                                                        customer_package_debt_res,
                                                    ))
                        customer_package_res_main_3 += customer_package_res
                        customer_package_res_main_3 += len(customer_list)
                        customer_package_debt_res_main_3 += customer_package_debt_res

                    main_html_3 = "{}".format(
                        render_to_string(
                            "panel/statistic/customers/pack/table-1.html",
                            {
                                'currency_item':currencies_list_item,
                                'list_html':list_item_3,
                                'color':color,
                                'title':"LOMBARDLAR ÜZRƏ YOXLAMADA OLAN MÜŞTƏRİLƏRİN SAYI VƏ MƏBLƏĞ",
                                'customer_package_debt_res':customer_package_debt_res_main_3
                            }
                        )
                    )

                    package_paymnets = PackagePaymentDates.objects.filter()
                    customer_package_res_main_2 = 0.0
                    for department_list_item in department_list:
                        customer_list = []
                        customer_package_res = 0.0
                        try:
                            id_list = customer_package_res_main_dict_3['c-{}-d-{}'.format(currencies_list_item.id, department_list_item.id)]
                        except:
                            id_list = []
                        pack_list = Package.objects.exclude(customer_id__in=id_list).filter(currency=currencies_list_item,department=department_list_item,first_date__gt=before_date_month,first_date__lte=end_date_month)
                        for pack_list_item in pack_list:
                            package_paymnets_details = package_paymnets.filter(package=pack_list_item,paid_date__lte=end_date_month)
                            customer_package_res += float(pack_list_item.amount)
                            if pack_list_item.customer_id not in customer_list:
                                customer_list.append(pack_list_item.customer_id)
                            for package_paymnets_details_item in package_paymnets_details:
                                customer_package_res -= float(package_paymnets_details_item.pay_amount)

                        list_item_2 = "{}{}".format(list_item_2,
                                                    "<tr><td style='background: {};'>{}</td><td>{}</td><td>{}</td></tr>".format(
                                                        color,
                                                        department_list_item.title,
                                                        len(customer_list),
                                                        customer_package_res,
                                                    ))
                        customer_package_res_main_2 += customer_package_res
                        customer_package_res_main_2 += len(customer_list)

                    main_html_2 = "{}".format(
                        render_to_string(
                            "panel/statistic/customers/pack/table-1.html",
                            {
                                'currency_item':currencies_list_item,
                                'list_html':list_item_2,
                                'color':color,
                                'title':"LOMBARDLAR ÜZRƏ <span style='color:#fff;'>AKTİV</span> MÜŞTƏRİLƏRİN SAYI VƏ MƏBLƏĞ",
                            }
                        )
                    )
                    main_html = "{}{}".format(main_html, main_html_1)
                    main_html = "{}{}".format(main_html, main_html_2)
                    main_html = "{}{}".format(main_html, main_html_3)
            elif type == 'monthly-p2':
                department_list = Department.objects.filter()
                currencies_list = Currency.objects.filter().order_by('-id')
                year = int(cleaned_data.get('year',0))
                month = int(cleaned_data.get('month',0))

                total_result_data = {}

                date_month = datetime.datetime.strptime('1-{}-{}'.format(month,year), '%d-%m-%Y')
                # print("------------------------------------------------------")
                # # print(date_month)
                # print("------------------------------------------------------")

                all_result = IncomeOutcome.objects.filter().order_by('date')

                # result_data = {}
                day_count = calendar.monthrange(year=year, month=month)
                before_date_month = datetime.datetime.strptime('1-{}-{}'.format(month,year), '%d-%m-%Y')
                end_date_month = datetime.datetime.strptime('{}-{}-{}'.format(day_count[1],month,year), '%d-%m-%Y')
                currencies_list_i = 0
                month_head_text = ''
                main_html_0_1 = ''
                for currencies_list_item in currencies_list:
                    color = 'greenyellow' if currencies_list_item.short_title.lower() == 'usd' else 'pink'
                    currencies_list_i += 1
                    month_head_text = render_to_string(
                        'panel/statistic/month/p2/head.html',
                        {
                            'date_text': "{}-{}-{}".format(day_count[1], month, year),
                            'department_list': department_list,
                            'currencies_list_item': currencies_list_item,
                            'color': color,
                        }
                    )
                    green_generally_must_be_html = ''
                    green_generally_must_be_result = 0
                    green_generally_be_html = ''
                    green_generally_be_result = 0
                    green_generally_difference_html = ''
                    green_generally_difference_result = 0

                    generally_must_be_html = ''
                    generally_must_be_result = 0
                    generally_be_html = ''
                    generally_be_result = 0
                    generally_difference_html = ''
                    generally_difference_result = 0
                    generally_before_month_portfel = ''
                    generally_before_month_portfel_result = 0
                    # before_month_portfel = ''
                    # before_month_portfel_result = 0

                    color = ['greenyellow', 'pink', ] if currencies_list_item.short_title.lower() == 'usd' else ['pink','greenyellow', ]

                    for department_list_item in department_list:
                        generally_be__part_result = 0.0
                        this_month_all_pack_payments = PackagePaymentDates.objects.filter(
                            package__department=department_list_item,package__currency=currencies_list_item,
                            date__gte=before_date_month,date__lte=end_date_month,
                        )
                        for this_month_all_pack_payments_item in this_month_all_pack_payments:
                            generally_be__part_result += float(this_month_all_pack_payments_item.paid_percent)



                        before_month_all_packages = Package.objects.filter(currency=currencies_list_item,
                                                                           date__lt=before_date_month,
                                                                           department=department_list_item)
                        before_month_all_payment_dates = PackagePaymentDates.objects.filter(
                            package__currency=currencies_list_item, date__lt=date_month,
                            package__department=department_list_item)

                        # "*********************************************************************************************************************************************************")
                        before_month_all_result_pack_result = 0.0
                        generally_must_be_part_result = 0.0
                        part_res = {}
                        for before_month_all_packages_item in before_month_all_packages:
                            before_month_all_result_pack_result += float(before_month_all_packages_item.amount)
                            part_res['p-{}'.format(before_month_all_packages_item.id)] = [
                                before_month_all_packages_item.amount, before_month_all_packages_item.percent]
                            for before_month_all_payment_dates_item in before_month_all_payment_dates.filter(package=before_month_all_packages_item):
                                try:
                                    part_res['p-{}'.format(before_month_all_payment_dates_item.package_id.id)][
                                        0] -= before_month_all_payment_dates_item.pay_amount
                                except:
                                    pass
                                before_month_all_result_pack_result -= float(before_month_all_payment_dates_item.pay_amount)
                            generally_must_be_part_result += float(part_res['p-{}'.format(before_month_all_packages_item.id)][0]) * float(part_res['p-{}'.format(before_month_all_packages_item.id)][1]) / 100
                        # print("------------------------------------------")
                        # # print(part_res)
                        # print("------------------------------------------")
                        # for before_month_all_payment_dates_item in before_month_all_payment_dates:
                        #     try:
                        #         part_res['p-{}'.format(before_month_all_payment_dates_item.package_id.id)][
                        #             0] -= before_month_all_payment_dates_item.pay_amount
                        #     except:
                        #         pass
                        #     before_month_all_result_pack_result -= float(before_month_all_payment_dates_item.pay_amount)
                        # print("********************************************************")
                        # print(part_res)

                        part_res_count = 0.0
                        for key in part_res.keys():
                            part_res_count += float(part_res[key][0]) * float(part_res[key][1]) / 100

                        # print("********************************************************")
                        generally_be_result += generally_be__part_result
                        generally_be_html = "{}{}".format(generally_be_html,
                                                             "<td>{}</td>".format(generally_be__part_result))


                        generally_must_be_result += (generally_must_be_part_result)
                        generally_must_be_html = "{}{}".format(generally_must_be_html,
                                                             "<td>{}</td>".format(generally_must_be_part_result))
                        green_generally_be_part_result = generally_be__part_result/generally_must_be_part_result * 100 if generally_must_be_part_result !=0 else 100
                        green_generally_must_be_html = "{}{}".format(green_generally_must_be_html,
                                                             "<td>{}%</td>".format(100))
                        # generally_must_be_html = "{}{}".format(generally_must_be_html,
                        #                                      "<td>{}</td>".format(generally_must_be_part_result))

                        green_generally_be_html = "{}{}".format(green_generally_be_html,
                                                             "<td>{}%</td>".format(
                                                                 green_generally_be_part_result
                                                             ))

                        generally_difference_part_result = generally_be__part_result - generally_must_be_part_result
                        generally_difference_result += generally_difference_part_result

                        generally_difference_html = "{}{}".format(generally_difference_html,
                                                             "<td style='background: {};color:white;'>{}</td>".format('green' if generally_difference_part_result>=0 else 'red', generally_difference_part_result))

                        green_generally_difference_html = "{}{}".format(green_generally_difference_html,
                                                             "<td>{}%</td>".format(
                                                                 '%.1f'.format(round((100 - generally_be_result/generally_must_be_result * 100) if generally_must_be_result != 0 else 100 ,1 ))
                                                             ))


                        generally_before_month_portfel_result += (before_month_all_result_pack_result)
                        generally_before_month_portfel = "{}{}".format(generally_before_month_portfel,
                                                                       "<td>{}</td>".format(
                                                                           before_month_all_result_pack_result))

                    generally_before_month_portfel = "{}{}".format(generally_before_month_portfel,
                                                                   "<td>{}</td>".format(generally_before_month_portfel_result))

                    generally_before_month_portfel = "<tr style='background:{};'><td>Ayın əvvəli portfel{}</td>{}</tr>".format(
                        'yellow', currencies_list_item.short_title, generally_before_month_portfel)

                    generally_must_be_html = "{}{}".format(generally_must_be_html,
                                                                   "<td>{}</td>".format(generally_must_be_result))

                    if generally_before_month_portfel_result and generally_must_be_result:
                        middle_percent = generally_must_be_result / generally_before_month_portfel_result * 100
                    # elif generally_must_be_result>0:
                    #     middle_percent = 100
                    else:
                        middle_percent = 0
                    generally_must_be_html = "<tr style='background:{};'><td>Olmalıdır {}%  </td>{}</tr>".format(
                        'yellow','{0:.1f}'.format(middle_percent), generally_must_be_html)

                    generally_be_html = "{}{}".format(generally_be_html,
                                                                   "<td>{}</td>".format(generally_be_result))

                    generally_be_html = "<tr style='background:{};'><td>Yığılıb</td>{}</tr>".format(
                        'yellow', generally_be_html)

                    generally_difference_html = "{}{}".format(generally_difference_html,"<td  style='background: {};color:white;'>{}</td>".format('green' if generally_difference_result>=0 else 'red', generally_difference_result))

                    generally_difference_html = "<tr style='background:{};'><td>Fərq </td>{}</tr>".format('yellow', generally_difference_html)


                    green_generally_be_html = "{}{}".format(green_generally_be_html,"<td  style=''>{}%</td>".format(
                        (generally_be_result / generally_must_be_result * 100) if generally_must_be_result != 0 else 100
                    ))

                    green_generally_be_html = "<tr style='background:{}'><td>Yığılıb  </td>{}</tr>".format(color[0],green_generally_be_html)


                    green_generally_must_be_html = "{}{}".format(green_generally_must_be_html,"<td  style=''>{}%</td>".format(100))

                    green_generally_must_be_html = "<tr style='background:{};'><td>Olmaldır</td>{}</tr>".format(color[0], green_generally_must_be_html)

                    main_html = "{}{}".format(main_html,
                                              render_to_string(
                                                  'panel/statistic/month/p2/main.html',
                                                  {
                                                      'color': color,
                                                      'department_list': department_list,
                                                      'generally_before_month_portfel': generally_before_month_portfel,
                                                      'generally_must_be_html': generally_must_be_html,
                                                      'generally_be_html': generally_be_html,
                                                      'generally_difference_html': generally_difference_html,

                                                      'head_text': month_head_text,
                                                  }))
                    main_html_0_1 = "{}{}".format(main_html_0_1,
                                              render_to_string(
                                                  'panel/statistic/month/p2/main-2.html',
                                                  {
                                                      'color': color,
                                                      'department_list': department_list,
                                                      # 'generally_before_month_portfel': generally_before_month_portfel,
                                                      'green_generally_must_be_html': green_generally_must_be_html,
                                                      'green_generally_be_html': green_generally_be_html,
                                                      # 'generally_difference_html': generally_difference_html,

                                                      'head_text': month_head_text,
                                                  }))
                main_html = "{}{}".format(main_html,main_html_0_1)
            main_html = '{}<div/>'.format(main_html)
            message_code = 1
            data = {
                # 'total_result':total_html,
                'main_result':main_html,
                'message_code':message_code,
            }
            return JsonResponse(data=data)
        else:
            pass


    context['page_title'] = page_title
    context['search_form'] = search_form
    return render(request, 'panel/statistic/main.html', context=context)

